import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoggerService } from '../authentication/logger.service';
import { BaseService, StorageService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  memoryStorage;
  guest;

  constructor(private baseService: BaseService, private router: Router, private loggerService: LoggerService,
    private storageService: StorageService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let guest
    this.baseService.guest.subscribe(data => guest = data)
    let u_id = this.storageService.getLocalStore('u_id')
    if (!guest && u_id) {
      return true;
    } else {
      this.loggerService.updatedMemoryStorage({})
      this.storageService.setLocalStore('logged_in', '0');
      this.storageService.removeLocalStore('u_id');
      this.storageService.removeLocalStore("sessionStorage");
      this.storageService.removeLocalStore('a_t');
      this.storageService.removeLocalStore('r_t');
      if (this.baseService.loginMode == 1) {
        this.router.navigateByUrl('/index')
        return false;
      } else {
        this.baseService.guestSource.next(true);
        this.router.navigateByUrl('/home');
        return false;
      }
    }
  }

}
