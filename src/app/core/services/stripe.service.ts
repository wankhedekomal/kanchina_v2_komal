import { Injectable } from '@angular/core';
import { SettingsService } from './settings.service';
import { loadStripe } from '@stripe/stripe-js';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  id: string;
  stripePromise: any = null;
  stripe: any = null;
  constructor(private settingsService: SettingsService) {
    const publishableKey = this.settingsService.stripeKey;
    this.stripePromise = loadStripe(publishableKey)
      .then(res => {
        this.stripe = res;
      });
  }

  checkout(id) {
    this.stripe.redirectToCheckout({ sessionId: id })
      .then(res => {
        console.log(res);
      })
      .catch(error => {
        console.log(error);
      });
  }

}
