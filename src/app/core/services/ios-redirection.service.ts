import { Injectable } from '@angular/core';
import { api, iosURL } from 'src/app/shared/constants';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IosRedirectionService {

  constructor(private http: HttpClient) { }

  iosRedirect(data: any): Observable<any>{
    const iosUrl = api + iosURL;
    const userId: string = data.u_id;
    const d_id: string = data.d_id;
    const g_id: string = data.g_id;
    const params = new HttpParams()
      .set('u_id', userId)
      .set('d_id', d_id)
      .set('g_id', g_id);
    return this.http.get<any>(iosUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
