import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { api } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChannelsService {

  constructor(private http: HttpClient) { }

  getChannels(data: any): Observable<any> {
    const d_type: string = data.d_type;
    const skip: string = data.skip;
    const take: string = data.take;
    const params: HttpParams = new HttpParams()
      .set('d_type', d_type)
      .set('skip', skip)
      .set('take', take);
    const chaneelURL = api + 'assets/livechannels';
    return this.http.get(chaneelURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
