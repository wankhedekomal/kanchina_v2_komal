import { ApplicationService } from './application.service';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { api, staticPages } from 'src/app/shared/constants/url.constants';


@Injectable({
    providedIn: 'root'
})

  export class StaticService {
      constructor(private http: HttpClient, private baseService: BaseService, private applicationService: ApplicationService){}

      getfooterDetails(pageId): Observable<any> {
        const staticUrl = api + staticPages + pageId;
        const params = new HttpParams()
          .set('d_type', 'web')
          .set('locale', 'en')
          .set('region', 'int');
        return this.http.get(staticUrl, { params }).pipe(
          map(
            (response) => {
              if (response['status_code'] === 200) {
                return response['data'];
              }
            }
            , (error: HttpErrorResponse) => {
              console.log(error);
            }
          )
        );
      }
  }
