import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { api, payPerView } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PayPerViewService {

  constructor(private http: HttpClient) { }

  getpayperView(data: any): Observable<any> {
    const video_type: string = data.video_type;
    const content_id: string = data.content_id;
    const u_id: string = data.u_id;
    const d_type: string = data.d_type;
    const params: HttpParams = new HttpParams()
      .set('d_type', d_type)
      .set('u_id', u_id);
    const ppvUrl = api + payPerView + video_type + '/' + content_id;
    return this.http.get(ppvUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
