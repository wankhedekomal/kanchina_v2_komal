import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api, resendMail } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResendMailService {

  constructor(private http: HttpClient) { }

  resendMail(data: any): Observable<any> {
    const chnagePasswordUrl = api + resendMail;
    return this.http.post<any>(chnagePasswordUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
