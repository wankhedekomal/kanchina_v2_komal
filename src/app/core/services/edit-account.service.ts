import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api, User } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';
import { ITS_JUST_ANGULAR } from '@angular/core/src/r3_symbols';

@Injectable({
  providedIn: 'root'
})
export class EditAccountService {

  constructor(private http: HttpClient) { }

  editAccountFun(data: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-type': undefined
    });
    const editUrl = api + User;
    return this.http.post<any>(editUrl, data, { headers }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
