import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api, watchCount, liveChannel } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class LiveVideoService {

  constructor(private http: HttpClient, private storageService: StorageService) { }

  getLiveVideo(data: any): Observable<any> {
    const u_id: string = data.u_id;
    const d_type: string = data.d_type;
    const video_id: string = data.video_id;
    const params: HttpParams = new HttpParams()
      .set('d_type', d_type)
      .set('u_id', u_id);
    const liveUrl = api + liveChannel + video_id;
    return this.http.get(liveUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  countwatchedTime(id: any, contentType: any): Observable<any> {
    const watchCountUrl = api + watchCount;
    const userId: string = this.storageService.getLocalStore('u_id');
    const body = {
      content_id: id,
      u_id: userId,
      content_type: contentType,
      d_type: 'web'
    };
    return this.http.put(watchCountUrl, body).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

}
