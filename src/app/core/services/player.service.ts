import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { api, playerVideo } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }

  getPlayerVideo(data: any): Observable<any> {
    const d_type: string = data.d_type;
    const content_type = data.content_type;
    const video_id: string = data.video_id;
    const params: HttpParams = new HttpParams()
      .set('d_type', d_type)
      .set('locale', 'en');
    const playerUrl = api + playerVideo + content_type + '/' + video_id;
    return this.http.get(playerUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
