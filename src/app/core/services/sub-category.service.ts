import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { api, assetSeeall } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class SubCategoryService {

  constructor(private http: HttpClient) { }

  getSubCategory(data): Observable<any> {
    const guest = data.guest;
    const cat = data.cat;
    const title = data.title;
    const skip = data.skip;
    const take = data.take;
    let paramurl;
    let params: HttpParams = new HttpParams()
      .set('d_type', 'web');
    if (guest) {
      if (cat) {
        const key = cat;
        const categoryId = title;
        // if (sub_cat_title) {
        //   var categoryId = sub_cat_title;
        // } else {
        //   var categoryId = catID;
        // }
        paramurl = key + '/' + categoryId;
        params = params.append('key', key)
          .append('category', categoryId)
          .append('skip', skip)
          .append('take', take);
      } else {
        const key = title;
        // if ($rootScope.sub_cat_title) {
        //   var key = $rootScope.sub_cat_title;
        // } else {
        //   var key = catID;
        // }
        paramurl = key;
        params = params.append('key', key)
          .append('skip', skip)
          .append('take', take);
      }
    } else {
      if (cat) {
        const key = cat;
        const categoryId = title;
        // if ($rootScope.sub_cat_title) {
        //   var categoryId = $rootScope.sub_cat_title;
        // } else {
        //   var categoryId = catID;
        // }
        paramurl = key + '/' + categoryId;
        params = params.append('key', key)
          .append('category', categoryId)
          .append('skip', skip)
          .append('take', take);
      } else {
        const key = title;
        // if ($rootScope.sub_cat_title) {
        //   var key = $rootScope.sub_cat_title;
        // } else {
        //   var key = catID;
        // }
        paramurl = key;
        params = params.append('key', key)
          .append('skip', skip)
          .append('take', take);
      }
    }
    const subCatURL = api + assetSeeall + paramurl;
    return this.http.get(subCatURL, { params })
      .pipe(
        map(
          (response) => {
            if (response['status_code'] === 200) {
              const respData = response['data'];
              return respData;
            }
            else {
              // this.uikitService.notifyError(response)
              // $state.go('profile.404', {}, { reload: true });
            }
          }
          , (error: HttpErrorResponse) => {
            if (error && error.error.code !== 1002) {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
            }
          }
        )
      );
  }
}
