import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { apiUrl, version, settings } from 'src/app/shared/constants/url.constants';
import { SettingsService } from './settings.service';
import { FooterService } from './footer.service';
import { StorageService } from './storage.service';
import { UikitService } from './uikit.service';
import { BaseService } from './base.service';
import { ConfigurationService } from './configuration.service';
import { LoggerService } from '../authentication/logger.service';
import { LoadService } from './load.service';
import { LanguageService } from './language.service';
import { Router } from '@angular/router';
import { VersionService } from './version.service';
import * as $ from 'jquery';
declare global {
  interface Window {
    dataLayer: Array<any>;
  }
}

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  paramurl;
  params;

  constructor(private http: HttpClient, private settingsService: SettingsService,
              private footerService: FooterService, private storageService: StorageService,
              private uikitService: UikitService, private baseService: BaseService,
              private configService: ConfigurationService, private loggerService: LoggerService,
              private loadService: LoadService, private languageService: LanguageService,
              private router: Router, private versionService: VersionService
  ) {
    window.dataLayer = window.dataLayer || [];
  }


  initApp = async () => {
    const userId = this.storageService.getLocalStore('u_id');
    const deviceId = this.storageService.getLocalStore('d_id');
    const guestId = this.storageService.getLocalStore('g_id');
    let localeSetting = this.storageService.getLocalStore('locale');
    let debugRegion: any = '';
    const checkExp = /type=([a-z.]*)/;
    const checkDevice = /\?device_type=([a-z.]*)/;
    if (location.search.match(checkExp)) {
      if (location.search.match(checkExp)[1] !== undefined && location.search.match(checkExp)[1] === 'debug') {
        debugRegion = this.storageService.getLocalStore('region');
        const debugDeviceType: any = location.search.match(checkDevice);
        this.storageService.setLocalStore('debug_region', debugRegion);
        if (debugDeviceType !== undefined && debugDeviceType === 'iOS') {
          window.location.href = 'filmbox://region=tr&type=debug';
        }
      }
    }
    if (this.storageService.getLocalStore('debug_region')) {
      debugRegion = this.storageService.getLocalStore('debug_region');
    }
    if (!localeSetting) {
      localeSetting = this.configService.defaultLanguage;
    }
    if (deviceId) {
      if (userId) {
        this.paramurl = '/' + deviceId + '/' + userId;
        if (debugRegion !== '') {
          this.params = new HttpParams()
            .set('d_type', 'web')
            .set('logged_in', '1')
            .set('locale', localeSetting)
            .set('debug_region', debugRegion);
        } else {
          this.params = new HttpParams()
            .set('d_type', 'web')
            .set('logged_in', '1')
            .set('locale', localeSetting);
        }
      } else if (this.storageService.getLocalStore('g_id')) {
        this.paramurl = '/' + deviceId + '/' + guestId;
        if (debugRegion !== '') {
          this.params = new HttpParams()
            .set('d_type', 'web')
            .set('logged_in', '0')
            .set('locale', localeSetting)
            .set('debug_region', debugRegion);
        } else {
          this.params = new HttpParams()
            .set('d_type', 'web')
            .set('logged_in', '0')
            .set('locale', localeSetting);
        }
      }
      else {
        this.storageService.removeAll();
        window.location.reload();
      }
    } else {
      this.paramurl = '';
      if (debugRegion !== '') {
        this.params = new HttpParams()
          .set('d_type', 'web')
          .set('logged_in', '0')
          .set('locale', localeSetting)
          .set('debug_region', debugRegion);
      } else {
        this.params = new HttpParams()
          .set('d_type', 'web')
          .set('logged_in', '0')
          .set('locale', localeSetting);
      }
    }


    let api = apiUrl + 'api/' + version + settings;
    if (this.paramurl) {
      api = api + this.paramurl;
    }
    return new Promise((resolve, reject) => {
      return this.http.get<any>(api, { params: this.params })
        .subscribe(
          async (response) => {
            if (response.status_code === 200) {
              const respData = response.data;
              const settingsData = respData.settings;
              if (respData.user) {
                if (!this.storageService.getLocalStore('d_id') || !this.storageService.getLocalStore('g_id')) {
                  this.storageService.setLocalStore('d_id', respData.user.d_id);
                  this.storageService.setLocalStore('g_id', respData.user.g_id);
                }
              }
              await this.loadService.getToken();
              this.loadService.getSettings();
              this.versionService.setVersion(respData.version);
              // this.eventFun();
              this.settingsService.settingFn(respData.settings);
              this.footerService.pageDetailsFn(respData.pages);
              this.languageService.languageFn(respData.configuration.locales);
              this.baseService.allPages = respData.pages;
              const configData = respData.configuration;
              if (configData) {
                this.configService.loacaleFn(configData);
              }
              this.baseService.loginMode = respData.login_mode;
              this.baseService.guestSource.next(false);
              if (respData.login_mode === 0) {
                if (!userId) {
                  this.baseService.guestSource.next(true);
                }
                this.baseService.startState = '/home';
              } else {
                this.baseService.startState = '/index';
                if (!userId) {
                  this.router.navigateByUrl('/index');
                }
              }
              if (settingsData) {
                // Site theme color settings
                document.body.style.setProperty('--bg-color', settingsData.background_color);
                document.body.style.setProperty('--mm-color', settingsData.menu_color);
                document.body.style.setProperty('--card-color', settingsData.card_color);
                document.body.style.setProperty('--button-color', settingsData.button_color);
                document.body.style.setProperty('--footer-color', settingsData.footer_color);
                document.body.style.setProperty('--bg-font-color', settingsData.background_font_color);
                document.body.style.setProperty('--mm-font-color', settingsData.menu_font_color);
                document.body.style.setProperty('--card-font-color', settingsData.card_font_color);
                document.body.style.setProperty('--button-font-color', settingsData.button_font_color);
                document.body.style.setProperty('--footer-font-color', settingsData.footer_font_color);
                resolve(respData);
                // $("#header_scripts").html($rootScope.settings.header_scripts);
                // $("#body_scripts").html($rootScope.settings.body_scripts);
                // $("#google_analytics").html($rootScope.settings.google_analytics);
                $('head').append(respData.settings.google_analytics);
                $('head').append(respData.settings.header_scripts);
                $('body').prepend(respData.settings.body_scripts);
              }
            } else {
              this.uikitService.notifyError(response.error);
              resolve(response.error);
            }
          },
          (error) => {
            // error_codes 1011,1012
            console.log(error);
            if ((error.error.error.code === 1018 || error.error.error.code === 1019)) {
              this.loggerService.updatedMemoryStorage({});
              this.storageService.removeLocalStore('a_t');
              this.storageService.removeLocalStore('r_t');
              this.storageService.setLocalStore('logged_in', '0');
              this.storageService.removeLocalStore('u_id');
              this.storageService.removeLocalStore('sessionStorage');
              this.storageService.removeLocalStore('g_id');
              this.storageService.removeLocalStore('d_id');
              window.location.reload();
              this.uikitService.notifyError(error.error);
            } else {
              this.uikitService.notifyError(error.error);
            }
            resolve(error);
          }
        );
    });
  }
}


