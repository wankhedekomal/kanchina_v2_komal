import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  settings;
  enableLive;
  siteLogo;
  siteIcon;
  homeBgImage;
  bannerHeading;
  bannerDescription;
  aboutSite;
  appstore;
  playstore;
  facebookLink;
  linkedinLink;
  twitterLink;
  pinterestLink;
  instagramLink;
  siteName;
  enablePolling;
  commentSection;
  seoUrl;
  enablePartnerVoucher;
  isSvodEnabled;
  isTvodEnabled;
  routeUrl;
  imagePath;
  stripeKey;
  displayLandingLocale;
  socialLink;
  socialAvail;
  webLogoUrl;
  commonBg;
  homeBg;
  showHeader = true;
  showFooter = true;
  logo: any;
  homeImage: any;
  facebookSignin;
  googleSignin;
  googleAnalytics;
  headerScripts;
  bodyScripts;
  pageH1Tag;
  pageTitleNew;
  pageDescription;
  pageKeyword;
  seo = {};
  virtualUrl;


  constructor(private http: HttpClient, private location: Location, private route: ActivatedRoute,
              private storageService: StorageService) { }

  settingFn(data: object) {
    this.settings = data;
    this.enableLive = this.settings.enable_live_channels;
    this.siteIcon = this.settings.site_icon;
    this.homeBgImage = this.settings.home_page_bg_image;
    this.bannerHeading = this.settings.banner_heading;
    this.bannerDescription = this.settings.banner_description;
    this.aboutSite = this.settings.home_about_site;
    this.appstore = this.settings.appstore;
    this.playstore = this.settings.playstore;
    this.facebookLink = this.settings.facebook_link;
    this.linkedinLink = this.settings.linkedin_link;
    this.twitterLink = this.settings.twitter_link;
    this.pinterestLink = this.settings.pinterest_link;
    this.instagramLink = this.settings.instagram_link;
    this.siteName = this.settings.site_name;
    this.enablePolling = this.settings.enable_polling;
    this.commentSection = this.settings.comment_section;
    this.seoUrl = this.settings.seo_url;
    this.displayLandingLocale = this.settings.display_landing_locale;
    this.googleAnalytics = this.settings.google_analytics;
    this.headerScripts = this.settings.header_scripts;
    this.bodyScripts = this.settings.body_scripts;

    if (this.settings.firebase_web_settings !== '') {
      // this.firebaseConfig = JSON.parse(this.settings.firebase_web_settings);
      // firebase.initializeApp(this.firebaseConfig);
    }
    this.routeUrl = this.settings.angular_site_url;
    this.imagePath = this.settings.image_base_path;
    this.stripeKey = this.settings.stripe_key;
    this.webLogoUrl = this.imagePath + this.settings.web_logo;
    this.commonBg = this.imagePath + this.settings.common_bg_image;
    this.homeBg = this.imagePath + this.settings.home_page_bg_image;
    this.homeImage = this.imagePath + this.homeBgImage;
    this.logo = this.imagePath + this.settings.logo;
    this.siteLogo = this.imagePath + this.settings.web_logo;
    if (this.stripeKey) {
      // angular.module('tv2zApp').config(function (StripeElementsProvider) {
      //   StripeElementsProvider.setAPIKey(this.stripeKey);
      // });
    }
    this.enablePartnerVoucher = this.settings.enable_partner_voucher;
    this.isSvodEnabled = this.settings.is_svod;
    this.isTvodEnabled = this.settings.is_tvod;
    this.stripeKey = this.settings.stripe_key;
    this.socialLink = {
      facebook: data['facebook_link'],
      linkedin: data['linkedin_link'],
      twitter: data['twitter_link'],
      pinterest: data['pinterest_link'],
      instagram: data['instagram_link'],
      googlePlus: data['google_plus_link']
    };
    this.socialAvail = this.socialLink.facebook || this.socialLink.linkedin || this.socialLink.twitter
      || this.socialLink.pinterest || this.socialLink.instagram || this.socialLink.googlePlus;
    this.facebookSignin = this.settings.facebook_signin;
    this.googleSignin = this.settings.google_signin;
    this.getSeo();
  }

  get settingsData() {
    return this.settings;
  }

  slugify(input) {
    // make lower case and trim
    let slug = input.toLowerCase().trim();
    // replace invalid chars with spaces
    // slug = slug.replace(/[^a-z0-9\s-]/g, ' ');
    // replace multiple spaces or hyphens with a single hyphen
    slug = slug.replace(/[\s-]+/g, '-');
    return slug;
  }

  getSeo() {
    const region = this.storageService.getLocalStore('region')
    if (Object.keys(this.seo).length === 0) {
      let headers: Headers = new Headers({});
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Content-Type', 'application/json');
      // this.http.get(this.seo_url).pipe(
      //   map((response) => {
      //     console.log("status: ", response)
      //     if (response['status'] = '200') {
      //       this.seo = response['data'];
      //       var pathname = location.pathname;
      //       var seourl = pathname.replace("/" + region + "/", '');
      //       if (this.seo.hasOwnProperty(seourl)) {
      //         this.pageH1Tag = this.seo[seourl]["h1"];
      //         this.pageTitleNew = this.seo[seourl]["title"];
      //         this.pageDescription = this.seo[seourl]["description"];
      //         this.pageKeyword = this.seo[seourl]["keywords"];
      //       } else {
      //         this.pageH1Tag = this.seo["default"]["h1"];
      //         this.pageTitleNew = this.seo["default"]["title"];
      //         this.pageDescription = this.seo["default"]["description"];
      //         this.pageKeyword = this.seo["default"]["keywords"];
      //       }
      //     }
      //     else
      //       console.log("Seo File not found");
      //   }, (error: HttpErrorResponse) => {
      //     console.log("Seo File not found");
      //   })
      // )
    } else {
      const pathname = location.pathname;
      const seourl = pathname.replace('/' + region + '/', '');
      if (this.seo.hasOwnProperty(seourl)) {
        this.pageH1Tag = this.seo[seourl].h1;
        this.pageTitleNew = this.seo[seourl].title;
        this.pageDescription = this.seo[seourl].description;
        this.pageKeyword = this.seo[seourl].keywords;
      } else {
        this.pageH1Tag = this.seo['default']['h1'];
        this.pageTitleNew = this.seo['default']['title'];
        this.pageDescription = this.seo['default']['description'];
        this.pageKeyword = this.seo['default']['keywords'];
      }
    }
    let loginEvent = 'False';
    let userId = '';
    const autoUrl = this.route['_routerState']['snapshot']['url'];
    const path = this.location.path();
    const absUrl = (autoUrl === '') ? this.routeUrl : autoUrl;
    this.virtualUrl = absUrl.substring(absUrl.indexOf(path));
    if (this.storageService.getLocalStore('u_id')) {
      loginEvent = 'True';
      userId = this.storageService.getLocalStore('u_id');
    }
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
      event: 'GAVirtual',
      pagePath: this.virtualUrl,
      pageTitle: (this.pageTitleNew ? this.pageTitleNew : 'Filmbox'),
      userId,
      loginStatus: loginEvent
    });
  }


}
