import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { api, token } from 'src/app/shared/constants';
import { CategoriesService } from './categories.service';
import { LoggerService } from '../authentication/logger.service';
import { SettingsService } from './settings.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class LoadService {

  userId;
  deviceId;
  guestId;
  accessToken;
  memoryStorage;
  userType;
  dataLayer
  constructor(private http: HttpClient, private categoryService: CategoriesService, private storageService: StorageService,
    private loggerService: LoggerService, private settingsService: SettingsService) {

  }


  getToken() {
    this.eventFun();
    this.userId = this.storageService.getLocalStore('u_id');
    this.deviceId = this.storageService.getLocalStore('d_id');
    this.guestId = this.storageService.getLocalStore('g_id');
    const aT = this.storageService.getLocalStore('a_t');
    const rT = this.storageService.getLocalStore('r_t');
    return new Promise((resolve, reject) => {
      if (aT && rT) {
        resolve({ data: this.storageService.getLocalStore('a_t'), status_code: 200 });
      } else {
        let params: HttpParams = new HttpParams()
          .set('d_type', 'web')
          .set('d_id', this.deviceId);
        if (this.userId) {
          params = params.append('u_id', this.userId);
        } else {
          params = params.append('g_id', this.guestId);
        }
        return this.http.get(api + token, { params }).subscribe(response => {
          if (response['status_code'] === 200) {
            const respData = response['data'];
            this.accessToken = respData.access_token;
            this.storageService.setLocalStore('a_t', respData.access_token);
            this.storageService.setLocalStore('r_t', respData.refresh_token);
            this.categoryService.getCategories();
            resolve(response);
          } else {
            // error_codes 1009,1010
            resolve(response);
          }
        }, (error: HttpErrorResponse) => {
          console.log(error);
          reject(error);
        });
      }
    });
  }


  eventFun() {
    let loginEvent = 'False';
    let userId = '';
    const plateform = 'Desktop Site';
    let userType = 'Free User';
    this.loggerService.memoryStorage.subscribe(data => {
      if (data) {
        this.memoryStorage = data;
        this.userType = this.memoryStorage.user_type;
      }
    });
    if (this.storageService.getLocalStore('u_id')) {
      loginEvent = 'True';
      userId = this.storageService.getLocalStore('u_id');
      if (this.memoryStorage.user_type === '1' && this.memoryStorage.user_telco !== '') {
        userType = 'Operator User';
      } else {
        userType = 'Paid User';
      }
    }
    window.dataLayer.push({
      loginStatus: loginEvent,
      platform: plateform,
      userStatus: userType,
      country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
    });
    console.log(window.dataLayer);
  }

  getSettings() {
    if (!this.storageService.getLocalStore('d_id') || (!this.storageService.getLocalStore('g_id') && !this.storageService.getLocalStore('u_id'))) {
      window.location.reload();
    }
    return;
  }
}
