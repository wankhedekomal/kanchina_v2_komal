import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { StorageService } from './storage.service';
import { BaseService } from './base.service';
import { feedback, api } from 'src/app/shared/constants';


@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient) { }
  feedbackfun(data: object): Observable<any> {
    const feedbackUrl = api + feedback;
    return this.http.post(feedbackUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

}
