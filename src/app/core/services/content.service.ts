import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import {
  api, contentVideoAsset, contentPersonDataVideo, similarAsset, userContinueWatch,
  assetContentSeason, assetContentSeries, userHistory, watchCount, contentPersonDataSeries
} from 'src/app/shared/constants';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { UikitService } from './uikit.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  constructor(private baseService: BaseService, private http: HttpClient, private uikitService: UikitService,
    private storageService: StorageService) { }

  getCategoryDataById(id: string): Observable<any> {
    // this.settingsService.spinner = true;
    const contentURL = api + contentVideoAsset + id;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(contentURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
        return response['data']['content'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }
  getContentPersonData(id: string) {
    const contentURL = api + contentPersonDataVideo + id;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(contentURL, { params }).pipe(
      map(
        (response) => {
          if (response['status_code'] === 200) {
            return response['data'];
          } else {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
          }
        }
        , (error: HttpErrorResponse) => {
          // this.uikitService.notifyError(response)
          // $state.go('profile.404', {}, { reload: true });
        }
      )
    );
  }

  getSimilarAssestById(id: string) {
    const similarAssetURL = api + similarAsset + id;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(similarAssetURL, { params }).pipe(
      map(
        (response) => {
          if (response['status_code'] === 200) {
            return response['data'];
          } else {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
          }
        }
        , (error: HttpErrorResponse) => {
          // this.uikitService.notifyError(response)
          // $state.go('profile.404', {}, { reload: true });
        }
      )
    );
  }

  changeSeasonApi(id: string) {
    const changeSeasonURL = api + assetContentSeason + id;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(changeSeasonURL, { params }).pipe(
      map(
        (response) => {
          if (response['status_code'] === 200) {
            return response['data'];
          } else {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
          }
        }
        , (error: HttpErrorResponse) => {
          // this.uikitService.notifyError(response)
          // $state.go('profile.404', {}, { reload: true });
        }
      )
    );
  }

  getEpisodesApi(id: string): Observable<any> {
    const getEpisodesUrl = api + assetContentSeries + id;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(getEpisodesUrl, { params }).pipe(
      map(
        (response) => {
          if (response['status_code'] === 200) {
            return response['data'];
          } else {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
          }
        }
        , (error: HttpErrorResponse) => {
          // this.uikitService.notifyError(response)
          // $state.go('profile.404', {}, { reload: true });
        }
      )
    );
  }

  watchCountApi(id: any, contentType: any) {
    const watchCountUrl = api + watchCount;
    const userId: string = this.storageService.getLocalStore('u_id');
    const body = {
      content_id: id,
      u_id: userId,
      content_type: contentType,
      d_type: 'web'
    };
    return this.http.put(watchCountUrl, body).pipe(
      map((response) => {
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

  userHistoryApi(contentId: any) {
    const userHistoryUrl = api + userHistory;
    const userId: string = this.storageService.getLocalStore('u_id');
    const body = {
      content_id: contentId,
      u_id: userId,
      d_type: 'web'
    };
    return this.http.post(userHistoryUrl, body).pipe(
      map((response) => {
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

  userContinueWatchingApi(contentId: any, watchedTime: any) {
    const userContinueWatchingUrl = api + userContinueWatch;
    const userId: string = this.storageService.getLocalStore('u_id');
    const body = {
      u_id: userId,
      content_id: contentId,
      w_time: watchedTime,
      d_type: 'web'
    };
    return this.http.post(userContinueWatchingUrl, body).pipe(
      map((response) => {
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

  getSeriesPersonData(id: string) {
    const contentURL = api + contentPersonDataSeries + id;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(contentURL, { params }).pipe(
      map(
        (response) => {
          if (response['status_code'] === 200) {
            return response['data'];
          } else {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
          }
        }
        , (error: HttpErrorResponse) => {
          // this.uikitService.notifyError(response)
          // $state.go('profile.404', {}, { reload: true });
        }
      )
    );
  }
}
