import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api, forgotPassword } from 'src/app/shared/constants';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private http: HttpClient) { }

  forgotPassService(data): Observable<any>{
    const forgetPassword = api + forgotPassword;
    return this.http.put<any>(forgetPassword, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })

    );
  }
}
