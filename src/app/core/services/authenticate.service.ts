import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api, authentication } from 'src/app/shared/constants';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { StorageService } from './storage.service';


@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {



  constructor(private http: HttpClient, private baseService: BaseService, private storageService: StorageService) { }

  authenticate(): Observable<any> {
    const userId = this.storageService.getLocalStore('u_id');
    const authenticteUrl = api + authentication;
    const params = new HttpParams()
      .set('d_type', 'web')
      .set('u_id', userId);
    return this.http.get<any>(authenticteUrl, { params }).pipe(
      map((response: any) => {
        if (response.status_code === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })

    );
  }
}
