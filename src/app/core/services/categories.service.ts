import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api, assetsCategory, contents, catAssets, seriesAssest } from 'src/app/shared/constants';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { UikitService } from './uikit.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  categories;
  channelsList;
  constructor(private http: HttpClient, private uikitService: UikitService) { }

  getCategories(): Observable<any> {
    const homeURL = api + assetsCategory;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(homeURL, { params }).pipe(
      map(
        (response) => {
          if (response['status_code'] === 200) {
            const respData = response['data'];
            this.categories = respData.categories;
            if (respData.livechannels) {
              this.channelsList = respData.livechannels;
            }
            return response['data'];
          }
          else {
            this.uikitService.notifyError(response);
          }
        }), catchError((error) => {
          if (error.error && error.error.code !== 1002) {
            this.uikitService.notifyError(error);
          }
          return error;
        })
    );
  }

  getCategoryContents(catTitle) {
    const catgoryURL = api + catAssets + catTitle + contents;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(catgoryURL, { params })
      .pipe(
        map(
          (response) => {
            if (response['status_code'] === 200) {
              const respData = response['data'];
              return respData;
            }
            else {
              // this.uikitService.notifyError(response)
              // $state.go('profile.404', {}, { reload: true });
            }
          }
          , (error: HttpErrorResponse) => {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
          }
        )
      );
  }

  getSeriesContents(id) {
    const catgoryURL = api + seriesAssest + id;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(catgoryURL, { params })
      .pipe(
        map(
          (response) => {
            if (response['status_code'] === 200) {
              const respData = response['data'];
              return respData;
            }
            else {
              // this.uikitService.notifyError(response)
              // $state.go('profile.404', {}, { reload: true });
            }
          }
          , (error: HttpErrorResponse) => {
            // this.uikitService.notifyError(response)
            // $state.go('profile.404', {}, { reload: true });
          }
        )
      );
  }
}
