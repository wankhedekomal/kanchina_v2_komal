import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { api, search } from 'src/app/shared/constants';
import { catchError, map } from 'rxjs/operators';
import { UikitService } from './uikit.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient, private uikitService: UikitService) { }

  getData(key): Observable<any> {
    const params: HttpParams = new HttpParams()
      .set('d_type', 'web')
      .set('keyword', key)
      .set('skip', '0')
      .set('take', '12');
    const searchApiURL = api + search;
    return this.http.get(searchApiURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response['data'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

}
