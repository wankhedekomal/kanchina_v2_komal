import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api, chaneelsData } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChannelDataService {

  constructor(private http: HttpClient) { }

  getChanneldata(data: any): Observable<any> {
    const d_type: string = data.d_type;
    const skip: string = data.skip;
    const take: string = data.take;
    const pageId: string = data.id;
    const params: HttpParams = new HttpParams()
      .set('d_type', d_type)
      .set('skip', skip)
      .set('take', take);
    const wishlistURL = api + chaneelsData + pageId;
    return this.http.get(wishlistURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
