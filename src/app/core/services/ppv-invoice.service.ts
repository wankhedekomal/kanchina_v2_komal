import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { api, ppvInvoice, applyCoupen, checkoutTvod, ppvProceed, paypal, stripeCouponValidate } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';
import { UikitService } from './uikit.service';

@Injectable({
  providedIn: 'root'
})
export class PpvInvoiceService {

  constructor(private http: HttpClient, private uikitSetvice: UikitService) { }

  getInvoice(data: any): Observable<any> {
    const video_type: string = data.video_type;
    const content_id: string = data.content_id;
    const u_id: string = data.u_id;
    const d_type: string = data.d_type;
    const params: HttpParams = new HttpParams()
      .set('d_type', d_type)
      .set('u_id', u_id);
    const ppvUrl = api + ppvInvoice + video_type + '/' + content_id;
    return this.http.get(ppvUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  applyCoupen(data: any): Observable<any> {
    const contentType: string = data.content_type;
    const contentId: string = data.content_id;
    const coupon: string = data.coupon_code;
    const uId: string = data.u_id;
    const couponUrl = `${api}${stripeCouponValidate}?content_id=${contentType}&content_type=${contentId}&coupon_code=${coupon}&d_type=${'web'}&locale=${'en'}&region=${'int'}&u_id=${uId}`;
    return this.http.post(couponUrl, {}).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  checkout(data: any): Observable<any> {
    const checkoutUrl = api + checkoutTvod;
    return this.http.post(checkoutUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  ppvProceed(data: any): Observable<any> {
    const proccedUrl = api + ppvProceed;
    return this.http.post(proccedUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  ppvPayPal(data: any): Observable<any> {
    const paypalUrl = api + paypal;
    return this.http.post(paypalUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
