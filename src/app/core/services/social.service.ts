import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { api, token } from 'src/app/shared/constants';
import { StorageService } from './storage.service';
import { UikitService } from './uikit.service';

@Injectable({
  providedIn: 'root'
})
export class SocialService {

  constructor(private http: HttpClient, private uikitService: UikitService,private storageService:StorageService) { }

  socailToken(data): Observable<any> {
    let params: HttpParams = new HttpParams()
      .set('d_type', 'web')
      .set('d_id', data.d_id);
    if (data.u_id) {
      params = params.append('u_id', data.u_id);
    }
    else {
      params = params.append('g_id', data.g_id);
    }
    const getTokenURL = api + token;
    return this.http.get(getTokenURL, { params }).pipe(
      map((response) => {
        if (response["status_code"] === 200) {
          let respData = response["data"]
          this.storageService.setLocalStore('a_t', respData.access_token);
          this.storageService.setLocalStore('r_t', respData.refresh_token);
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

  userToken(): Observable<any> {
    const params: HttpParams = new HttpParams()
      .set('d_type', 'web');
    const getUserURL = api + token;
    return this.http.get(getUserURL, { params });
  }
}
