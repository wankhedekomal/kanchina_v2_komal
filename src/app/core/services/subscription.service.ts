import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { StorageService } from './storage.service';
import { api, userPlans, stripeResume, stripeChange, stripeCancel, stripeRedirect,
  stripeCheckout, stripeCouponValidate, applyCoupon, userPayment } from 'src/app/shared/constants';
import { UikitService } from './uikit.service';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  transactionDetails: any;
  constructor(private http: HttpClient, private storageService: StorageService, private uikitService: UikitService
 ) { }

  userPlan(): Observable<any> {
    const userId: string = this.storageService.getLocalStore('u_id');
    const subscriptionUrl = api + userPlans;
    const params = new HttpParams()
      .set('d_type', 'web')
      .set('u_id', userId);
    return this.http.get(subscriptionUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response['data'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }, (error: HttpErrorResponse) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
      })
    );
  }

  resumePlanApiFun(data: any): Observable<any> {
    const resumePlanApiFunUrl = api + stripeResume;
    return this.http.post(resumePlanApiFunUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          this.uikitService.notifySuccess(response);
          return true;
        }
        else {
          this.uikitService.notifyError(response);
        }
      }, (error: HttpErrorResponse) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
      })
    );
  }

  changePlanApiFun(data: any): Observable<any> {
    const changePlanApiFunUrl = api + stripeChange;
    return this.http.post(changePlanApiFunUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          this.uikitService.notifySuccess(response);
          return true;
        }
        else {
          this.uikitService.notifyError(response);
        }
      }, (error: HttpErrorResponse) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
      })
    );
  }

  cancelSubscriptionApiFun(data: any): Observable<any> {
    const cancelSubscriptionUrl = api + stripeCancel;
    return this.http.post(cancelSubscriptionUrl, data).pipe(
      map((response) => {
        return response;
      }, (error: HttpErrorResponse) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
      })
    );
  }

  saveTransactionDetails(data: any) {
    this.transactionDetails = data;
  }

  getTransactionDetails() {
    return this.transactionDetails;
  }

  updateMethodApi(): Observable<any> {
    const updateMethodUrl = api + stripeRedirect;
    const userId: string = this.storageService.getLocalStore('u_id');
    const data = { u_id: userId, d_type: 'web' };
    return this.http.post(updateMethodUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response['data'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }, (error: HttpErrorResponse) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
      })
    );
  }

  checkOut(data): Observable<any> {
    const checkOutUrl = api + stripeCheckout;
    return this.http.post(checkOutUrl, data);
  }

  invoiceUserPlan(data): Observable<any> {
    const userId: string = this.storageService.getLocalStore('u_id');
    const subscriptionUrl = api + userPlans + '/' + data.subscription_id;
    const params = new HttpParams()
      .set('d_type', 'web')
      .set('u_id', userId)
      .set('stripe_plan_id', data.stripe_plan_id);
    return this.http.get(subscriptionUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response['data'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }, (error: HttpErrorResponse) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
      })
    );
  }

  validateCoupon(data) {
    const subscriptionUrl = api + stripeCouponValidate;
    return this.http.post(subscriptionUrl, data).pipe(
      map((response) => {
        return response;
      }, (error: HttpErrorResponse) => {
        if (error.error && error.error.code !== 1002) {
          return error;
        }
      })
    );
  }

  applyCoupon(data): Observable<any> {
    const updateMethodUrl = api + applyCoupon;
    return this.http.post(updateMethodUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

  userPayments(): Observable<any> {
    const userPaymentURL = api + userPayment;
    const params = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(userPaymentURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response['data'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

}
