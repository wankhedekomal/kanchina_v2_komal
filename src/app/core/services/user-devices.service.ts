import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { apiUrl, version, api, userDevices } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserDevicesService {

  constructor(private http: HttpClient) { }

  getUserDevice(data: any): Observable<any> {
    const dType: string = data.d_type;
    const params = new HttpParams()
      .set('d_type', dType)
      .set('locale', 'en');
    const deviceUrl = api + userDevices;
    return this.http.get<any>(deviceUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  removeUserDevice(url: any): Observable<any> {
    const deviceUrl = api + url;
    return this.http.get<any>(deviceUrl).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

}
