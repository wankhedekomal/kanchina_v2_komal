import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api, homeAssets } from 'src/app/shared/constants';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { UikitService } from './uikit.service';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient, private uikitService: UikitService, private baseService: BaseService) { }

  assets(): Observable<any> {
    const homeAssetsURL = api + homeAssets;
    const params: HttpParams = new HttpParams()
      .set('d_type', 'web');
    return this.http.get(homeAssetsURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response['data'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }
}
