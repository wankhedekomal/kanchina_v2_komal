import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  pageList: Array<object>;
  guest: boolean;
  constructor() { }

  pageDetailsFn(pages: Array<object>) {
    this.pageList = pages;
  }

}
