import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SettingsService } from './settings.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  configuration;
  region;
  operatorLogin;
  locales;
  displayLandingLocale;
  availableLocale = ['en-US', 'bg-BG', 'hu-HU', 'cs-CZ', 'tr-TR', 'pl-PL', 'ro-RO', 'sk-SK'];
  defaultLanguage = 'en-US';
  locale;
  localeIndex;
  localeActive;
  guest;
  constructor(private settingsService: SettingsService, private translate: TranslateService, private router: Router,
              private storageService: StorageService
    ) {
  }

  loacaleFn(configuration) {
    this.configuration = configuration;
    this.region = this.configuration.region.region_code;
    this.operatorLogin = this.configuration.region.operator_login;
    this.locale = this.storageService.getLocalStore('locale');
    this.locales = configuration.locales || [];
    this.storageService.setLocalStore('region', this.configuration.region.region_code);
    this.displayLandingLocale = this.settingsService.displayLandingLocale;
    if (this.locale) {
      const localeAvailable = this.availableLocale.indexOf(this.locale);
      if (localeAvailable > -1) {
        this.translate.use(this.locale);
        this.defaultLanguage = this.locale;
      } else {
        this.translate.use(this.defaultLanguage);
      }
      this.locales.find((item, i) => {
        if (item.lang_code === this.defaultLanguage) {
          this.localeIndex = i;
          return i;
        }
      });
    } else {
      if (this.locales.length === 1) {
        const localeAvailable = this.availableLocale.indexOf(this.locales[0].lang_code);
        if (localeAvailable > -1) {
          this.translate.use(this.locales[0].lang_code);
          this.defaultLanguage = this.locales[0].lang_code;
        } else {
          this.translate.use(this.defaultLanguage);
        }
        this.localeIndex = 0;
      } else {
        if (this.displayLandingLocale === 0) {
          const localeAvailable = this.availableLocale.indexOf(this.configuration.region.locale);
          if (localeAvailable > -1) {
            this.storageService.setLocalStore('locale', this.configuration.region.locale);
            this.translate.use(this.configuration.region.locale);
            this.locale = this.configuration.region.locale;
          } else {
            this.storageService.setLocalStore('locale', this.defaultLanguage);
            this.translate.use(this.defaultLanguage);
            this.locale = this.defaultLanguage;
          }
          this.localeIndex = 0;
        } else {
          this.locales.find((item, i) => {
            if (item.is_default === 1) {
              this.localeIndex = i;
              return i;
            }
          });
          this.locale = this.locales[this.localeIndex].lang_code;
          if (!this.storageService.getLocalStore('locale')) {
            this.router.navigate(['/app-language', 'select']);
          }
        }
      }
    }
  }
}
