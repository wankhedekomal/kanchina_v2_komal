import { Injectable } from '@angular/core';
import { api, User, editUser, stripeRedirect } from 'src/app/shared/constants/url.constants';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { StorageService } from './storage.service';
import { UikitService } from './uikit.service';


@Injectable({
  providedIn: 'root'
})
export class MyAccountService {

  account_Url;

  constructor(private http: HttpClient, private storageservice: StorageService, private uikitService: UikitService
    ,private storageService: StorageService) { }

  getAccountDetails(): Observable<any> {
    const accountUrl = api + User;
    const params = new HttpParams();
    return this.http.get(accountUrl, { params }).pipe(
      map(
        (response) => {
          if (response['status_code'] === 200) {
            return response['data'];
          }
        }
        , (error: HttpErrorResponse) => {
          console.log(error);
        }
      )
    );
  }

  updateData(): Observable<any> {
    const updateMethodUrl = api + stripeRedirect;
    const userId: string = this.storageService.getLocalStore('u_id');
    const data = { u_id: userId, d_type: 'web' };
    return this.http.post(updateMethodUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response['data'];
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }
}
