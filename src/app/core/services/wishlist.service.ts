import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api, wishlist } from 'src/app/shared/constants';
import { HttpErrorResponse, HttpClient, HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { UikitService } from './uikit.service';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  constructor(private http: HttpClient, private uikitService: UikitService) { }

  addToWishlist(data: object): Observable<any> {
    const wishlistURL = api + wishlist;
    return this.http.post(wishlistURL, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }

  removeWishlist(paramsData: object): Observable<any> {
    const reqParamsData: object = paramsData['req'];
    const queryParamsData: object = paramsData['query'];
    const params: HttpParams = new HttpParams()
      .set('d_type', queryParamsData['d_type'])
      .set('u_id', queryParamsData['u_id']);
    const wishlistURL = api + wishlist + '/' + reqParamsData['content_type'] + '/' + reqParamsData['id'];
    return this.http.delete(wishlistURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  getWishlist(data: any): Observable<any> {
    const uId: string = data['u_id'];
    const dType: string = data['d_type'];
    const skip: string = data['skip'];
    const take: string = data['take'];
    const params: HttpParams = new HttpParams()
      .set('d_type', dType)
      .set('skip', skip)
      .set('take', take)
      .set('u_id', uId);
    const wishlistURL = api + wishlist;
    return this.http.get(wishlistURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }

  removeSeriesWishList(paramsData) {
    const reqParamsData: object = paramsData['req'];
    const queryParamsData: object = paramsData['query'];
    const params: HttpParams = new HttpParams()
      .set('d_type', queryParamsData['d_type'])
      .set('u_id', queryParamsData['u_id']);
    const wishlistURL = api + wishlist + '/series/' + reqParamsData['id'];
    return this.http.delete(wishlistURL, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
        else {
          this.uikitService.notifyError(response);
        }
      }), catchError((error) => {
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error);
        }
        return error;
      })
    );
  }
}
