import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api, User } from 'src/app/shared/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DeleteAccountService {

  constructor(private http: HttpClient) { }

  deletefun(data: any): Observable<any> {
    const userId: string = data.u_id;
    const d_type: string = data.d_type;
    const password: string = data.password;
    let params = new HttpParams()
      .set('u_id', userId)
      .set('d_type', d_type)
      .set('locale', 'en');
    if (password) {
      params = params.append('password', password);
    }
    const deleteUrl = api + User;
    return this.http.delete<any>(deleteUrl, { params }).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })
    );
  }
}
