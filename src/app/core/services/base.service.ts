import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { token } from 'src/app/shared/constants';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  loginMode: number;
  public guestSource = new BehaviorSubject<Boolean>(true);
  guest = this.guestSource.asObservable();
  public loaderSource = new BehaviorSubject<Boolean>(false);
  allPages;
  baseURL;
  isLive;
  startState: string;
  showIosApp: boolean;
  showAndroidApp: boolean;
  constructor(private http: HttpClient, private storageService: StorageService, private router: Router) { }

  getBaseUrl(url: string) {
    this.baseURL = url;
    return url;
  }


  fetchToken(userData: object) {
    const user_id: string = userData['u_id'];
    const device_id: string = userData['d_id'];
    const guest_id: string = userData['g_id'];
    let params = new HttpParams()
      .set('d_type', 'web')
      .set('d_id', device_id);
    if (user_id) {
      params = params.append('u_id', user_id);
    }
    else {
      params = params.append('g_id', guest_id);
    }
    const tokenURL = this.baseURL + token;
    return this.http.get(tokenURL, { params }).subscribe(response => {
      if (response['status_code'] === 200) {
        const respData = response['data'];
        this.storageService.setLocalStore('a_t', respData['access_token']);
        this.storageService.setLocalStore('r_t', respData['refresh_token']);
        this.router.navigateByUrl('/home');
      }
    }, (error: HttpErrorResponse) => {
      if (error['error']['error']['code'] === 1019) {
        this.storageService.removeLocalStore('u_id');
        this.storageService.removeLocalStore('d_id');
        this.storageService.removeLocalStore('g_id');
      }
    });
  }

  redirectTo(uri: string) {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate([uri]));
  }

  checkDevice() {
    const isMobile = {
      Android() {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry() {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      }
    };
    if (isMobile.iOS()) {
      this.showIosApp = true;
    }
    if (isMobile.Android()) {
      this.showAndroidApp = true;
    }
  }
}
