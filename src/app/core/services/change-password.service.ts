import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { api,changePassword } from 'src/app/shared/constants';
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  constructor(private http: HttpClient) { }

  changePasswordfun(data: any): Observable<any> {
    const chnagePasswordUrl = api + changePassword;
    return this.http.put<any>(chnagePasswordUrl, data).pipe(
      map((response) => {
        if (response['status_code'] === 200) {
          return response;
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
      })

    );
  }
}
