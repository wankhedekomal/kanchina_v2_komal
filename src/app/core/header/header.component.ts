import { Component, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from '../services';
import { ConfigurationService } from '../services/configuration.service';
import { LoggerService } from '../authentication/logger.service';
import { CategoriesService } from '../services/categories.service';
import { ActivatedRoute } from '@angular/router';
import { filter, first, last } from 'rxjs/operators';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { SearchComponent } from 'src/app/shared/components';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  guest;
  operatorLogin;
  userPicture;
  userName;
  showIosApp;
  showAndroidApp;
  categories;
  channelsList;
  selectedLocale;
  selectedLocaleKey;
  categorySelected = false;
  locales;
  localeLength;
  siteLogo;
  imagePath;
  catName;
  categoryInfo;
  isLive;
  enableLive;
  categoryId: any;
  memoryStorage;
  logo: any;
  constructor(public settingsService: SettingsService, private baseService: BaseService,
              private configService: ConfigurationService, private loggerService: LoggerService,
              private categoryService: CategoriesService, private route: ActivatedRoute, public dialog: MatDialog,
              private translate: TranslateService, private storageService: StorageService) {
    this.baseService.guest.subscribe(data => this.guest = data);
    this.operatorLogin = this.configService.operatorLogin;
    this.loggerService.memoryStorage.subscribe(data => {
      if (data) {
        this.memoryStorage = data;
        this.userPicture = (this.memoryStorage.user_picture !== ''
          && this.memoryStorage.user_picture !== undefined) ? this.memoryStorage.user_picture : '';
        this.userName = (this.memoryStorage.user_name !== '' &&
          this.memoryStorage.user_name !== undefined) ? this.memoryStorage.user_name : '';
        if (!this.userPicture) { } else {
          this.userPicture = this.settingsService.imagePath + this.userPicture;
        }
      }
    });
    if (this.storageService.getLocalStore('a_t') && this.storageService.getLocalStore('r_t')) {
      this.categoryService.getCategories().subscribe(
        (data) => {
          if (data) {
            this.categories = data.categories;
            if (data.livechannels) {
              this.channelsList = data.livechannels;
            }
            // Friendly url
            // var headCatReload = {}
            // angular.forEach(res.data.categories, function (value, key) {
            //   var slug = slugify(value.title);
            //   res.data.categories[key]['slug'] = slug;
            //   headCatReload[slug] = value.title;
            // });
            // if (Object.keys(headCatReload).length)
            //   this.storageService.setLocalStore("headCatReload", JSON.stringify(headCatReload))
          }
        });
    }
    this.selectedLocale = this.configService.locales[this.configService.localeIndex].lang_name || 'Select Locale';
    this.selectedLocaleKey = this.configService.locales[this.configService.localeIndex].lang_code;
    this.locales = this.configService.locales;
    this.localeLength = this.locales.length;
    this.imagePath = this.settingsService.imagePath;
    this.siteLogo = this.settingsService.siteLogo;
    this.isLive = this.baseService.isLive;
    this.enableLive = this.settingsService.enableLive;
    this.logo = this.settingsService.logo;
  }

  ngOnInit(): void {
    this.baseService.checkDevice();
    this.showAndroidApp = this.baseService.showAndroidApp;
    this.showIosApp = this.baseService.showIosApp;
    if (!this.showAndroidApp && !this.showIosApp && this.settingsService.displayLandingLocale) {
      if (this.configService.locales.length > 1) {
        let locale = this.storageService.getLocalStore('locale')
        if (!locale) {
          this.configService.localeActive = true;
          // $('.overlay').css('overflow-y', 'auto');
          // $('.overlay').css('display', 'block');
          // $('body').css('overflow-y', 'hidden');
          // $('body').css('height', '100%');
        } else {
          this.configService.localeActive = false;
        }
      }
    } else {
      this.configService.localeActive = false;
    }
    this.route.queryParams
      .pipe(filter(params => params.category_id))
      .subscribe(params => {
        this.categoryId = params.category_id;
      });
  }

  logout() {
    this.loggerService.logout();
  }

  hoverIn(value) {
    // $(".show-categories-onhover").css('display', 'block');
    if (value === 'live') {
      this.categoryInfo = this.categoryService.channelsList;
      this.catName = 'live';
      // angular.forEach(this.categoryInfo, function (value, key) {
      //   var slug = slugify(value.title);
      //   this.categoryInfo[key]['slug'] = slug;
      // });
    } else if (value === 'sub') {
      if (this.categoryInfo) {
        this.catName = this.categoryInfo.title;
        // this.category = this.categoryInfo.slug;
        // angular.forEach(this.categoryInfo.collections, function (value, key) {
        //   var slug = slugify(value.title);
        //   this.categoryInfo.collections[key]['slug'] = slug;
        // });
      }
    } else {
      this.catName = value.title;
      this.categoryInfo = value;
      // var slug = slugify(value.title);
      // this.catName = slug;
    }
    this.categorySelected = true;
  }
  hoverOut = function() {
    this.catName = '';
    this.categorySelected = false;
  };

  hoverOutClass = function() {
    this.catName = '';
    this.categorySelected = false;
  };

  setLocale(i, value) {
    this.selectedLocale = value.lang_name;
    this.selectedLocaleKey = value.lang_code;
    this.configService.localeIndex = i;
    this.translate.use(this.selectedLocaleKey);
    this.storageService.setLocalStore('locale', this.selectedLocaleKey);
    this.configService.locale = this.selectedLocaleKey;
    window.location.reload();
  }

  setTitleValue(catName) {

  }

  scrollToRight() {

  }
  scrollToLeft() {

  }

  searchShow() {
    const dialogRef = this.dialog.open(SearchComponent, {
      panelClass: 'my-full-screen-dialog',
      data: {},
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
