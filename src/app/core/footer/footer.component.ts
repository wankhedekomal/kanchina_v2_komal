import { Component, OnInit } from '@angular/core';
import { BaseService, SettingsService } from '../services';
import { FooterService } from '../services/footer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  socialLink: object;
  appstore: string;
  playstore: string;
  facebookLink: string;
  twitterLink: string;
  linkedinLink: string;
  instagramLink: string;
  pinterestLink: string;
  guest;
  pageList;
  socialAvailable: string;
  logo: any;
  socialLength;

  constructor(public settingsService: SettingsService, public footerService: FooterService, private router: Router,
              private baseService: BaseService) { }

  ngOnInit(): void {
    this.socialLink = this.settingsService.socialLink;
    this.appstore = this.settingsService.appstore;
    this.playstore = this.settingsService.playstore;
    this.facebookLink = this.settingsService.facebookLink;
    this.twitterLink =  this.settingsService.twitterLink;
    this.linkedinLink = this.settingsService.linkedinLink;
    this.instagramLink = this.settingsService.instagramLink;
    this.pinterestLink = this.settingsService.pinterestLink;
    this.socialAvailable = (this.facebookLink || this.twitterLink || this.linkedinLink || this.pinterestLink || this.instagramLink);
    this.socialLength = this.socialAvailable.length;
    this.pageList = this.footerService.pageList;
    this.logo = this.settingsService.logo;
    this.baseService.guest.subscribe(data => this.guest = data);
  }

  goToFooter(page: any){
    this.router.navigate(['/pages', page.type]);
  }

}
