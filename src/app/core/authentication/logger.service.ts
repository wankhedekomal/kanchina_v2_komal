import { Injectable } from '@angular/core';
import { BaseService, StorageService } from '../services';
import { Router } from '@angular/router';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { refreshToken, login, Register, api, logout } from 'src/app/shared/constants';
import { UikitService } from '../services/uikit.service';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  memorySource = new BehaviorSubject<any>({});
  memoryStorage = this.memorySource.asObservable();
  constructor(private http: HttpClient, private baseService: BaseService,
              private router: Router, private storageService: StorageService, private uikitService: UikitService,
              private translate: TranslateService
  ) {
    let sessionStorage = this.storageService.getLocalStore('sessionStorage');
    this.memorySource.next(sessionStorage);
    window.dataLayer = window.dataLayer || [];
  }

  updatedMemoryStorage(data) {
    this.memorySource.next(data);
  }

  loginFn(data) {
    const loginAPI: string = api + login;
    this.http.post<any>(loginAPI, data).subscribe(
      (response) => {
        if(response.status_code === 200){
          let respData = response["data"];
          let u_id = respData["user_id"];
          this.storageService.setLocalStore('a_t', respData["access_token"])
          this.storageService.setLocalStore('r_t', respData["refresh_token"])
          this.storageService.setLocalStore('u_id', u_id)
          this.storageService.setLocalStore('logged_in', respData["logged_in"])
          let updateMemoryData = {
            login_by: respData["user_login_by"],
            user_picture: respData["user_picture"],
            user_name: respData["user_name"],
            user_type: respData["user_type"],
            one_time_subscription: respData["one_time_subscription"],
            u_id: u_id
          };
          this.memorySource.next(updateMemoryData)
          this.storageService.setLocalStore('sessionStorage', updateMemoryData)
          this.baseService.guestSource.next(false);
          window.dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': 'Membership Actions',
            'eventAction': 'Login',
            'eventLabel': 'Success - Username',
            'loginStatus': 'True',
            'userId': u_id,
            'notificationStatus': 'True',
            'country': (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            'plateform': "Web"
          });
          let loginSucessMsg
          this.translate.get('login_msg_login_succ_def').subscribe(data => loginSucessMsg = data)
          this.uikitService.staticSuccessMessage(loginSucessMsg);
          let authenticate = this.storageService.getLocalStore('authenticate')
          if (authenticate) {
            this.storageService.removeLocalStore('authenticate');
            this.router.navigateByUrl('/authenticte')
          } else {
            this.router.navigateByUrl('/home')
          }
        } else{
          window.dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': 'Membership Actions',
            'eventAction': 'Login',
            'eventLabel': 'Error - Username',
            'loginStatus': 'False',
            'userId': '',
            'notificationStatus': 'False',
            'country': (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            'plateform': "Web"
          });
        }
      },
      (error: HttpErrorResponse) => {
        window.dataLayer.push({
          event: 'GAEvent',
          eventCategory: 'Membership Actions',
          eventAction: 'Login',
          eventLabel: 'Error - Username',
          loginStatus: 'False',
          userId: '',
          notificationStatus: 'False',
          country: (this.storageService.getLocalStore('region') ?this.storageService.getLocalStore('region') : ''),
          plateform: 'Web'
        });
        this.uikitService.staticErrorMessage(error.error.error.message);
        console.log(error);
      }
    );
  }

  signUpFn(data) {
    let signUpAPI = api + Register;
    const region = this.storageService.getLocalStore('region')
    this.http.post<any>(signUpAPI, data).subscribe(
      (response) => {
        if (response.status_code === 200) {
          window.dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'Membership Actions',
            eventAction: 'Register',
            eventLabel: 'Success',
            loginStatus: 'True',
            userId: response.data.user_id,
            notificationStatus: 'True',
            country: (region ? region : ''),
            plateform: 'Web'
          });
          console.log(window.dataLayer)
          if (response.data.verifyEmail == 1) {
            this.storageService.setLocalStore('mail', data.email);
            this.uikitService.notifySuccess(response);
            this.router.navigateByUrl('/confirm-account');
          }
          else {
            let respData = response['data']
            let u_id = respData['u_id']
            this.storageService.setLocalStore('a_t', respData['access_token'])
            this.storageService.setLocalStore('r_t', respData['refresh_token'])
            this.storageService.setLocalStore('u_id', u_id)
            this.storageService.setLocalStore('logged_in', respData['logged_in'])
            let updateMemoryData = {
              login_by: respData['user_login_by'],
              user_picture: respData['user_picture'],
              user_name: respData['user_name'],
              user_type: respData['user_type'],
              one_time_subscription: respData['one_time_subscription'],
              u_id: respData['user_id']
            }
            this.memorySource.next(updateMemoryData)
            this.storageService.setLocalStore('sessionStorage', updateMemoryData)
            // $rootScope.guest = false;
            // UIkit.notify({ message: res.message, timeout: 3000, pos: 'top-center', status: 'success' });
            this.router.navigateByUrl('/home');
          }
        } else {
          window.dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'Membership Actions',
            eventAction: 'Register',
            eventLabel: 'Error - Username',
            loginStatus: 'False',
            userId: '',
            notificationStatus: 'False',
            country: (region ? region : ''),
            plateform: 'Web'
          });
        }
      },
      (error: HttpErrorResponse) => {
        this.uikitService.staticErrorMessage(error.error.error.message);
        console.log(error);
      }
    );
  }

  refreshToken() {
    let refreshAPI = api + refreshToken;
    let d_id: string = this.storageService.getLocalStore('d_id');
    let g_id: string = this.storageService.getLocalStore('g_id');
    let u_id: string = this.storageService.getLocalStore('u_id');
    let params: HttpParams = new HttpParams()
      .set('d_type', 'web')
      .set('d_id', d_id)
    if (u_id)
      params = params.append('u_id', u_id)
    else
      params = params.append('g_id', g_id)
    this.http.get(refreshAPI, { params: params }).subscribe(response => {
      if (response['status_code'] === 200) {
        let respData = response['data'];
        this.storageService.setLocalStore('a_t', respData['access_token'])
        this.storageService.setLocalStore('r_t', respData['refresh_token'])
      }
    }, (error: HttpErrorResponse) => {
      console.log(error);
    });
  }

  logout() {
    let logoutMsg;
    this.translate.get('menu_msg_logout_def').subscribe(data => {
      logoutMsg = data;
    })
    const d_id: string = this.storageService.getLocalStore('d_id');
    const u_id: string = this.storageService.getLocalStore('u_id');
    const region: string = this.storageService.getLocalStore('region');
    let logoutAPI: string = api + logout + d_id;
    let params: HttpParams = new HttpParams()
      .set('d_type', 'web')
    this.http.get(logoutAPI, { params }).subscribe(response => {
      if (response['status_code'] === 200) {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
          event: 'GAEvent',
          eventCategory: 'Membership Actions',
          eventAction: 'Logout',
          eventLabel: 'Success',
          loginStatus: (u_id ? 'True' : 'False'),
          userId: (u_id ? u_id : ''),
          notificationStatus: 'False',
          country: (region ? region : ''),
          plateform: 'Web'
        });
        console.log(window.dataLayer);
        this.uikitService.notifySuccess(response)
        this.memorySource.next({})
        this.storageService.removeLocalStore('a_t');
        this.storageService.removeLocalStore('r_t');
        this.storageService.removeLocalStore('u_id');
        this.storageService.setLocalStore('logged_in', '0');
        this.storageService.removeLocalStore('sessionStorage');
        if (this.baseService.loginMode == 1)
          this.router.navigateByUrl('/index')
        else {
          this.baseService.guestSource.next(true);
          this.router.navigateByUrl('/home');
        }
      } else {
        this.uikitService.notifyError(response);
      }
    }, (error: HttpErrorResponse) => {
      if (error && error.error.error.code !== 1002) {
        this.uikitService.notifyError(error);
      }
    });
  }


  errorLogout() {
    this.memorySource.next({})
    this.storageService.removeLocalStore('a_t');
    this.storageService.removeLocalStore('r_t');
    this.storageService.setLocalStore('logged_in', '0');
    this.storageService.removeLocalStore('u_id');
    this.storageService.removeLocalStore('sessionStorage');
    if (this.baseService.loginMode == 1) {
      this.router.navigateByUrl('/index')
    } else {
      this.router.navigateByUrl('/home');
      this.baseService.guestSource.next(true);
    }
  }
}
