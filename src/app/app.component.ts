import { AfterViewInit, ChangeDetectorRef, Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, RouterEvent, NavigationStart, NavigationEnd, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
import { BaseService, SettingsService } from './core/services';
import { ConfigurationService } from './core/services/configuration.service';
import { AnimationOptions } from 'ngx-lottie';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'Tv2z-app';
  showAndroidApp: boolean;
  showIosApp: boolean;
  loading;
  options: AnimationOptions = {
    path: '/assets/loader.json',
  };

  constructor(private translate: TranslateService, private router: Router, public settingsService: SettingsService,
              private baseService: BaseService, private configService: ConfigurationService, private cdRef: ChangeDetectorRef) {
    const defaultLanguage = this.configService.defaultLanguage;
    this.showAndroidApp = this.baseService.showAndroidApp;
    this.showIosApp = this.baseService.showIosApp;
    this.translate.setDefaultLang(defaultLanguage);
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)).subscribe(
        (event: RouterEvent) => {
          this.settingsService.getSeo();
          window.scrollTo(0, 0);
          if (event instanceof RouteConfigLoadStart) {
            this.baseService.loaderSource.next(true);
          } else if (event instanceof RouteConfigLoadEnd) {
            this.baseService.loaderSource.next(false);
          }
        });
    this.baseService.loaderSource.subscribe(data => {
      this.loading = data;
    });
  }


  ngAfterViewInit() {
    this.cdRef.detectChanges();
  }

}
