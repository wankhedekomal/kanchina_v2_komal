import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MobileAppComponent, NavigationComponent, ReadMoreComponent, SlickItemComponent, SigninPopupComponent, SearchComponent } from '../components';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazyImgDirective } from '../directives';


@NgModule({
  declarations: [MobileAppComponent, NavigationComponent, ReadMoreComponent, SearchComponent, SlickItemComponent, SigninPopupComponent,
  LazyImgDirective],
  imports: [
    CommonModule,
    RouterModule,
    SlickCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
        deps: [HttpClient]
      }
    })
  ],
  exports: [MobileAppComponent, NavigationComponent, ReadMoreComponent, SearchComponent, SlickItemComponent, SigninPopupComponent,
     LazyImgDirective
  ],
})
export class SharedModule {}
