import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { SettingsService } from 'src/app/core/services';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/core/services/search.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
export interface DialogData {}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  slideToShow: number;
  slideToScroll: number;
  slideToShowSeries: number;
  slideToScrollSeries: number;
  windowWidth: number;
  searchKey;
  searchActive: boolean;
  data: any[];
  searchCount;
  noResultsMessage: string;
  searchOpenedFirst = true;
  imagePath: any;

  constructor(public settingsService: SettingsService, private router: Router,
              private searchService: SearchService, public dialogRef: MatDialogRef<SearchComponent>,
              @Inject(MAT_DIALOG_DATA) public dialogData: DialogData) {
    this.settingsService.showHeader = false;
    this.settingsService.showFooter = false;
  }

  ngOnInit(): void {
    this.imagePath = this.settingsService.imagePath;
    this.windowWidth = window.innerWidth;
  }

  closeSearch() {
    this.searchActive = false;
    this.data = [];
    this.searchCount = '';
    this.searchKey = undefined;
    this.noResultsMessage = '';
    this.dialogRef.close();
  }

  getSearchData(key) {
    this.searchOpenedFirst = false;
    if (key.length >= 3) {
      this.searchService.getData(key).subscribe(
        (data) => {
          if (data) {
            this.data = data.contents;
            this.searchCount = data.total;
            if (this.searchCount === 0) {
              this.noResultsMessage = 'No search results Found';
            }
          }
        }
      );
    } else {
      if (this.data && this.data.length > 0) {
        this.data = [];
        this.searchCount = 0;
      }
    }
  }

  goToSearchPage = function(term) {
    this.searchService.getData(term).subscribe(
      (data) => {
        if (data) {
          this.data = data.contents;
          this.searchCount = data.total;
          if (this.searchCount === 0) {
            this.noResultsMessage = 'No search results Found';
          }
        }
      }
    );
  };

  ngOnDestroy() {
    this.settingsService.showHeader = true;
    this.settingsService.showFooter = true;
    this.closeSearch();
  }
}
