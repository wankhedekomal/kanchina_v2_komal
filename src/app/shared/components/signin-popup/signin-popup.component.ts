import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin-popup',
  templateUrl: './signin-popup.component.html',
  styleUrls: ['./signin-popup.component.scss']
})
export class SigninPopupComponent implements OnInit {

  @Input() back: boolean;
  @Input() close: boolean;

  constructor(private location: Location, private loggerService: LoggerService, private router: Router) { }

  ngOnInit(): void {
  }

  signIn = () => {
    document.getElementById('signinclose').click();
    this.loggerService.updatedMemoryStorage({});
    this.router.navigateByUrl('/signin');
  }

  goBack = () => {
    document.getElementById('signinclose').click();
    this.location.back();
  }

}
