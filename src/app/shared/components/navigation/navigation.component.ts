import { Component, Input, OnInit } from '@angular/core';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  startState: any;
  @Input() routes;
  @Input() value;
  @Input() guest;
  logo;

  constructor(private baseService: BaseService, private settingsService: SettingsService, private storageService: StorageService) {
    this.logo = this.settingsService.logo;
  }

  ngOnInit(): void {
    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
  }
}
