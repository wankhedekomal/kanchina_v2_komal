import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { BaseService, SettingsService } from 'src/app/core/services';

@Component({
  selector: 'app-mobile-app',
  templateUrl: './mobile-app.component.html',
  styleUrls: ['./mobile-app.component.scss']
})
export class MobileAppComponent implements OnInit, OnDestroy, AfterViewInit {

  showIosApp: boolean;
  showAndroidApp: boolean;
  showPopup: boolean;
  logo: any;

  constructor(private settingsService: SettingsService, private baseService: BaseService) {
    this.logo = this.settingsService.logo;
  }

  ngOnInit(): void {
    this.baseService.checkDevice();
    this.showAndroidApp = this.baseService.showAndroidApp;
    this.showIosApp = this.baseService.showIosApp;
    this.showPopup = true;
    this.closePopUp();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.showIosApp || this.showAndroidApp) {
        this.settingsService.showHeader = false;
        this.settingsService.showFooter = false;
      }
    });
  }

  closePopUp = function() {
    this.showPopup = false;
  };

  ngOnDestroy() {
    this.settingsService.showFooter = true;
    this.settingsService.showHeader = true;
  }

}
