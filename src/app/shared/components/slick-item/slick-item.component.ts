import { Component, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { SettingsService, StorageService } from 'src/app/core/services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-slick-item',
  templateUrl: './slick-item.component.html',
  styleUrls: ['./slick-item.component.scss']
})
export class SlickItemComponent implements OnInit {

  imagePath;
  cardConfig;
  seriesConfig;
  slideToShow;
  slideToScroll;
  slideToShowSeries;
  slideToScrollSeries;
  value;
  windowWidth;
  barwidth;
  searchKey;
  searchActive: boolean;
  datas: any[];
  dataList;
  @Input() data;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Input() categoryName;
  @Input() searchData;

  constructor(private settingsService: SettingsService, private router: Router, private storageService: StorageService) {
    this.getAdjustCards();
    this.imagePath = this.settingsService.imagePath;
    this.cardConfig = {
      centerMode: false,
      infinite: false,
      slidesToShow: (this.slideToShow - 1),
      variableWidth: true,
      slidesToScroll: (this.slideToScroll - 1)
    };
    this.seriesConfig = {
      centerMode: false,
      infinite: false,
      slidesToShow: (this.slideToShowSeries - 1),
      variableWidth: true,
      slidesToScroll: (this.slideToScrollSeries - 1)
    };
  }

  ngOnInit(): void {
    if (this.data.list) {
      this.dataList = this.data.list;
    } else if (this.data.contents) {
      this.dataList = this.data.contents;
    } else if (this.data.videos) {
      this.dataList = this.data.videos;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.getAdjustCards();
  }

  getAdjustCards() {
    this.windowWidth = window.innerWidth;
    switch (true) {
      case this.windowWidth > 1800:
        this.slideToShow = 6;
        this.slideToScroll = 5;
        this.slideToShowSeries = 7;
        this.slideToScrollSeries = 7;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 1365 && this.windowWidth < 1801):
        this.slideToShow = 5;
        this.slideToScroll = 4;
        this.slideToShowSeries = 6;
        this.slideToScrollSeries = 6;
        this.barwidth = 0.85;
        break;

      case (this.windowWidth > 1023 && this.windowWidth < 1366):
        this.slideToShow = 4;
        this.slideToScroll = 3;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 4;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 992 && this.windowWidth < 1024):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 4;
        this.barwidth = 0.65;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.slideToShow = 2;
        this.slideToScroll = 1;
        this.slideToShowSeries = 3.5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.44;
        break;
      case (this.windowWidth < 480):
        this.slideToShow = 1.5;
        this.slideToScroll = 2;
        this.slideToShowSeries = 2.5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.70;
        break;
      case (this.windowWidth < 360):
        this.slideToShow = 1;
        this.slideToScroll = 1;
        this.slideToShowSeries = 1.8;
        this.slideToScrollSeries = 2;
        this.barwidth = 0.70;
        break;
      default:
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.85;
        break;
    }
  }

  redirect = (video, key, title) => {
    this.closeSearch();
    const vedioName = this.settingsService.slugify(video.title);
    window.dataLayer = window.dataLayer || [];
    if (this.categoryName) {
      window.dataLayer.push({
        event: 'GAEvent',
        eventCategory: 'Content Click',
        eventAction: this.categoryName,
        eventLabel: video.title,
        loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
        content: video.title,
        VideoCategory: this.categoryName,
        notificationStatus: 'False',
        country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
        plateform: 'Web'
      });
      console.log(window.dataLayer);
    }
    else if (this.searchData) {
      window.dataLayer.push({
        event: 'GAEvent',
        eventCategory: 'Search',
        eventAction: this.searchData,
        eventLabel: this.settingsService.virtualUrl,
        loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        userId: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        content: video.title,
        VideoCategory: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
        notificationStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
        plateform: 'Web'
      });
      console.log(window.dataLayer);
    }
    else {
      window.dataLayer.push({
        event: 'GAEvent',
        eventCategory: 'Content Click',
        eventAction: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
        eventLabel: video.title,
        loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
        notificationStatus: 'False',
        country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
        content: video.title,
        VideoCategory: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
        plateform: 'Web'
      });
      console.log(window.dataLayer);
    }
    if (video.is_livechannel) {
      this.router.navigate(['/live-video', vedioName, video.content_id]);
    } else if (key === 'popular_series' || key === 'series' || video.is_series) {
      this.router.navigate(['/series', vedioName, video.content_id]);
    } else if (key === 'video' && title === 'VOD') {
      this.router.navigate(['/player', 'video', video.content_id]);
    } else {
      this.router.navigate(['/video', vedioName, video.content_id]);
    }
  }

  closeSearch() {
    this.closeEvent.emit(true);
  }

}
