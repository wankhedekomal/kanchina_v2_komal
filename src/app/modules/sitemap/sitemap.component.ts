import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/core/services';

@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss']
})
export class SitemapComponent implements OnInit, OnDestroy {

  constructor(private settingsService: SettingsService) {
    this.settingsService.showHeader = false;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(){
    this.settingsService.showHeader = true;
  }

}
