import { Component, OnInit } from '@angular/core';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { SubCategoryService } from 'src/app/core/services/sub-category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {
  windowWidth: number;
  seriesWidth: number;
  videosWidth: number;
  datas = [];
  datasLength = 0;
  noContent = '';
  executed = 0;
  guest: any;
  imagePath: any;
  orientation;
  title;
  cat;
  homeImage: any;
  catTitle: any;
  constructor(private baseService: BaseService, private subCategoryService: SubCategoryService,
              public settingsService: SettingsService, private route: ActivatedRoute, private router: Router,
              private storageService: StorageService) {
    this.windowWidth = window.innerWidth - 40;
    this.seriesWidth = (Math.floor(this.windowWidth / 255) * 255);
    this.videosWidth = (Math.floor(this.windowWidth / 334) * 334);
    this.baseService.guest.subscribe(data => this.guest = data);
    this.imagePath = this.settingsService.imagePath;
    this.homeImage = this.settingsService.homeImage;
    this.baseService.loaderSource.next(true);
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {
        this.datasLength = 0;
        if (Object.keys(params).length) {
          this.cat = params.sub_cat_id;
          this.catTitle = params.title;
          this.datas = [];
          this.detailsFn(0, 16);
        }
      });
  }


  // $(window).scroll(function() {
  //   if ($(window).scrollTop() == $(document).height() - $(window).height()) {
  //     // ajax call get data from server and append to the div
  //     $("#load_more_details").click();
  //   }
  // });

  detailsFn = function(skip, take) {
    // var arr = {}
    // var homeCatReload = JSON.parse(this.storageService.getLocalStore('homeCatReload'))
    // var headCatReload = JSON.parse(this.storageService.getLocalStore('headCatReload'))
    // if (homeCatReload && headCatReload)
    //   arr = Object.assign(homeCatReload, headCatReload);
    // else if (homeCatReload)
    //   arr = homeCatReload;
    // else if (headCatReload)
    //   arr = headCatReload;
    // if ($stateParams.category) {
    //   for (var k in arr) {
    //     if ($stateParams.category == k) {
    //       var catID = arr[k];
    //     }
    //   }
    // }
    // if ($stateParams.cat) {
    //   for (var k in arr) {
    //     if ($stateParams.cat == k) {
    //       var catID = arr[k];
    //     }
    //   }
    // }
    const SubCatdata = { guest: this.guest, cat: this.cat, title: this.catTitle, skip, take };
    this.subCategoryService.getSubCategory(SubCatdata).subscribe((data) => {
      if (data) {
        this.baseService.loaderSource.next(false);
        if (data.contents.length > 0) {
          this.executed = 1;
          this.title = data.title;
          this.orientation = data.orientation;
          if (this.datas.length > 0) {
            this.datas = this.datas.concat(data.contents);
          }
          else {
            this.datas = data.contents;
          }
          if (this.datas.length === 0) {
            this.noContent = 'No content found!';
          }
        } else {
          this.executed = 0;
          if (this.datas.length === 0) {
            this.noContent = 'No content found!';
          }
        }
        this.datasLength = this.datas.length;
      }
    });
  };


  loadMoreDetails = function() {
    const dataLength = this.datas.length;
    if (dataLength > 0 && this.executed === 1) {
      this.executed = 0;
      this.detailsFn(dataLength, 16);
    }
  };

  redirect(video) {
    const vedioName = this.settingsService.slugify(video.title);
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
      event: 'GAEvent',
      eventCategory: 'Content Click',
      eventAction: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
      eventLabel: video.title,
      loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
      userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
      content: video.title,
      VideoCategory: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
      notificationStatus: 'False',
      country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
      plateform: 'Web'
    });
    console.log(window.dataLayer);
    if (video.is_series) {
      this.router.navigate(['/series', vedioName, video.content_id]);
    } else {
      this.router.navigate(['/video', vedioName, video.content_id]);
    }
  }

}
