import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { ContentService } from 'src/app/core/services/content.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { WishlistService } from 'src/app/core/services/wishlist.service';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { last } from 'rxjs/operators';
declare function tv2zPlayer(options: object): any;
declare function videojs(player: any): any;

@Component({
  selector: 'app-series-video',
  templateUrl: './series-video.component.html',
  styleUrls: ['./series-video.component.scss']
})
export class SeriesVideoComponent implements OnInit, OnDestroy {
  selectedSeason: any;
  seasons: any;
  seasonsLength: any;
  firstVideo: any;
  seriesVideos: any;
  seriesLength: any;
  contentId: any;
  series;
  trailers: any;
  trailersLength: any;
  tags: any;
  tagsLength: any;
  guest: any;
  trailerActive = false;
  video: any;
  trailer: any;
  calledSeriesPlayer: number;
  videoStartTime: Date;
  videoDuration: any;
  trailerWatching: boolean;
  windowWidth: number;
  height: any;
  width: any;
  holaWidth: any;
  trailerclicked: boolean;
  holaPlayer: boolean;
  type: string;
  trailerId: string;
  seriesVideoStartTime: Date;
  videoWidth: any;
  playclicked: boolean;
  playready: any;
  widevine: any;
  contentType: any;
  videoTitle: any;
  seriesEndTime: Date;
  imagePath: any;
  videoType: string;
  isSvodEnabled: number;
  back = false;
  updateMemoryData: any;
  director: any;
  cast: any;

  constructor(private contentService: ContentService, private route: ActivatedRoute, private categoryService: CategoriesService,
              private baseService: BaseService, private wishListService: WishlistService, private loggerService: LoggerService,
              private router: Router, private http: HttpClient, private settingsService: SettingsService,
              private storageService: StorageService) {
    this.isSvodEnabled = this.settingsService.isSvodEnabled;
    this.loggerService.memoryStorage.subscribe(data => {
      if (data) {
        this.updateMemoryData = data;
      }
    });
    this.baseService.loaderSource.next(true);
    window.dataLayer = window.dataLayer || [];
  }

  ngOnInit(): void {
    this.baseService.guest.subscribe(data => this.guest = data);
    this.windowWidth = window.innerWidth;
    this.imagePath = this.settingsService.imagePath;
    this.resetScreen();
    this.route.params
      .subscribe(params => {
        if (Object.keys(params).length) {
          this.contentId = params.series_id;
          this.getSeriesContent();
        }
      });
  }

  displayContent(id) {
    $('#seasons').hide();
    $('#trailers').hide();
    $('#episodes').hide();
    if (id === 'seasons') {
      $('#seasons').show();
    } else if (id === 'trailers') {
      $('#trailers').show();
    } else if (id === 'episodes') {
      $('#episodes').show();
    }
  }

  change_season(i): void {
    if (i !== undefined) {
      this.selectedSeason = this.seasons[i];
    }
    this.contentService.changeSeasonApi(this.selectedSeason.season_id).subscribe((data) => {
      if (data) {
        this.selectedSeason.videos = data.content;
        if (this.selectedSeason.videos.length > 0) {
          this.firstVideo = this.selectedSeason.videos[0];
          this.displayContent('seasons');
        } else {
          this.displayContent('description');
        }
      }
    });
  }

  get_episodes(): void {
    this.contentService.getEpisodesApi(this.contentId).subscribe((data) => {
      if (data) {
        this.seriesVideos = data.content;
        this.seriesLength = data.content.length;
        if (this.seriesVideos.length > 0) {
          this.firstVideo = this.seriesVideos[0];
          this.displayContent('episodes');
        } else {
          this.displayContent('description');
        }
      }
    });
  }

  getSeriesContent() {
    this.categoryService.getSeriesContents(this.contentId).subscribe((data) => {
      if (data) {
        this.baseService.loaderSource.next(false);
        this.fetchUrlApi();
        this.series = data.series;
        this.director = this.series.director;
        this.cast = this.series.cast;
        // $('.series-video-description').append(this.series.description);
        if (this.series.has_season) {
          this.seasons = data.seasons;
          this.seasonsLength = data.seasons.length;
          if (this.seasons.length > 0) {
            this.change_season(0);
            this.displayContent('seasons');
          } else {
            this.displayContent('description');
          }
        } else {
          this.seriesVideos = [];
          this.get_episodes();
        }
        if (data.trailer_videos.length > 0) {
          this.trailers = data.trailer_videos;
          this.trailersLength = data.trailer_videos.length;
        }
        if (this.series.tags) {
          this.tags = this.series.tags.split(',');
          this.tagsLength = this.tags.length;
        }
      }
    });
  }

  fetchUrlApi = () => {
    this.contentService.getSeriesPersonData(this.contentId).subscribe((data) => {
      if (data) {
        this.series.wishlist_status = data.wishlist_status;
        this.series.availability = data.availability;
        this.series.amount = data.amount;
        if (data.availability === 2) {
          this.series.tvod_max_age = data.tvod_max_age;
        }
      }
    });
  }

  addWishlist(id, status, title) {
    if (this.guest) {
      document.getElementById('signinButton').click();
    } else {
      const addWishlistData = { content_id: id, content_type: 'series', d_type: 'web', status };
      this.wishListService.addToWishlist(addWishlistData).subscribe((data) => {
        if (data) {
          window.dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'Add Wishlist',
            eventAction: 'Video name',
            eventLabel: title,
            loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
            userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
            notificationStatus: 'False',
            country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            plateform: 'Web'
          });
          console.log(window.dataLayer);
          this.series.wishlist_status = status;
        }
      });
    }
  }

  removeWishlist(id, status) {
    const paramsData: object = {
      req: { id },
      query: { d_type: 'web', status }
    };
    if (this.guest) {
      document.getElementById('signinButton').click();
    } else {
      this.wishListService.removeSeriesWishList(paramsData).subscribe((data) => {
        if (data) {
          this.series.wishlist_status = status;
        }
      });
    }
  }

  playTrailer(trailer) {
    this.trailer = trailer;
    this.trailerActive = true;
    this.calledSeriesPlayer = 1;
    // $('.overlay').css('display', 'block');
    // $('body').css('overflow-y', 'hidden');
    // $('body').css('height', '100%');
    this.callParentMethod('trailer');
  }

  getVideoDetails(id) {
    this.contentService.getCategoryDataById(id).subscribe((data) => {
      if (data) {
        this.video = data;
        if (this.video.u_t && this.updateMemoryData) {
          this.updateMemoryData.user_type = this.video.u_t;
          this.loggerService.updatedMemoryStorage(this.updateMemoryData);
        }
        this.videoDuration = this.video.duration;
        const videodate = new Date();
        this.videoStartTime = videodate;
        this.trailerWatching = false;
        this.trailer = this.video;
        this.trailerActive = true;
        this.calledSeriesPlayer = 1;
        // $('.overlay').css('overflow-y', 'scroll');
        // $('.overlay').css('display', 'block');
        // $('body').css('overflow-y', 'hidden');
        // $('body').css('height', '100%');
        this.callParentMethod('');
      }
    });
  }

  playVideo(id) {
    this.contentService.getContentPersonData(id).subscribe((data) => {
      if (data) {
        this.video = data;
        this.video.content_id = id;
        this.video.title = this.video.content_title;
        if (this.video.u_t && this.updateMemoryData) {
          this.updateMemoryData.user_type = this.video.user_type;
          this.loggerService.updatedMemoryStorage(this.updateMemoryData);
        }
        this.videoDuration = this.video.duration;
        const videodate = new Date();
        this.videoStartTime = videodate;
        this.trailerWatching = false;
        this.trailer = this.video;
        this.trailerActive = false;
        if (this.guest) {
          if (this.video.availability !== 3) {
            document.getElementById('signinButton').click();
          } else {
            this.trailerActive = true;
            this.calledSeriesPlayer = 1;
            // $('.overlay').css('display', 'block');
            // $('body').css('overflow-y', 'hidden');
            // $('body').css('height', '100%');
            this.callParentMethod('');
            // this.$emit("CallParentMethod", {});
          }
        } else {
          if (this.video.availability === 1) {
            this.router.navigate(['/subscriptions']);
            // $state.go('profile.subscriptions', { region: this.storageService.getLocalStore('region') });
          } else if (this.video.availability === 2) {
            this.videoType = 'series';
            if (this.video.user_type === 1 || this.isSvodEnabled === 0) {
              this.router.navigate(['/ppv-invoice', this.contentId, this.videoType]);
              // $state.go('profile.ppv-invoice', { id: this.contentId , type: this.videoType, region: this.storageService.getLocalStore('region') });
            } else {
              this.router.navigate(['/pay-per-view', this.contentId, this.videoType]);
              // $state.go('profile.pay_per_view', { id: this.contentId, type: this.videoType, region: this.storageService.getLocalStore('region') });
            }
          } else {
            this.trailerActive = true;
            this.calledSeriesPlayer = 1;
            // $('.overlay').css('display', 'block');
            // $('body').css('overflow-y', 'hidden');
            // $('body').css('height', '100%');
            this.callParentMethod('');
            // this.$emit("CallParentMethod", {});
          }
        }
      }
    });
  }

  showSignIn() {
    document.getElementById('signinButton').click();
  }

  signIn() {
    document.getElementById('signinclose').click();
    this.loggerService.updatedMemoryStorage({});
    // $state.go('static.signin', { region: this.storageService.getLocalStore('region') }, { reload: true });
    this.router.navigateByUrl('/sigin');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resetScreen();
  }
  resetScreen() {
    switch (true) {
      case this.windowWidth > 1800:
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 1450 && this.windowWidth < 1801):
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 991 && this.windowWidth < 1451):
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.height = 500;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth < 480):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      default:
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
    }
  }

  playSelectedTrailer(data) {
    this.trailerWatching = true;
    if (data.playback_type === 2) {
      const t = /v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(data.playback_url);
      const youtubeId = t ? t.slice(1).join('').replace('?', '') : '';
      const ytembed = 'https://www.youtube.com/embed/';
      const completeId = ytembed + youtubeId + '?enablejsapi=1&amp;rel=0&amp;showinfo=0&amp;autoplay=1';
      const iframe: HTMLIFrameElement = document.createElement('iframe');
      iframe.width = '100%';
      iframe.height = '100%';
      iframe.allowFullscreen = true;
      iframe.id = 'randomid';
      iframe.setAttribute('src', completeId);
      document.getElementById('trailer-player').appendChild(iframe);
      this.videoStartTime = new Date();
      this.trailerclicked = true;
      // if (window._paq) {
      // _paq.push(['MediaAnalytics::enableMediaAnalytics']);
      // _paq.push(['MediaAnalytics::enableTrackEvents']);
      // _paq.push(['MediaAnalytics::enableTrackProgress']);
      // _paq.push(['MediaAnalytics::scanForMedia']);
      // }
    } else if (data.playback_type === 4) {
      const url = 'https://vimeo.com/api/oembed.json';
      const params = new HttpParams().set('url', this.video.playback_url);
      this.http.get(url, { params }).subscribe(
        (response: any) => {
          if (response.video_id) {
            const videoid = 'https://player.vimeo.com/video/' + response.video_id + '?color=ff9933&title=0&byline=0&portrait=0&autoplay=1';
            const iframe: HTMLIFrameElement = document.createElement('iframe');
            iframe.width = '100%';
            iframe.height = '100%';
            iframe.id = 'randomid';
            iframe.setAttribute('allowfullscreen', 'true');
            iframe.setAttribute('webkitallowfullscreen', 'true');
            iframe.setAttribute('mozallowfullscreen', 'true');
            iframe.setAttribute('src', videoid);
            document.getElementById('trailer-player').appendChild(iframe);
            this.videoStartTime = new Date();
            this.trailerclicked = true;
            // if (window._paq) {
            //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
            //   _paq.push(['MediaAnalytics::enableTrackEvents']);
            //   _paq.push(['MediaAnalytics::enableTrackProgress']);
            //   _paq.push(['MediaAnalytics::scanForMedia']);
            // }
          } else {
            console.log('Given vimeo video id is not working');
          }
        });
    } else {
      this.holaPlayer = true;
      const videotag = document.createElement('video');
      videotag.controls = true;
      if (data.playback_url.match(/.mpd/g)) {
        this.type = 'application/dash+xml';
      } else if (data.playback_url.match(/.m3u8/g)) {
        this.type = 'application/x-mpegURL';
      } else {
        this.type = 'video/mp4';
      }
      videotag.setAttribute('title', data.title);
      this.trailerId = 'tv2zPlayer' + Math.floor(Math.random() * 1000000000);
      videotag.setAttribute('id', this.trailerId);
      videotag.setAttribute('height', this.height);
      videotag.setAttribute('width', this.holaWidth);
      videotag.setAttribute('class', 'video-js vjs-default-skin');
      document.getElementById('trailer-player').appendChild(videotag);
      this.videoStartTime = new Date();

      // New Player integration changes
      const res = data.playback_url.match(/DVR/ig);
      let options = { id: this.trailerId, src: data.playback_url, type: this.type };
      if (res) {
        options = Object.assign(options, { dvr: true });
      }
      tv2zPlayer(options);
      this.trailerclicked = false;
      const singleTrailerPlayer = videojs(this.trailerId);
      singleTrailerPlayer.on('play', function() {
        this.trailerclicked = true;
        window.dataLayer.push({
          event: 'GAEvent',
          eventCategory: 'Video',
          eventAction: 'Start',
          eventLabel: this.videoTitle,
          loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
          userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
          content: this.videoTitle,
          VideoCategory: '',
          audio: '',
          resolution: '',
          subtitle: '',
          notificationStatus: 'False',
          country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
          plateform: 'Web'
        });
        console.log(window.dataLayer);
      });
      // if (window._paq) {
      // videojs(this.trailerId).ready(function () {
      //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
      //   _paq.push(['MediaAnalytics::enableTrackEvents']);
      //   _paq.push(['MediaAnalytics::enableTrackProgress']);
      //   _paq.push(['MediaAnalytics::scanForMedia']);
      // });
      // }
      document.getElementsByClassName(this.trailerId + '-dimensions')[0]['style']['width'] = '100%';
      document.getElementsByClassName(this.trailerId + '-dimensions')[0]['style']['height'] = '100vh';
    }
  }
  playMainVideo(playbackType) {
    console.log('title', this.video.title);
    const videotitle = this.video.title;
    document.getElementById('trailer-player').innerHTML = '';
    this.trailerWatching = true;
    this.trailerclicked = false;
    if (this.guest && this.video.availability !== 3) {
      const iframe: HTMLIFrameElement = document.createElement('iframe');
      iframe.width = this.width;
      iframe.height = this.height;
      document.getElementById('trailer-player').appendChild(iframe);
    } else {
      if (playbackType === 2) {
        const t = /v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(this.video.playback_url);
        const youtubeId = t ? t.slice(1).join('').replace('?', '') : '';
        const ytembed = 'https://www.youtube.com/embed/';
        let completeId = ytembed + youtubeId + '?enablejsapi=1&amp;rel=0&amp;showinfo=0&amp;autoplay=1';
        if (this.video.seek !== '') {
          completeId = completeId + '&amp;start=' + this.video.seek;
        }
        const iframe: HTMLIFrameElement = document.createElement('iframe');
        iframe.width = '100%';
        iframe.height = '100%';
        iframe.allowFullscreen = true;
        iframe.id = 'randomid';
        iframe.setAttribute('src', completeId);
        document.getElementById('trailer-player').appendChild(iframe);
        const videoStartTime = new Date();
        this.seriesVideoStartTime = videoStartTime;
        this.videoWidth = this.width;
        this.playclicked = true;
        // if (window._paq) {
        //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //   _paq.push(['MediaAnalytics::enableTrackEvents']);
        //   _paq.push(['MediaAnalytics::enableTrackProgress']);
        //   _paq.push(['MediaAnalytics::scanForMedia']);
        // }
      } else if (playbackType === 4) {
        const url = 'https://vimeo.com/api/oembed.json';
        const params = new HttpParams().set('url', this.video.playback_url);
        this.http.get(url, { params }).subscribe(
          (response: any) => {
            if (response.video_id) {
              let videoid = 'https://player.vimeo.com/video/' + response.video_id + '?color=ff9933&title=0&byline=0&portrait=0&autoplay=1';
              if (this.video.seek !== '') {
                const hours = Math.floor(this.video.seek / 60 / 60);
                const  minutes = Math.floor(this.video.seek / 60) % 60;
                const  seconds = Math.floor(this.video.seek - minutes * 60);
                let seekformat = hours > 0 ? hours + 'h' : '';
                seekformat = minutes > 0 ? seekformat + minutes + 'm' : '';
                seekformat = seconds > 0 ? seekformat + seconds + 's' : '';
                videoid = videoid + '#t=' + seekformat;
              }
              const iframe = document.createElement('iframe');
              iframe.width = '100%';
              iframe.height = '100%';
              iframe.id = 'randomid';
              iframe.setAttribute('allowfullscreen', 'true');
              iframe.setAttribute('webkitallowfullscreen', 'true');
              iframe.setAttribute('mozallowfullscreen', 'true');
              iframe.setAttribute('src', videoid);
              document.getElementById('trailer-player').appendChild(iframe);
              const videoStartTime = new Date();
              this.seriesVideoStartTime = videoStartTime;
              // if (window._paq) {
              //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
              //   _paq.push(['MediaAnalytics::enableTrackEvents']);
              //   _paq.push(['MediaAnalytics::enableTrackProgress']);
              //   _paq.push(['MediaAnalytics::scanForMedia']);
              // }
            } else {
              console.log('Given vimeo video id is not working');
            }
          });
        this.playclicked = true;
        this.videoWidth = this.width;
      } else {
        this.holaPlayer = true;
        const videotag = document.createElement('video');
        videotag.controls = true;
        if (this.video.playback_url.match(/.mpd/g)) {
          this.type = 'application/dash+xml';
        } else if (this.video.playback_url.match(/.m3u8/g)) {
          this.type = 'application/x-mpegURL';
        } else {
          this.type = 'video/mp4';
        }
        videotag.setAttribute('title', this.video.title);
        videotag.setAttribute('id', this.trailerId);
        videotag.setAttribute('height', this.height);
        videotag.setAttribute('width', this.holaWidth);
        videotag.setAttribute('class', 'video-js vjs-default-skin');
        document.getElementById('trailer-player').appendChild(videotag);
        const videoStartTime = new Date();
        this.seriesVideoStartTime = videoStartTime;
        // New Player integration related changes
        const res = this.video.playback_url.match(/DVR/ig);
        let options: any = { id: this.trailerId, src: this.video.playback_url, type: this.type };
        if (res) {
          options = Object.assign(options, { dvr: true });
        }
        if (this.video.is_drm === 1) {
          options.keySystems = {};
          if (this.playready) {
            options.keySystems['com.microsoft.playready'] = this.playready;
          }
          if (this.widevine) {
            options.keySystems['com.widevine.alpha'] = this.widevine;
          }
        }
        if (this.video.ad_tag) {
          options.adTagUrl = this.video.ad_tag;
        }
        this.playclicked = false;
        tv2zPlayer(options);
        const singlePlayer = videojs(this.trailerId);
        if (this.video.seek !== '') {
          singlePlayer.currentTime(this.video.seek);
        }
        singlePlayer.on('play', function() {
          this.playclicked = true;
          window.dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'Video',
            eventAction: 'Start',
            eventLabel: videotitle,
            loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
            userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
            content: videotitle,
            VideoCategory: '',
            audio: '',
            resolution: '',
            subtitle: '',
            notificationStatus: 'False',
            country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            plateform: 'Web'
          });
          console.log('pushed');
        });
        console.log('dataLayer', window.dataLayer);
        // if (window._paq) {
        //   singlePlayer.ready(function () {
        //     _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //     _paq.push(['MediaAnalytics::enableTrackEvents']);
        //     _paq.push(['MediaAnalytics::enableTrackProgress']);
        //     _paq.push(['MediaAnalytics::scanForMedia']);
        //   });
        // }
        this.videoWidth = this.holaWidth;
        document.getElementsByClassName(this.trailerId + '-dimensions')[0]['style']['width'] = '100%';
        document.getElementsByClassName(this.trailerId + '-dimensions')[0]['style']['height'] = '100vh';
      }
    }
  }
  callParentMethod(type) {
    if (this.calledSeriesPlayer === 1) {
      this.calledSeriesPlayer = 0;
      this.trailerclicked = false;
      document.getElementById('trailer-player').innerHTML = '';
      this.trailerId = 'tv2zPlayer' + Math.floor(Math.random() * 1000000000);
      // document.getElementById("trailer-player").innerHTML = "";
      this.contentType = type;
      if (type === 'trailer') {
        this.contentType = type;
        this.trailer = this.trailer;
        this.videoTitle = this.trailer.title;
        this.playSelectedTrailer(this.trailer);
        // this.playTrailer(this.video.trailers[0].playback_type, this.video.trailers[0].playback_url)
      } else {
        this.contentType = 'video';
        this.video = this.trailer;
        this.videoTitle = this.video.title;
        this.playMainVideo(this.video.playback_type);
      }
      this.settingsService.showHeader = false;
      this.settingsService.showFooter = false;
    }
  }

  closeTrailer() {
    this.trailerActive = false;
    this.seriesEndTime = new Date();
    let durationInSeconds = 0;
    if (this.contentType !== 'trailer' && this.video.duration_sec !== undefined) {
      durationInSeconds = this.video.duration_sec;
    }
    if (this.contentType === 'trailer' && this.trailerclicked) {
      this.contentService.watchCountApi(this.trailer.trailer_id, 'trailer').subscribe();
    }
    if (this.seriesVideoStartTime) {
      let watchedTime = Math.ceil((this.seriesEndTime.getTime() - this.seriesVideoStartTime.getTime()) / 1000);
      if (60 <= watchedTime && this.contentType !== 'trailer') {
        this.contentService.watchCountApi(this.video.content_id, 'video').subscribe();
      }
      if (!this.guest) {
        if (this.video.seek !== '') {
          watchedTime = watchedTime + this.video.seek;
        }
        if (durationInSeconds <= watchedTime) {
          this.contentService.userHistoryApi(this.video.content_id);
        } else {
          if (this.playclicked) {
            this.contentService.userContinueWatchingApi(this.video.content_id, watchedTime).subscribe();
          }
        }
      }
    }
    // $('.overlay').css('overflow-y', 'hidden');
    // $('.overlay').css('display', 'none');
    // $('body').css('overflow-y', 'auto');
    const seriesPlayer = document.getElementById(this.trailerId);
    if (seriesPlayer) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(seriesPlayer).dispose();
    }
    document.getElementById('trailer-player').innerHTML = '';
    this.settingsService.showHeader = true;
    this.settingsService.showFooter = true;
  }

  ngOnDestroy() {
    this.trailerActive = false;
    this.closeTrailer();
  }

}
