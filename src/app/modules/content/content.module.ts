import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ContentRoutingModule } from './content-routing.module';
import { CategoryComponent } from './category/category.component';
import { SingleVideoComponent } from './single-video/single-video.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { SeriesVideoComponent } from './series-video/series-video.component';
import { WishlistComponent } from './wishlist/wishlist.component';


@NgModule({
  declarations: [CategoryComponent, SingleVideoComponent, SubCategoryComponent, SeriesVideoComponent, WishlistComponent],
  imports: [
    CommonModule,
    ContentRoutingModule,
    InfiniteScrollModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
        deps: [HttpClient]
      }
    })
  ]
})
export class ContentModule { }
