import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { BaseService, SettingsService } from 'src/app/core/services';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  slideToShow: number;
  slideToScroll: number;
  slideToShowSeries: number;
  slideToScrollSeries: number;
  barwidth: number;
  increment = 4;
  windowWidth: number;
  categoryName: any;
  mainData: any;
  limit: number;
  datas: any;
  datasLength: any;
  categoryId: any;
  homeImage: any;
  imagePath: any;
  constructor(private route: ActivatedRoute, private categoryService: CategoriesService, private settingsService: SettingsService,
              private router: Router, private baseService: BaseService) {
    this.getAdjustCards();
    this.imagePath = this.settingsService.imagePath;
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {

        this.baseService.loaderSource.next(true);
        if (Object.keys(params).length) {
          this.categoryId = params.category_id;
          this.categoryService.getCategoryContents(this.categoryId).subscribe(data => {
            if (data) {
              this.baseService.loaderSource.next(false);
              this.mainData = data;
              if (this.mainData.length > 0) {
                this.categoryName = this.mainData[0].category_title;
                // this.categoryName_slug = slugify(this.categoryName);
                // angular.forEach(this.mainData, function (value, key) {
                //   var slug = slugify(value.title);
                //   this.mainData[key]['slug'] = slug;
                // });
              }
              if (this.mainData.length > this.increment) {
                this.limit = this.increment;
                this.datas = this.mainData.slice(0, this.increment);
                this.datasLength = this.mainData.slice(0, this.increment).length;
              } else {
                this.datas = this.mainData;
                this.limit = this.mainData.length;
                this.datasLength = this.mainData.length;
              }
            }
          });
        }
      });
  }

  getAdjustCards() {
    this.windowWidth = window.innerWidth;
    switch (true) {
      case this.windowWidth > 1800:
        this.slideToShow = 6;
        this.slideToScroll = 5;
        this.slideToShowSeries = 7;
        this.slideToScrollSeries = 7;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 1365 && this.windowWidth < 1801):
        this.slideToShow = 5;
        this.slideToScroll = 4;
        this.slideToShowSeries = 6;
        this.slideToScrollSeries = 6;
        this.barwidth = 0.85;
        break;

      case (this.windowWidth > 1023 && this.windowWidth < 1366):
        this.slideToShow = 4;
        this.slideToScroll = 3;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 4;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 992 && this.windowWidth < 1024):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 4;
        this.barwidth = 0.65;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.slideToShow = 2;
        this.slideToScroll = 1;
        this.slideToShowSeries = 3.5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.44;
        break;
      case (this.windowWidth < 480):
        this.slideToShow = 1.5;
        this.slideToScroll = 2;
        this.slideToShowSeries = 2.5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.70;
        break;
      case (this.windowWidth < 360):
        this.slideToShow = 1;
        this.slideToScroll = 1;
        this.slideToShowSeries = 1.8;
        this.slideToScrollSeries = 2;
        this.barwidth = 0.70;
        break;
      default:
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.85;
        break;
    }
  }

  // this.hoverIn = function (event, datakey, sub, id, key, length) {
  //     $('#' + id + "_" + key).addClass('transition-class');
  // };
  // this.hoverOut = function (event, sub, id, key, length) {
  //     $('#' + id + "_" + key).removeClass('transition-class');
  // };
  loadMore = function() {
    this.datas = this.mainData;
  };
  setTitleValue = function(value) {
    this.sub_cat_title = value;
  };
  redirect(video) {
    const vedioName = this.settingsService.slugify(video.title);
    if (video.is_series) {
      this.router.navigate(['/series', vedioName, video.content_id]);
      // $state.go('profile.series', { series_name: vedioName, id: video.content_id, region: this.storageService.getLocalStore('region') });
    } else {
      this.router.navigate(['/video', vedioName, video.content_id]);
      // $state.go('profile.video', { video_name: vedioName, id: video.content_id, region: this.storageService.getLocalStore('region') });
    }
  }
}
