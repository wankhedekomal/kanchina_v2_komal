import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { ContentService } from 'src/app/core/services/content.service';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { routeUrl, fURL, tURL } from 'src/app/shared/constants';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { WishlistService } from 'src/app/core/services/wishlist.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';

declare function tv2zPlayer(options: object): any;
declare function videojs(player: any): any;

@Component({
  selector: 'app-single-video',
  templateUrl: './single-video.component.html',
  styleUrls: ['./single-video.component.scss']
})
export class SingleVideoComponent implements OnInit, OnDestroy {
  video;
  videoLength;
  displayPopup = false;
  windowWidth;
  height: any;
  width: any;
  holaWidth: number;
  selectedSeason: any;
  seasons: any;
  contentId: string;
  seriesVideos: any;
  moreLikeVideos: any;
  morelikeOrientation: any;
  trailerWatching = false;
  videoDuration: any;
  videoStartTime: Date;
  furl: string;
  turl: string;
  videoType: string;
  videoId: any;
  trailers: any;
  tags: any;
  tagsLength: any;
  comments: any;
  guest;
  uId: string;
  showShareicons = false;
  calledSinglePlayer;
  videoDetails;
  playerActive;
  isSvodEnabled;
  stateChange = false;
  playerId: string;
  endTime;
  imagePath: any;
  back = false;
  videoTitle;
  memoryStorage: any;
  trailerclicked: boolean;
  contentType: any;
  trailer: any;
  playclicked: any;
  videoWidth: any;
  holaPlayer: boolean;
  type: string;
  playready: any;
  widevine: any;
  likedvodLength: any;
  updateMemoryData: any;
  director: any;
  cast: any;
  constructor(private contentService: ContentService, private loggerService: LoggerService,
              private baseService: BaseService, private wishlistService: WishlistService, private router: Router,
              private http: HttpClient, private settingsService: SettingsService, private route: ActivatedRoute,
              private storageService: StorageService) {
    this.windowWidth = window.innerWidth;
    this.baseService.guest.subscribe(data => this.guest = data);
    this.uId = this.storageService.getLocalStore('u_id');
    this.isSvodEnabled = this.settingsService.isSvodEnabled;
    this.imagePath = this.settingsService.imagePath;
    this.loggerService.memoryStorage.subscribe(data => {
      if (data) {
        this.updateMemoryData = data;
      }
    });
    window.dataLayer = window.dataLayer || [];
  }

  ngOnInit(): void {
    this.resetScreen();
    this.route.params
      .subscribe(params => {
        if (Object.keys(params).length) {
          this.contentId = params.video_id;
          this.baseService.loaderSource.next(true);
          this.getVideoContent();
        }
      });

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resetScreen();
  }

  resetScreen() {
    switch (true) {
      case this.windowWidth > 1800:
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 1450 && this.windowWidth < 1801):
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 991 && this.windowWidth < 1451):
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.height = 500;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth < 480):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      default:
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
    }
  }

  showPopup = () => {
    this.displayPopup = true;
  }
  closePopup = () => {
    this.displayPopup = false;
  }
  change_season(i, season): void {
    if (i !== undefined) {
      this.selectedSeason = this.seasons[i];
    } else {
      this.selectedSeason = season;
    }
    this.contentService.changeSeasonApi(this.selectedSeason.season_id).subscribe((data) => {
      if (data) {
        this.selectedSeason.videos = data.content;
      }
    });
  }
  get_episodes(): void {
    this.contentService.getEpisodesApi(this.video.series_id).subscribe((data) => {
      if (data) {
        this.seriesVideos = data.content;
      }
    });
  }

  getMoreLikeVideos(): void {
    this.moreLikeVideos = [];
    this.contentService.getSimilarAssestById(this.contentId).subscribe((data) => {
      if (data) {
        this.moreLikeVideos = data;
        this.likedvodLength = data.contents.length;
        this.morelikeOrientation = data.orientation;
      }
    });
  }

  showSignIn = (value) => {
    if (value === 'paid') {
      this.back = true;
    }
    document.getElementById('signinButton').click();
  }

  fetchUrlApi = () => {
    this.contentService.getContentPersonData(this.contentId).subscribe((data) => {
      if (data) {
        this.video.ad_url = data.ad_url;
        this.video.availability = data.availability;
        this.video.like_status = data.like_status;
        this.video.playback_url = data.playback_url;
        this.video.seek = data.seek;
        this.video.user_type = data.user_type;
        this.video.wishlist_status = data.wishlist_status;
        this.video.amount = data.amount;
        if (this.video.user_type && this.updateMemoryData) {
          this.updateMemoryData.user_type = this.video.user_type;
          this.loggerService.updatedMemoryStorage(this.updateMemoryData);
        }
        if (data.availability === 2) {
          this.video.tvod_max_age = data.tvod_max_age;
        }
      }
    });
  }
  getVideoContent() {
    this.video = undefined;
    this.contentService.getCategoryDataById(this.contentId).subscribe((data) => {
      if (data) {
        this.baseService.loaderSource.next(false);
        this.video = data;
        this.director = this.video.director;
        this.cast = this.video.cast;
        this.videoLength = this.video.trailers.length;
        if (this.video.u_t && this.updateMemoryData) {
          this.updateMemoryData.user_type = this.video.u_t;
          this.loggerService.updatedMemoryStorage(this.updateMemoryData);
        }
        this.videoDuration = this.video.duration_sec;
        const videodate = new Date();
        this.videoStartTime = videodate;
        const sharingurl = routeUrl + 'video/' + this.contentId;
        this.furl = fURL + sharingurl;
        this.turl = tURL + sharingurl;
        this.trailerWatching = false;
        if (this.video.series_id !== '') {
          this.videoType = 'series';
          this.videoId = this.video.series_id;
        } else {
          this.videoType = 'video';
          this.videoId = this.video.content_id;
        }
        if (this.video.series_id !== '') {
          if (this.video.season_id !== '') {
            this.seasons = this.video.seasons;
            if (this.seasons.length > 0) {
              this.change_season(0, '');
            }
          } else {
            this.get_episodes();
          }
        } else {
          this.getMoreLikeVideos();
        }
        if (this.video.trailers.length > 0) {
          this.trailers = this.video.trailers;
        }
        if (this.video.tags) {
          this.tags = this.video.tags.split(',');
          this.tagsLength = this.tags.length;
        }
        this.fetchUrlApi();
        if (this.video.comments.comments && this.video.comments.comments.length > 0) {
          this.comments = this.video.comments.comments;
          for (let i = 0; i < this.comments.length; i++) {
            this.comments[i].showReplyBox = false;
            this.comments[i].viewReply = false;
            for (let j = 0; j < this.comments[i].replies.length; j++) {
              this.comments[i].replies[j].subReplyBox = false;
            }
          }
        }
      }
    });
  }

  addWishlist(id, index, type, wStatus, title) {
    let contentType: string;
    if (this.guest) {
      document.getElementById('signinButton').click();
    } else {
      if (type === 'single') {
        if (this.video.series_id !== '') {
          contentType = 'series';
          id = this.video.series_id;
        } else {
          contentType = 'video';
        }
      } else if (type === 'more') {
        contentType = 'video';
      }
      const AddWishlistdata: object = {
        u_id: this.uId,
        content_id: id,
        content_type: contentType,
        d_type: 'web'
      };
      this.wishlistService.addToWishlist(AddWishlistdata).subscribe((data) => {
        if (data) {
          window.dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'Add Wishlist',
            eventAction: 'Video name',
            eventLabel: title,
            loginStatus: 'True',
            userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
            notificationStatus: 'False',
            country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            plateform: 'Web'
          });
          console.log(window.dataLayer);
          if (type === 'more') {
            this.moreLikeVideos[index].w_status = wStatus;
          }
          if (type === 'single') {
            this.video.wishlist_status = wStatus;
          } else if (type === 'season') {
            this.selectedSeason.videos[index].wishlist_status = wStatus;
          } else if (type === 'series') {
            this.seriesVideos[index].wishlist_status = wStatus;
          } else {
            if (wStatus === 1) {
              // setTimeout(function () {
              //   $("#my-list-txt_" + id + "_989").html('<a onclick="angular.element(this).scope().
              // removeWishlist(' + "'" + id + "'" + ', ' + index + ', ' + "'" + type + "'" + ',' + 0 + ')"
              // class="my-list bold my-list-single-video-page" id="remove-my-list-txt" style="cursor: pointer;">' +
              //     '<img class="my-list-image" src="assets/img/favorite-filled.png" alt="added-to-my-list">' +
              //     '<span class="my-list-txt">' + $filter('translate')('My List') + '</span>' +
              //     '</a>');
              // }, 1000);
            } else {
              // setTimeout(function () {
              //   $("#my-list-txt_" + id + "_989").html('<a onclick="angular.element(this).scope().
              // addWishlist(' + "'" + id + "'" + ', ' + index + ', ' + "'" + type + "'" + ',' + 1 + ')"
              // class="my-list bold my-list-single-video-page" style="cursor: pointer;">' +
              //     '<img class="my-list-image" src="assets/img/favorite-empty.png" alt="added-to-my-list">' +
              //     '<span class="my-list-txt">' + $filter('translate')('My List') + '</span>' +
              //     '</a>');
              // }, 2000);
            }
          }
        }
      });

    }
  }

  removeWishlist = (id, index, type, wStatus) => {
    let contentType;

    if (this.guest) {
      document.getElementById('signinButton').click();
    } else {
      if (type === 'single') {
        if (this.video.series_id !== '') {
          contentType = 'series';
          id = this.video.series_id;
        } else {
          contentType = 'video';
        }
      } else if (type === 'more') {
        contentType = 'video';
      }
      const paramsData: object = {
        req: { content_type: contentType, id },
        query: { u_id: this.uId, d_type: 'web' }
      };
      this.wishlistService.removeWishlist(paramsData).subscribe((data) => {
        if (data) {
          if (type === 'more') {
            this.moreLikeVideos[index].w_status = wStatus;
          }
          if (type === 'single') {
            this.video.wishlist_status = wStatus;
          } else if (type === 'season') {
            this.selectedSeason.videos[index].wishlist_status = wStatus;
          } else if (type === 'series') {
            this.seriesVideos[index].wishlist_status = wStatus;
          } else {
            if (wStatus === 1) {
              // setTimeout(function () {
              //   $("#my-list-txt_" + id + "_989").html('<a onclick="angular.element(this).scope()
              // .removeWishlist(' + "'" + id + "'" + ', ' + index + ', ' + "'" + type + "'" + ',' + 0 + ')"
              // class="my-list bold my-list-single-video-page" id="remove-my-list-txt" style="cursor: pointer;">' +
              //     '<img class="my-list-image" src="assets/img/favorite-filled.png" alt="added-to-my-list">' +
              //     '<span class="my-list-txt">' + $filter('translate')('My List') + '</span>' +
              //     '</a>');
              // }, 1000);
            } else {
              // setTimeout(function () {
              //   $("#my-list-txt_" + id + "_989").html('<a onclick="angular.element(this).scope().
              // addWishlist(' + "'" + id + "'" + ', ' + index + ', ' + "'" + type + "'" + ',' + 1 + ')"
              // class="my-list bold my-list-single-video-page" style="cursor: pointer;">' +
              //     '<img class="my-list-image" src="assets/img/favorite-empty.png" alt="added-to-my-list">' +
              //     '<span class="my-list-txt">' + $filter('translate')('My List') + '</span>' +
              //     '</a>');
              // }, 2000);
            }
          }
        }
      });
    }
  }
  showShare = () => {
    this.showShareicons = !this.showShareicons;
  }
  signIn = () => {
    document.getElementById('signinclose').click();
    this.loggerService.updatedMemoryStorage({});
    this.router.navigateByUrl('/signin');
  }
  selectTrailer = (trailer) => {
    this.trailer = trailer;
    this.calledSinglePlayer = 1;
    this.playerActive = true;
    // $('.overlay').css('display', 'block');
    // $('body').css('overflow-y', 'hidden');
    // $('body').css('height', '100%');
    this.callVideoPlayer('trailer');
  }
  playVideo = (id, type, title) => {
    if (id === this.contentId) {
      if (this.guest) {
        if (this.video.availability !== 3) {
          document.getElementById('signinButton').click();
        } else {
          this.calledSinglePlayer = 1;
          this.videoDetails = this.video;
          this.playerActive = true;
          // $('.overlay').css('display', 'block');
          // $('body').css('overflow-y', 'hidden');
          // $('body').css('height', '100%');
          if (this.trailer) {
            this.callVideoPlayer('trailer');
          }
          else {
            this.callVideoPlayer('');
          }
        }
      } else {
        if (this.video.availability === 1) {
          this.router.navigate(['/subscriptions']);
          // $state.go('profile.subscriptions', { region: this.storageService.getLocalStore('region') });
        } else if (this.video.availability === 2) {
          if (this.video.series_id > 0) {
            this.videoType = 'series';
            if (this.video.user_type === 1 || this.isSvodEnabled === 0) {
              this.router.navigate(['/ppv-invoice', this.video.series_id, this.videoType]);
              // $state.go('profile.ppv-invoice', { id: this.video.series_id, type: this.videoType,
              // region: this.storageService.getLocalStore('region') });
            } else {
              this.router.navigate(['/pay-per-view', this.video.series_id, this.videoType]);
              // $state.go('profile.pay_per_view', { id: this.video.series_id, type: this.videoType,
              // region: this.storageService.getLocalStore('region') });
            }
          } else {
            this.videoType = 'video';
            if (this.video.user_type === 1 || this.isSvodEnabled === 0) {
              this.router.navigate(['/ppv-invoice', this.video.content_id, this.videoType]);
              // $state.go('profile.ppv-invoice', { id: this.video.content_id, type: this.videoType,
              // region: this.storageService.getLocalStore('region') });
            } else {
              this.router.navigate(['/pay-per-view', this.video.content_id, this.videoType]);
              // $state.go('profile.pay_per_view', { id: this.video.content_id, type: this.videoType,
              // region: this.storageService.getLocalStore('region') });
            }
          }
        } else {
          this.calledSinglePlayer = 1;
          this.videoDetails = this.video;
          this.playerActive = true;
          // $('.overlay').css('display', 'block');
          // $('body').css('overflow-y', 'hidden');
          // $('body').css('height', '100%');
          this.callVideoPlayer(type);
        }
      }
    } else {
      this.router.navigate(['/video', this.video.name, this.video.content_id]);
      // $state.go('profile.video', { video_name: vedio_name, id: id, region: this.storageService.getLocalStore('region') }, { reload: false });
    }
  }

  playTrailer = (videoType, trailerVideo) => {
    const mainVideoPlayer = document.getElementById(this.playerId);
    if (mainVideoPlayer) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(mainVideoPlayer).dispose();
    }
    document.getElementById('video-player').innerHTML = '';
    this.trailerWatching = true;
    if (videoType === 2) {
      const t = /v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(trailerVideo);
      const youtubeId = t ? t.slice(1).join('').replace('?', '') : '';
      const ytembed = 'https://www.youtube.com/embed/';
      const completeId = ytembed + youtubeId + '?enablejsapi=1&amp;rel=0&amp;showinfo=0&amp;autoplay=1';
      const iframe: HTMLIFrameElement = document.createElement('iframe');
      iframe.width = '100%';
      iframe.height = '100%';
      iframe.allowFullscreen = true;
      iframe.id = 'randomid';
      iframe.setAttribute('src', completeId);
      document.getElementById('video-player').appendChild(iframe);
      this.videoStartTime = new Date();
      this.trailerclicked = true;
      this.videoWidth = this.width;
      // if (window._paq) {
      //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
      //   _paq.push(['MediaAnalytics::enableTrackEvents']);
      //   _paq.push(['MediaAnalytics::enableTrackProgress']);
      //   _paq.push(['MediaAnalytics::scanForMedia']);
      // }
    } else if (videoType === 4) {
      const url = 'https://vimeo.com/api/oembed.json';
      const params = new HttpParams().set('url', this.video.playback_url);
      this.http.get(url, { params }).subscribe(
        (response) => {
          if (response['video_id']) {
            const videoid = 'https://player.vimeo.com/video/' + response['video_id'] + '?color=ff9933&title=0&byline=0&portrait=0&autoplay=1';
            const iframe: HTMLIFrameElement = document.createElement('iframe');
            iframe.width = '100%';
            iframe.height = '100%';
            iframe.id = 'randomid';
            iframe.setAttribute('allowfullscreen', 'true');
            iframe.setAttribute('webkitallowfullscreen', 'true');
            iframe.setAttribute('mozallowfullscreen', 'true');
            iframe.setAttribute('src', videoid);
            document.getElementById('video-player').appendChild(iframe);
            this.videoStartTime = new Date();
            this.trailerclicked = true;
            // if (window._paq) {
            //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
            //   _paq.push(['MediaAnalytics::enableTrackEvents']);
            //   _paq.push(['MediaAnalytics::enableTrackProgress']);
            //   _paq.push(['MediaAnalytics::scanForMedia']);
            // }
          } else {
            console.log('Given vimeo video id is not working');
          }
        });
      this.playclicked = true;
      this.videoWidth = this.width;
    } else {
      this.holaPlayer = true;
      const videotag = document.createElement('video');
      videotag.controls = true;
      if (trailerVideo.match(/.mpd/g)) {
        this.type = 'application/dash+xml';
      } else if (trailerVideo.match(/.m3u8/g)) {
        this.type = 'application/x-mpegURL';
      } else {
        this.type = 'video/mp4';
      }
      videotag.setAttribute('title', this.trailer.title);
      videotag.setAttribute('id', this.playerId);
      videotag.setAttribute('height', this.height);
      videotag.setAttribute('width', this.holaWidth + 'px');
      videotag.setAttribute('class', 'video-js vjs-default-skin');
      document.getElementById('video-player').appendChild(videotag);
      this.videoStartTime = new Date();

      // New Player integration changes
      const res = trailerVideo.match(/DVR/ig);
      let options = { id: this.playerId, src: trailerVideo, type: this.type };
      if (res) {
        options = Object.assign(options, { dvr: true });
      }
      tv2zPlayer(options);
      this.trailerclicked = false;
      const singleTrailerPlayer = videojs(this.playerId);
      singleTrailerPlayer.on('play', () => {
        this.trailerclicked = true;
        window.dataLayer.push({
          event: 'GAEvent',
          eventCategory: 'Video',
          eventAction: 'Start',
          eventLabel: this.videoTitle,
          loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
          userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
          content: this.videoTitle,
          VideoCategory: '',
          audio: '',
          resolution: '',
          subtitle: '',
          notificationStatus: 'False',
          country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
          plateform: 'Web'
        });
        console.log(window.dataLayer);
      });
      // if (window._paq) {
      //   singleTrailerPlayer.ready(function () {
      //     _paq.push(['MediaAnalytics::enableMediaAnalytics']);
      //     _paq.push(['MediaAnalytics::enableTrackEvents']);
      //     _paq.push(['MediaAnalytics::enableTrackProgress']);
      //     _paq.push(['MediaAnalytics::scanForMedia']);
      //   });
      // }
      this.videoWidth = this.holaWidth;
      document.getElementsByClassName(this.playerId + '-dimensions')[0]['style']['width'] = '100%';
      document.getElementsByClassName(this.playerId + '-dimensions')[0]['style']['height'] = '100vh';
    }
  }
  playMainVideo = (playbackType) => {
    document.getElementById('video-player').innerHTML = '';
    this.trailerWatching = false;
    this.trailerclicked = false;
    if (this.guest && this.video.availability !== 3) {
      const iframe: HTMLIFrameElement = document.createElement('iframe');
      iframe.width = this.width;
      iframe.height = this.height;
      document.getElementById('video-player').appendChild(iframe);
    } else {
      if (playbackType === 2) {
        const t = /v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(this.video.playback_url);
        const youtubeId = t ? t.slice(1).join('').replace('?', '') : '';
        const ytembed = 'https://www.youtube.com/embed/';
        let completeId = ytembed + youtubeId + '?enablejsapi=1&amp;rel=0&amp;showinfo=0&amp;autoplay=1';
        if (this.video.seek !== '') {
          completeId = completeId + '&amp;start=' + this.video.seek;
        }
        const iframe: HTMLIFrameElement = document.createElement('iframe');
        iframe.width = '100%';
        iframe.height = '100%';
        iframe.allowFullscreen = true;
        iframe.id = 'randomid';
        iframe.setAttribute('src', completeId);
        document.getElementById('video-player').appendChild(iframe);
        this.videoStartTime = new Date();
        this.videoWidth = this.width;
        this.playclicked = true;
        // if (window._paq) {
        //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //   _paq.push(['MediaAnalytics::enableTrackEvents']);
        //   _paq.push(['MediaAnalytics::enableTrackProgress']);
        //   _paq.push(['MediaAnalytics::scanForMedia']);
        // }
      } else if (playbackType === 4) {
        const url = 'https://vimeo.com/api/oembed.json';
        const params = new HttpParams().set('url', this.video.playback_url);
        this.http.get(url, { params }).subscribe(
          (response) => {
            if (response['video_id']) {
              let videoid = 'https://player.vimeo.com/video/' + response['video_id'] + '?color=ff9933&title=0&byline=0&portrait=0&autoplay=1';
              if (this.video.seek !== '') {
                const hours = Math.floor(this.video.seek / 60 / 60);
                const minutes = Math.floor(this.video.seek / 60) % 60;
                const seconds = Math.floor(this.video.seek - minutes * 60);
                let seekformat = hours > 0 ? hours + 'h' : '';
                seekformat = minutes > 0 ? seekformat + minutes + 'm' : '';
                seekformat = seconds > 0 ? seekformat + seconds + 's' : '';
                videoid = videoid + '#t=' + seekformat;
              }
              const iframe: HTMLIFrameElement = document.createElement('iframe');
              iframe.width = '100%';
              iframe.height = '100%';
              iframe.id = 'randomid';
              iframe.setAttribute('allowfullscreen', 'true');
              iframe.setAttribute('webkitallowfullscreen', 'true');
              iframe.setAttribute('mozallowfullscreen', 'true');
              iframe.setAttribute('src', videoid);
              document.getElementById('video-player').appendChild(iframe);
              this.videoStartTime = new Date();
              // if (window._paq) {
              //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
              //   _paq.push(['MediaAnalytics::enableTrackEvents']);
              //   _paq.push(['MediaAnalytics::enableTrackProgress']);
              //   _paq.push(['MediaAnalytics::scanForMedia']);
              // }
            } else {
              console.log('Given vimeo video id is not working');
            }
          });
        this.playclicked = true;
        this.videoWidth = this.width;
      } else {
        this.holaPlayer = true;
        const videotag = document.createElement('video');
        videotag.controls = true;
        if (this.video.playback_url.match(/.mpd/g)) {
          this.type = 'application/dash+xml';
        } else if (this.video.playback_url.match(/.m3u8/g)) {
          this.type = 'application/x-mpegURL';
        } else {
          this.type = 'video/mp4';
        }
        videotag.setAttribute('title', this.video.title);
        videotag.setAttribute('id', this.playerId);
        videotag.setAttribute('height', this.height);
        videotag.setAttribute('width', this.holaWidth + 'px');
        videotag.setAttribute('class', 'video-js vjs-default-skin');
        document.getElementById('video-player').appendChild(videotag);
        this.videoStartTime = new Date();
        // New Player integration related changes
        const res = this.video.playback_url.match(/DVR/ig);
        let options = { id: this.playerId, src: this.video.playback_url, type: this.type };
        if (res) {
          options = Object.assign(options, { dvr: true });
        }
        if (this.video.is_drm === 1) {
          options['keySystems'] = {};
          if (this.playready) {
            options['keySystems']['com.microsoft.playready'] = this.playready;
          }
          if (this.widevine) {
            options['keySystems']['com.widevine.alpha'] = this.widevine;
          }
        }
        if (this.video.ad_tag) {
          options['adTagUrl'] = this.video.ad_tag;
        }
        this.playclicked = false;
        tv2zPlayer(options);
        const singlePlayer = videojs(this.playerId);
        if (this.video.seek !== '') {
          singlePlayer.currentTime(this.video.seek);
        }
        singlePlayer.on('play', () => {
          this.playclicked = true;
          window.dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'Video',
            eventAction: 'Start',
            eventLabel: this.videoTitle,
            loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
            userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
            content: this.videoTitle,
            VideoCategory: '',
            audio: '',
            resolution: '',
            subtitle: '',
            notificationStatus: 'False',
            country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            plateform: 'Web'
          });
          console.log(window.dataLayer);
        });
        // if (window._paq) {
        //   singlePlayer.ready(function () {
        //     _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //     _paq.push(['MediaAnalytics::enableTrackEvents']);
        //     _paq.push(['MediaAnalytics::enableTrackProgress']);
        //     _paq.push(['MediaAnalytics::scanForMedia']);
        //   });
        // }
        this.videoWidth = this.holaWidth;
        document.getElementsByClassName(this.playerId + '-dimensions')[0]['style']['width'] = '100%';
        document.getElementsByClassName(this.playerId + '-dimensions')[0]['style']['height'] = '100vh';
      }
    }
  }
  callVideoPlayer = (type) => {
    if (this.calledSinglePlayer === 1) {
      this.calledSinglePlayer = 0;
      this.trailerclicked = false;
      this.playerId = 'tv2zPlayer' + Math.floor(Math.random() * 1000000000);
      document.getElementById('video-player').innerHTML = '';
      if (type === 'trailer') {
        this.contentType = type;

        this.videoTitle = this.trailer.title;
        this.playTrailer(this.trailer.playback_type, this.trailer.playback_url);
      } else {
        this.contentType = 'video';
        this.video = this.videoDetails;
        this.director = this.video.director;
        this.cast = this.video.cast;
        this.videoLength = this.video.trailers.length;
        this.videoTitle = this.video.title;
        this.playMainVideo(this.video.playback_type);
      }
      this.settingsService.showHeader = false;
      this.settingsService.showFooter = false;
    }
    // this.playSelectedTrailer(this.trailer);
  }

  closeVideo() {
    this.playerActive = false;
    this.endTime = new Date();
    let durationInSeconds = 0;
    if (this.contentType !== 'trailer' && this.videoDuration !== undefined) {
      durationInSeconds = this.videoDuration;
    }
    if (this.contentType === 'trailer' && this.trailerclicked) {
      this.contentService.watchCountApi(this.trailer.trailer_id, 'trailer').subscribe();
    }
    if (this.videoStartTime) {
      let watchedTime = Math.ceil((this.endTime.getTime() - this.videoStartTime.getTime()) / 1000);
      if (60 <= watchedTime && this.contentType !== 'trailer') {
        this.contentService.watchCountApi(this.video.content_id, 'video').subscribe();
      }
      if (!this.guest) {
        if (this.video.seek !== '') {
          watchedTime = watchedTime + this.video.seek;
        }
        if (durationInSeconds <= watchedTime) {
          this.contentService.userHistoryApi(this.video.content_id);
        } else {
          if (this.playclicked) {
            this.contentService.userContinueWatchingApi(this.video.content_id, watchedTime).subscribe();
          }
        }
      }
      const seriesPlayer = document.getElementById(this.playerId);
      if (seriesPlayer) {
        if (document['pictureInPictureElement']) {
          document['exitPictureInPicture']();
        }
        videojs(seriesPlayer).dispose();
      }
      document.getElementById('video-player').innerHTML = '';
      // $('.overlay').css('overflow-y', 'hidden');
      // $('.overlay').css('display', 'none');
      // $('body').css('overflow-y', 'auto');
    }
    const videoPlayerId = document.getElementById(this.playerId);
    if (videoPlayerId) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(videoPlayerId).dispose();

    }
    this.settingsService.showHeader = true;
    this.settingsService.showFooter = true;
  }

  ngOnDestroy() {
    this.playerActive = false;
    const oldPlayer = document.getElementById(this.playerId);
    if (oldPlayer) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(oldPlayer).dispose();
    }
  }
}
