import { Component, OnInit } from '@angular/core';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { Router } from '@angular/router';
import { WishlistService } from 'src/app/core/services/wishlist.service';
import { UikitService } from 'src/app/core/services/uikit.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {
  data;
  section;
  dataLoaded;
  response;
  wishlistData;
  wishlistCount;
  noResultsFound;
  homeImage: any;


  constructor(public settingsService: SettingsService, private router: Router, private wishlistService: WishlistService,
              private uikitService: UikitService, private baseSerive: BaseService, private storageService: StorageService) {
    this.homeImage = this.settingsService.homeImage;
    this.baseSerive.loaderSource.next(true);

    this.loadList();
  }

  ngOnInit(): void {
  }

  loadList() {
    this.response = {
      u_id: this.storageService.getLocalStore('u_id'),
      d_type: 'web',
      skip: 0,
      take: 12
    };
    this.wishlistService.getWishlist(this.response).subscribe({
      next: data => {
        this.baseSerive.loaderSource.next(false);
        this.wishlistData = data;
      }, complete: () => {
        if (this.wishlistData.status_code === 200) {
          this.data = this.wishlistData.data.contents;
          this.wishlistCount = this.wishlistData.data.total;
          this.noResultsFound = '';
          if (this.wishlistCount === 0) {
            this.noResultsFound = 'Wishlist is empty';
          }
        } else {
          this.uikitService.staticErrorMessage(this.wishlistData.error.message);
        }
        if (this.wishlistData.error && this.wishlistData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.wishlistData.error.message);
        }
      }
    });
  }
}
