import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { SeriesVideoComponent } from './series-video/series-video.component';
import { SingleVideoComponent } from './single-video/single-video.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { WishlistComponent } from './wishlist/wishlist.component';



const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'category/:category/:category_id',
        component: CategoryComponent
      },
      {
        path: 'sub-cat/:title',
        component: SubCategoryComponent,
      },
      {
        path: 'sub-cat/:title/:sub_cat_id',
        component: SubCategoryComponent
      },
      {
        path: 'video/:video_title/:video_id',
        component: SingleVideoComponent
      },
      {
        path: 'series/:series_title/:series_id',
        component: SeriesVideoComponent
      },
      {
        path: 'wishlist',
        component: WishlistComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
