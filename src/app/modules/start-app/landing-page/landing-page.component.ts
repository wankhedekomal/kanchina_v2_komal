import { Router } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { MobileAppComponent } from 'src/app/shared/components/mobile-app/mobile-app.component';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit, OnDestroy {

  bannerHeading: string;
  bannerDescription: string;
  siteName: string;
  showIosApp: boolean;
  showAndroidApp: boolean;
  showPopup: boolean;
  logo: any;
  homeImage: any;


  constructor(public settingsService: SettingsService, private router: Router, private baseService: BaseService,
              private storageService: StorageService) {
    this.homeImage = this.settingsService.homeImage;
    const userId = this.storageService.getLocalStore('u_id');
    if (this.baseService.loginMode !== 1 || this.storageService.getLocalStore('u_id')) {
      if (!userId) {
        this.baseService.guestSource.next(true);
      }
      this.baseService.startState = '/home';
      this.router.navigateByUrl('home');
    } else {
      this.baseService.startState = '/index';
      this.router.navigateByUrl('index');
    }
    this.settingsService.showHeader = false;
  }

  ngOnInit(): void {
    this.bannerHeading = this.settingsService.bannerHeading;
    this.bannerDescription = this.settingsService.bannerDescription;
    this.siteName = this.settingsService.siteName;
    this.logo = this.settingsService.logo;
    // if (this.baseService.loginMode != 1 || this.storageService.getLocalStore('u_id')) {
    //   // this.router.navigateByUrl('home');
    //   return;
    // }
    // var $elements = $('.animateBlock.notAnimated'); //contains all elements of nonAnimated class
    // var $window = $(window);
    // $window.on('scroll', function (e) {
    //   $elements.each(function (i, elem) { //loop through each element
    //     if ($(this).hasClass('animated')) // check if already animated
    //       return;
    //     // this.animateMe($(this));
    //   });
    // });
  }
  // animateMe(elem) {
  //   var winTop = $(window).scrollTop(); // calculate distance from top of window
  //   var winBottom = winTop + $(window).height();
  //   var elemTop = $(elem).offset().top; // element distance from top of page
  //   var elemBottom = elemTop + $(elem).height();
  //   if ((elemBottom <= winBottom) && (elemTop >= winTop)) {
  //     $(elem).removeClass('notAnimated').addClass('animated');
  //   }
  // }
  ngOnDestroy() {
    this.settingsService.showHeader = true;
  }

}
