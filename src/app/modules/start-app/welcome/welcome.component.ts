import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Router } from '@angular/router';
import { BaseService, StorageService } from 'src/app/core/services';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  startState = '';
  homeImage: any;


  constructor(public settingsService: SettingsService, private router: Router,
              private baseService: BaseService, private storageService: StorageService) {
      this.homeImage = this.settingsService.homeImage;
    }

  ngOnInit(): void {
    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
  }



}
