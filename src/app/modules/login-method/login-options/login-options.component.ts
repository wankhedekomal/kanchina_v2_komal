import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-login-options',
  templateUrl: './login-options.component.html',
  styleUrls: ['./login-options.component.scss']
})
export class LoginOptionsComponent implements OnInit, OnDestroy {

  startState = '';
  homeImage: any;

  constructor(public settingsService: SettingsService, private baseService: BaseService, private storageService: StorageService) {
    this.settingsService.showHeader = false;
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
  }



ngOnDestroy(){
  this.settingsService.showHeader = true;
}

}
