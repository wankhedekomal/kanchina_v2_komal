import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-telco-login',
  templateUrl: './telco-login.component.html',
  styleUrls: ['./telco-login.component.scss']
})
export class TelcoLoginComponent implements OnInit, OnDestroy {

  startState = '';
  signinForm: any;
  showPassword;
  homeImage: any;

  constructor(public settingsService: SettingsService, private baseService: BaseService,
              private formBuilder: FormBuilder, private storageService: StorageService) {
    this.settingsService.showHeader = false;
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }

    this.signinForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  telcoSignin() {

  }

  hidePassword(type) {
    this.showPassword = !this.showPassword;
    const x = document.getElementById('password') as HTMLInputElement;
    if (x.type === 'password') {
      x.type = 'text';
    } else {
      x.type = 'password';
    }
  }
  ngOnDestroy() {
    this.settingsService.showHeader = true;
  }

}
