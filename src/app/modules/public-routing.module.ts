import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';

import { UserGuard } from '../core/gaurds/user.guard';
import { TokenResolverService } from '../core/services/token-resolver.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: '',
    loadChildren: () => import('./content/content.module').then(m => m.ContentModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: 'pages/:type',
    loadChildren: () => import('./static/static.module').then(m => m.StaticModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: 'feedback',
    loadChildren: () => import('./feedback/feedback.module').then(m => m.FeedbackModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: '',
    loadChildren: () => import('./live/live.module').then(m => m.LiveModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: '',
    loadChildren: () => import('./ppv/ppv.module').then(m => m.PpvModule),
    resolve: {
      data: TokenResolverService
    },
    canActivate: [UserGuard],
  },
  {
    path: '',
    loadChildren: () => import('./subcribe-payment/subcribe-payment.module').then(m => m.SubcribePaymentModule)
  },
  {
    path: '',
    loadChildren: () => import('./user/user.module').then(m => m.UserModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: '',
    loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
    resolve: {
      data: TokenResolverService
    },
    canActivate: [UserGuard],
  },
  {
    path: '',
    loadChildren: () => import('./start-app/start-app.module').then(m => m.StartAppModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: '',
    loadChildren: () => import('./start-app/start-app.module').then(m => m.StartAppModule),
    resolve: {
      data: TokenResolverService
    }
  },
  {
    path: '404',
    component: NotFoundComponent,
  },
  { path: '**', component: NotFoundComponent },
  { path: '**', pathMatch: 'full', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }

