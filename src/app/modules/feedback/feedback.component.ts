import { Component, OnInit, ElementRef } from '@angular/core';
import { FeedbackService } from '../../core/services/feedback.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseService } from '../../core/services/base.service';
import { SettingsService } from 'src/app/core/services';
// import { MyAccountService } from '../../core/services/my-account.service';
import { StorageService } from '../../core/services/storage.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { TranslateService } from '@ngx-translate/core';
// import { UikitService } from '../../core/services/uikit.service'

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  // taking user input
  params: {};
  // Feedback string
  feedback: any;
  // Type of feedback
  type = 'bug';
  // User profile data coming from my account service
  profile: any;
  // User form data
  feedbackform: FormGroup;
  // Feedback data coming from response
  feedbackData: any;
  guest;
  email: any;
  feedbackGroup: FormGroup;
  homeImage: any;

  constructor(private element: ElementRef, private baseService: BaseService, private feedbackService: FeedbackService,
              private formBuilder: FormBuilder, public settingsService: SettingsService, private storageService: StorageService,
              private fb: FormBuilder, private uikitService: UikitService, private translate: TranslateService) {
    this.homeImage = this.settingsService.homeImage;
    this.baseService.guest.subscribe(data => this.guest = data);
  }

  ngOnInit(): void {
    // this.element.nativeElement.querySelector('#textarea').focus()
    this.browserDetect();
    this.feedbackGroup = this.fb.group({
      feedback: ['', Validators.required],
      email: ['', Validators.required],
      type: ['bug', Validators.required]
    });
  }
  browserDetect() {
    const nav = window.navigator;
    const ua = window.navigator.userAgent.toLowerCase();
    // Detect browsers (only the ones that have some kind of quirk we need to work around)
    if ((nav.appName.toLowerCase().indexOf('microsoft') !== -1 || nav.appName.toLowerCase().match(/trident/gi) !== null)) {
      return 'IE';
    }
    if (ua.match(/chrome/gi) !== null) {
      return 'Chrome';
    }
    if (ua.match(/firefox/gi) !== null) {
      return 'Firefox';
    }
    if (ua.match(/safari/gi) !== null) {
      return 'Safari';
    }
    if (ua.match(/webkit/gi) !== null) {
      return 'Webkit';
    }
    if (ua.match(/gecko/gi) !== null) {
      return 'Gecko';
    }
    if (ua.match(/opera/gi) !== null) {
      return 'Opera';
    }
    return null;
  }

  submitFeedback() {
    let emailMsg;
    this.translate.get('login_desc_email_req_def').subscribe(data => {
      emailMsg = data;
    });
    if (!this.guest) {
      this.params = {
        u_id: this.storageService.getLocalStore('u_id'),
        type: this.type,
        content: this.feedback,
        d_type: 'web',
        data: {
          device_model: this.browserDetect()
        }
      };
    } else {
      if (!this.email) {
        this.uikitService.staticErrorMessage(emailMsg);
        return;
      }
      this.params = {
        type: this.type,
        content: this.feedback,
        d_type: 'web',
        data: {
          device_model: this.browserDetect()
        },
        email: this.email,
      };
    }
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
      event: 'GAEvent',
      eventCategory: 'Add Comment',
      eventAction: this.type,
      eventLabel: '',
      loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
      userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
      notificationStatus: 'False',
      country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
      plateform: 'Web'
    });
    console.log(window.dataLayer);
    this.feedbackService.feedbackfun(this.params).subscribe({
      next: data => {
        this.feedbackData = data;
      }, complete: () => {
        if (this.feedbackData.status_code === 200) {
          this.uikitService.staticSuccessMessage(this.feedbackData.message);
          this.feedback = '';
          this.type = 'bug';
          this.email = '';
        } else {
          this.uikitService.staticErrorMessage(this.feedbackData.error.message);
        }
      }
    });
  }
}
