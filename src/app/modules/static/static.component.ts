import { StaticService } from './../../core/services/static.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-static',
  templateUrl: './static.component.html',
  styleUrls: ['./static.component.scss']
})
export class StaticComponent implements OnInit, OnDestroy {
  type: any;
  page: any;
  heading: any;
  description: any;
  pageH1Tag;
  startState;
  homeImage: any;

  constructor(public route: ActivatedRoute, public staticService: StaticService, public settingsService: SettingsService,
              private baseService: BaseService, private storageService: StorageService) {
      this.settingsService.showHeader = false;
      this.homeImage = this.settingsService.homeImage;
     }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.type = params.type;
      this.getFooterDetails();
    });
    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
  }

  getFooterDetails() {
    this.baseService.loaderSource.next(true);
    this.staticService.getfooterDetails(this.type)
      .subscribe({
        next: data => {
          this.baseService.loaderSource.next(false);
          this.page = data;
          this.heading = this.page.page.heading;
          this.description = this.page.page.description;
        }
      });
  }

  ngOnDestroy() {
    this.settingsService.showHeader = true;
  }
}
