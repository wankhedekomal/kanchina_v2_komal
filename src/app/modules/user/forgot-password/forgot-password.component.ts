import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ForgotPasswordService } from 'src/app/core/services/forgot-password.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

  pageH1Tag;
  forgetPasswordForm: FormGroup;
  response;
  forgotRes;
  email;
  startState = '';
  homeImage: any;


  constructor(public settingsService: SettingsService, private fb: FormBuilder,
              private forgotService: ForgotPasswordService, private uikitService: UikitService,
              private router: Router, private baseService: BaseService, private storageService: StorageService) {
    this.homeImage = this.settingsService.homeImage;
    this.settingsService.showHeader = false;
  }

  ngOnInit(): void {
    this.forgetPasswordForm = this.fb.group({
      email: ['', [Validators.required]]
    });

    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
  }

  forgot() {
    this.response =
    {
      emai: this.forgetPasswordForm.value.email,
      d_type: 'web',
    };
    this.forgotService.forgotPassService(this.response).subscribe({
      next: data => {
        this.forgotRes = data;
      }, complete: () => {
        if (this.forgotRes.status_code === 200) {
          this.storageService.setLocalStore('mail', this.email);
          this.email = '';
          this.uikitService.staticSuccessMessage(this.forgotRes.message);
          this.router.navigate(['reset']);
        } else {
          this.uikitService.staticErrorMessage(this.forgotRes.error.message);
        }
      }
    });

  }

  ngOnDestroy() {
    this.settingsService.showHeader = true;
  }
}
