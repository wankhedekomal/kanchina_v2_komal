import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { ResendMailService } from 'src/app/core/services/resend-mail.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit, OnDestroy {

  startState = '';
  response: { email: any, email_type_flag: 2, d_type: 'web' };
  resendData: any;
  homeImage: any;

  constructor(public settingsService: SettingsService, private baseService: BaseService,
              private resendService: ResendMailService, private storageService: StorageService,
              private uikitService: UikitService, private router: Router) {
    this.settingsService.showHeader = false;
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
    if(this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
  }

  resend(){
    this.response =
    {
      email: this.storageService.getLocalStore('mail'),
      email_type_flag: 2,
      d_type: 'web',
    };
    this.resendService.resendMail(this.response).subscribe({
      next: data => {
        this.resendData = data;
      }, complete: () => {
        if (this.resendData.status_code === 200) {
          this.uikitService.staticSuccessMessage(this.resendData.message);
          this.storageService.removeLocalStore('mail');
          this.router.navigate(['signin']);
        } else {
          this.uikitService.staticErrorMessage(this.resendData.error.message);
        }
      }
    });
  }


  ngOnDestroy(){
    this.settingsService.showHeader = true;
  }

}
