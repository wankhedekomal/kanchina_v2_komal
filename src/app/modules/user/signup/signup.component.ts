import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { UikitService } from 'src/app/core/services/uikit.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  showPassword = false;
  showConfirmPassword = false;
  signupForm: FormGroup;
  startState = '';
  homeImage: any;


  constructor(private formBuilder: FormBuilder, private loggerService: LoggerService,
              public settingsService: SettingsService, private baseService: BaseService, private uikitService: UikitService,
              private translate: TranslateService, private storageService: StorageService) {
    this.settingsService.showHeader = false;
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
    const dId = this.storageService.getLocalStore('d_id');
    this.signupForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      cPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      login_by: ['manual'],
      d_type: ['web'],
      d_id: [dId]
    }, { Validators : this.passwordCheck});
  }

  passwordCheck(group: FormGroup) {
    const password = group.get('password').value;
    const cPassword = group.get('confirmPass').value;
    return password === cPassword ? true : false;
  }

  signup() {
    let passErr;
    this.translate.get('sign_up_desc_psw_nt_mtch_def').subscribe(data => {
      passErr = data;
    });
    const pass = this.signupForm.get('password').value;
    const cPass = this.signupForm.get('cPassword').value;
    if (pass === cPass){
      this.loggerService.signUpFn(this.signupForm.value);
    } else {
      this.uikitService.staticErrorMessage(passErr);
    }
  }

  hidePassword(type) {
    if (type === 'password') {
      this.showPassword = !this.showPassword;
      const x = document.getElementById('password') as HTMLInputElement;
      if (x.type === 'password') {
        x.type = 'text';
      } else {
        x.type = 'password';
      }
    } else {
      this.showConfirmPassword = !this.showConfirmPassword;
      const x = document.getElementById('cPassword') as HTMLInputElement;
      if (x.type === 'password') {
        x.type = 'text';
      } else {
        x.type = 'password';
      }
    }
  }


  ngOnDestroy() {
    this.settingsService.showHeader = true;
  }
}
