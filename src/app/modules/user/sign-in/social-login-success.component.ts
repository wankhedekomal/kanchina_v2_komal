import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { error } from 'protractor';
import { BaseService, StorageService } from 'src/app/core/services';
import { SocialService } from 'src/app/core/services/social.service';
import { UikitService } from 'src/app/core/services/uikit.service';

@Component({
  selector: 'app-social-login-success',
  template: `
    <p>
      social-login-success works!
    </p>
  `,
  styles: [
  ]
})
export class SocialLoginSuccessComponent implements OnInit {

  constructor(private route: ActivatedRoute, private socialService: SocialService, private baseService: BaseService,
    private router: Router, private uikitService: UikitService, private storageService: StorageService) { }

  ngOnInit(): void {
    this.storageService.removeLocalStore('d_id');
    this.storageService.removeLocalStore('a_t');
    this.storageService.removeLocalStore('r_t');
    this.storageService.setLocalStore('logged_in', '1');
    let g_id = this.storageService.getLocalStore('g_id')
    this.route.params.subscribe(params => {
      let u_id = params['u_id']
      let d_id = params['d_id']
      this.storageService.setLocalStore('u_id', u_id);
      this.storageService.setLocalStore('d_id', d_id);
      if (u_id) {
        params = { d_type: 'web', d_id: d_id, u_id: u_id }
      } else {
        params = { d_type: 'web', d_id: d_id, g_id: g_id }
      }
      this.socialService.socailToken(params).subscribe(
        (data) => {
          setTimeout(() => {
            this.userInfo();
          }, 100);
        }
      )
    })
  }

  userInfo() {
    this.socialService.userToken().subscribe(
      (res) => {
        if (res.status_code === 200) {
          let memoryStorage;
          memoryStorage.login_by = res.data.user_login_by;
          memoryStorage.user_picture = res.data.user_picture;
          memoryStorage.user_name = res.data.user_name;
          memoryStorage.user_type = res.data.user_type;
          memoryStorage.one_time_subscription = res.data.one_time_subscription;
          this.storageService.setLocalStore('sessionStorage', JSON.stringify(memoryStorage));
          memoryStorage.u_id = res.data.user_id;
          this.baseService.guestSource.next(false);
          window.dataLayer = window.dataLayer || [];
          window.dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': 'Membership Actions',
            'eventAction': 'Login',
            'eventLabel': 'Success - Gmail/Facebook',
            'loginStatus': 'True',
            'userId': memoryStorage.u_id,
            'notificationStatus': 'False',
            'country': (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            'plateform': "Web"
          });
          console.log(window.dataLayer);
          if (this.storageService.getLocalStore('authenticate')) {
            this.storageService.removeLocalStore('authenticate');
            this.router.navigateByUrl('/authenticate')
          } else
            this.router.navigateByUrl('/home')
        } else {
          window.dataLayer = window.dataLayer || [];
          window.dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': 'Membership Actions',
            'eventAction': 'Login',
            'eventLabel': 'Error - Username',
            'loginStatus': 'False',
            'userId': '',
            'notificationStatus': 'False',
            'country': (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            'plateform': "Web"
          });
          console.log(window.dataLayer);
          this.uikitService.notifyError(res)
        }
      },
      (error) => {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
          'event': 'GAEvent',
          'eventCategory': 'Membership Actions',
          'eventAction': 'Login',
          'eventLabel': 'Error - Username',
          'loginStatus': 'False',
          'userId': '',
          'notificationStatus': 'False',
          'country': (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
          'plateform': "Web"
        });
        console.log(window.dataLayer);
        if (error.error && error.error.code !== 1002) {
          this.uikitService.notifyError(error)
        }
      }
    )
  }

}
