import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { socialURL } from 'src/app/shared/constants';
import { LoggerService } from 'src/app/core/authentication/logger.service';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit, OnDestroy {
  fbStatus: any;
  googleStatus: any;
  startState = '';
  homeImage: any;

  constructor(private loggerService: LoggerService, public settingsService: SettingsService,
              private formBuilder: FormBuilder, private baseService: BaseService, private storageService: StorageService) {
    this.settingsService.showHeader = false;
    this.homeImage = this.settingsService.homeImage;
    window.dataLayer = window.dataLayer || [];
  }

  signinForm: FormGroup;
  showPassword = false;

  ngOnInit(): void {
    if (this.baseService.loginMode === 0) {
      if (!this.storageService.getLocalStore('u_id')) {
        this.baseService.guestSource.next(true);
      }
      this.startState = '/home';
    } else {
      this.startState = '/index';
    }
    this.signinForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      login_by: ['manual'],
      d_type: ['web'],
      device_token: ['123456']
    })
    this.fbStatus = this.settingsService.facebookSignin;
    this.googleStatus = this.settingsService.googleSignin;
  }

  hidePassword(type) {
    if (type === 'password') {
      this.showPassword = !this.showPassword;
      const x = document.getElementById('password') as HTMLInputElement;
      if (x.type === 'password') {
        x.type = 'text';
      } else {
        x.type = 'password';
      }
    }
  }

  social_login(provider) {
    window.location.href = socialURL + 'social?provider=' + provider + '&d_id=' + this.storageService.getLocalStore('d_id');
  }

  signin() {
    this.loggerService.loginFn(this.signinForm.value);
  }

  ngOnDestroy() {
    this.settingsService.showHeader = true;
  }
}
