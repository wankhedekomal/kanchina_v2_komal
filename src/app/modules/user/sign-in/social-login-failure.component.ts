import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UikitService } from 'src/app/core/services/uikit.service';

@Component({
  selector: 'app-social-login-failure',
  template: `
    <p>
      social-login-failure works!
    </p>
  `,
  styles: [
  ]
})
export class SocialLoginFailureComponent implements OnInit {

  constructor(private route: ActivatedRoute, private uikitService: UikitService, private translate: TranslateService,
    private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params['id']
      if (id == 100) {
        let loginDeined
        this.translate.get('login_msg_login_dnd_def').subscribe(data => {
          loginDeined = data;
        })
        this.uikitService.staticErrorMessage(loginDeined)
      } else if (id == 101) {
        let fbEmailLinkFail
        this.translate.get('login_msg_fbnot_link_def').subscribe(data => {
          fbEmailLinkFail = data;
        })
        this.uikitService.staticErrorMessage(fbEmailLinkFail)
      } else {
        let accountDisabled
        this.translate.get('login_msg_dsbl_acct_def').subscribe(data => {
          accountDisabled = data;
        })
        this.uikitService.staticErrorMessage(accountDisabled)
      }
      this.router.navigateByUrl('/signin')
    })
  }

}
