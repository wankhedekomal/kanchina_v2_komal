import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingsService, StorageService } from 'src/app/core/services';
import { ResendMailService } from 'src/app/core/services/resend-mail.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.scss']
})
export class ConfirmAccountComponent implements OnInit, OnDestroy {

  pageH1Tag;
  response: { email: any, email_type_flag: 1, d_type: 'web' };
  resendData: any;
  startState = '';
  logo: any;
  homeImage: any;

  constructor(public settingsService: SettingsService, private resendService: ResendMailService,
              private uikitService: UikitService, private router: Router, private storageService: StorageService) {
    this.homeImage = this.settingsService.homeImage;
    this.settingsService.showHeader = false;
  }

  ngOnInit(): void {
  }

  resend() {
    this.response =
    {
      email: this.storageService.getLocalStore('mail'),
      email_type_flag: 1,
      d_type: 'web',
    };
    this.resendService.resendMail(this.response).subscribe({
      next: data => {
        this.resendData = data;
      }, complete: () => {
        if (this.resendData.status_code === 200) {
          this.uikitService.staticSuccessMessage(this.resendData.message);
          this.router.navigate(['signin']);
        } else {
          this.uikitService.notifyError(this.resendData.error.message);
        }
        this.uikitService.staticErrorMessage(this.resendData.error.message);
      }
    });
  }

  ngOnDestroy() {
    this.settingsService.showHeader = true;
  }
}
