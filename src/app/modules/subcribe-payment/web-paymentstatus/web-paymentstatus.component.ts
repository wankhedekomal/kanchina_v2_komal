import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-web-paymentstatus',
  templateUrl: './web-paymentstatus.component.html',
  styleUrls: ['./web-paymentstatus.component.scss']
})
export class WebPaymentstatusComponent implements OnInit, OnDestroy {
  logo: any;
  startState: any;
  showIosApp: boolean;
  showAndroidApp: boolean;
  transactionDetails: any;
  homeImage: any;

  constructor(public settingsService: SettingsService, private baseService: BaseService, private router: Router,
              private route: ActivatedRoute, private storageService: StorageService) {
    this.homeImage = this.settingsService.homeImage;
    this.logo = this.settingsService.logo;
    this.startState = this.baseService.startState;
    this.settingsService.showHeader = false;
    window.dataLayer = window.dataLayer || [];
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.transactionDetails = params;
    });
    this.baseService.checkDevice();
    this.showAndroidApp = this.baseService.showAndroidApp;
    this.showIosApp = this.baseService.showIosApp;
    this.paymentCheck();
  }

  paymentCheck() {
    if (this.transactionDetails.payment_status === 'PAID') {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({
        event: 'GAEvent',
        eventCategory: 'Payment',
        eventAction: 'Success',
        eventLabel:
        (this.transactionDetails.subscription_plan ? this.transactionDetails.subscription_plan : this.transactionDetails.content_title) + ' - ' + this.transactionDetails.amount,
        loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        userId: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        notificationStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
        plateform: 'Web'
      });
      console.log(window.dataLayer);
    } else {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({
        event: 'GAEvent',
        eventCategory: 'Payment',
        eventAction: 'Error',
        eventLabel:
        (this.transactionDetails.subscription_plan ? this.transactionDetails.subscription_plan : this.transactionDetails.content_title) + ' - ' + this.transactionDetails.amount,
        loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        userId: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        notificationStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
        country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
        plateform: 'Web'
      });
      console.log(window.dataLayer);
    }
  }

  redirect() {
    const videoName = this.settingsService.slugify(this.transactionDetails.content_title);
    window.dataLayer.push({
      event: 'GAEvent',
      eventCategory: 'Content Click',
      eventAction: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
      eventLabel: this.transactionDetails.content_title,
      loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
      userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
      content: this.transactionDetails.content_title,
      VideoCategory: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
      notificationStatus: 'False',
      country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
      plateform: 'Web'
    });
    if (this.transactionDetails.content_type === 'series') {
      this.router.navigate(['/series', this.transactionDetails.content_id, videoName]);
    } else {
      this.router.navigate(['/video', this.transactionDetails.content_id, videoName]);
    }
  }

  ngOnDestroy() {
    this.settingsService.showFooter = true;
    this.settingsService.showHeader = true;
  }

}
