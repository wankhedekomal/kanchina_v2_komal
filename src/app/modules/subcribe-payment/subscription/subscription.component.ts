import { Component, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StripeService } from 'src/app/core/services/stripe.service';
import { MatDialog } from '@angular/material/dialog';
import * as $ from 'jquery';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
declare var UIkit: any;
@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  subscriptions: any[];
  subscriptionsLength: any;
  noSubscriptionText: string;
  oneTime: any;
  updatedPayment: any;
  updatePaymentUrl: any;
  stripeSubscriptionId: any;
  subscribedPlanId: any;
  userId: string;
  windowWidth: number;
  cancelclicked = 0;
  homeImage: any;

  constructor(private router: Router, private subscriptionService: SubscriptionService,
              public settingsService: SettingsService, private translate: TranslateService, private stripeService: StripeService,
              private baseService: BaseService, public dialog: MatDialog, private storageService: StorageService) {
    this.userId = this.storageService.getLocalStore('u_id');
    this.windowWidth = window.innerWidth;
    this.homeImage = this.settingsService.homeImage;
    this.baseService.loaderSource.next(true);
  }

  ngOnInit() {
    this.subscription_index();
  }

  subscription_index() {
    this.subscriptions = [];
    this.subscriptionService.userPlan().subscribe((data) => {
      if (data) {
        this.baseService.loaderSource.next(false);
        this.noSubscriptionText = '';
        this.subscriptions = data.plans;
        this.subscriptionsLength = data.plans.length;
        this.oneTime = data.one_time_subscription;
        if (this.subscriptions.length === 0) {
          this.noSubscriptionText = 'subsc_desc_prsnt_def';
        }
        if (data.subscribed_plan_id) {
          this.subscribedPlanId = data.subscribed_plan_id;
          this.stripeSubscriptionId = data.stripe_subscription_id;
        }
        this.updatedPayment = data.update_payment;
        this.updatePaymentUrl = data.update_payment_url;
      }
    });
  }

  resumePlanApi(plan) {
    const paramData = {
      d_type: 'web',
      u_id: this.userId,
      subscription_id: plan.subscription_id,
      stripe_subscription_id: this.subscribedPlanId
    };
    this.subscriptionService.resumePlanApiFun(paramData).subscribe((data) => {
      if (data) {
        this.subscription_index();
      }
    });
  }

  resumePlan(plan) {
    if (this.windowWidth < 1025) {
      this.resumePlanApi(plan);
    } else {
      let title;
      let content;
      this.translate.get('subsc_noti_def').subscribe(data => {
        title = data;
      });
      this.translate.get('subsc_plan_resum_def').subscribe(data => {
        content = data;
      });
      UIkit.modal.confirm(`<div class="uk-modal-body"><div class="uk-modal-title">${title}</div><p>${content}</p></div>`).then(() => {
        this.resumePlanApi(plan);
      });
    }
  }

  changePlanApi(plan) {
    const changePlandata = {
      d_type: 'web', u_id: this.storageService.getLocalStore('u_id'), subscription_id: plan.subscription_id,
      old_subscription_id: this.subscribedPlanId, stripe_subscription_id: this.stripeSubscriptionId,
      stripe_plan_id: plan.stripe_plan_id
    };
    this.subscriptionService.changePlanApiFun(changePlandata).subscribe((data) => {
      if (data) {
        this.subscription_index();
      }
    });
  }

  changePlan(plan) {
    if (this.windowWidth < 1025) {
      this.changePlanApi(plan);
    } else {
      let title;
      let content;
      this.translate.get('subsc_noti_def').subscribe(data => {
        title = data;
      });
      this.translate.get('subsc_chng_plan_def').subscribe(data => {
        content = data;
      });
      UIkit.modal.confirm(`<div class="uk-modal-body"><div class="uk-modal-title">${title}</div><p>${content}</p></div>`).then(() => {
        this.changePlanApi(plan);
      });
    }
  }


  cancelSubscription(plan) {
    if (this.windowWidth < 1025) {
      this.cancelclicked = 1;
      this.router.navigate(['/subscription-cancel', plan.subscription_id, this.stripeSubscriptionId]);
    } else {
      let title;
      let content;
      this.translate.get('subsc_noti_def').subscribe(data => {
        title = data;
      });
      this.translate.get('subsc_unsubscr_plan_def').subscribe(data => {
        content = data;
      });
      UIkit.modal.confirm(`<div class="uk-modal-body"><div class="uk-modal-title">${title}</div><p>${content}</p></div>`).then(() => {
        this.router.navigate(['/subscription-cancel', plan.subscription_id, this.stripeSubscriptionId]);
      });
    }
  }

  checkout(plan, redirection) {
    if (redirection === 'checkout') {
      const postData = {
        d_type: 'web',
       u_id: this.storageService.getLocalStore('u_id'),
       subscription_id: plan.subscription_id,
       stripe_plan_id: plan.stripe_plan_id
      };
      this.subscriptionService.checkOut(postData).subscribe((data) => {
        if (data) {
          this.stripeService.checkout(data.session.id);
        }
      });
    } else if (redirection === 'invoice') {
      this.router.navigate(['/invoice', plan.subscription_id, plan.stripe_plan_id]);
    }
  }

  updateMethod() {
    this.subscriptionService.updateMethodApi().subscribe((data) => {
      if (data) {
        this.stripeService.checkout(data.session.id);
      }
    });
  }

  updatePayment() {
    window.open(this.updatePaymentUrl, '_blank');
  }

}
