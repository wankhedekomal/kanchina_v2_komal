import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/core/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-manage-subcriptions',
  templateUrl: './manage-subcriptions.component.html',
  styleUrls: ['./manage-subcriptions.component.scss']
})
export class ManageSubcriptionsComponent implements OnInit {
  imagePath: any;
  type: any;
  homeImage: any;

  constructor(public settingsService: SettingsService, private route: ActivatedRoute) {
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {
        if (Object.keys(params).length) {
          this.type = params.type;
        }
      });
  }

}
