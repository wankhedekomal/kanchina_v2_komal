import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SettingsService, BaseService } from 'src/app/core/services';

@Component({
  selector: 'app-update-method',
  templateUrl: './update-method.component.html',
  styleUrls: ['./update-method.component.scss']
})
export class UpdateMethodComponent implements OnInit, OnDestroy {
  status: any;
  showIosApp: boolean;
  showAndroidApp: boolean;
  homeImage: any;
  logo: any;
  startState: string;

  constructor(private route: ActivatedRoute, public settingsService: SettingsService, private baseService: BaseService) {
    this.homeImage = this.settingsService.homeImage;
    this.logo = this.settingsService.logo;
    this.startState = this.baseService.startState;
    this.settingsService.showHeader = false;
    this.settingsService.showFooter = false;
  }

  ngOnInit(): void {
    this.baseService.checkDevice();
    this.showAndroidApp = this.baseService.showAndroidApp;
    this.showIosApp = this.baseService.showIosApp;
    this.route.params
      .subscribe(params => {
        if (Object.keys(params).length) {
          this.status = params.status;
        }
      });
  }

  ngOnDestroy() {
    this.settingsService.showHeader = true;
    this.settingsService.showFooter = true;
  }

}
