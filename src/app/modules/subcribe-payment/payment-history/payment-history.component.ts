import { Component, OnInit } from '@angular/core';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { BaseService, SettingsService } from 'src/app/core/services';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
// import * as $ from 'jquery';
@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.scss']
})
export class PaymentHistoryComponent implements OnInit {

  paidVideos = [];
  paidvodLength = 0;
  subscriptions = [];
  subscriptionLength = 0;
  voucherHistory = [];
  voucherLength = 0;
  userPicture: any;
  userName: any;
  imagePath: any;
  amount: any;
  selectedTab: string;
  homeImage: any;

  constructor(private loggerService: LoggerService, public settingsService: SettingsService,
              private subscriptionService: SubscriptionService, private baseService: BaseService) {
      this.baseService.loaderSource.next(true);
      this.homeImage = this.settingsService.homeImage;
    }

  ngOnInit(): void {
    this.loggerService.memoryStorage.subscribe(data => {
      if (data) {
        this.userPicture = data.user_picture;
        this.userName = data.user_name;
      }
    });
    this.imagePath = this.settingsService.imagePath;
    this.subscriptionService.userPayments().subscribe((data) => {
      this.baseService.loaderSource.next(false);
      if (Array.isArray(data) && data.length > 0 || typeof data === 'object') {
        if (data.ppv_payments) {
          this.paidVideos = data.ppv_payments;
          this.paidvodLength = this.paidVideos.length;
        }
        if (data.subscription_payments) {
          this.subscriptions = data.subscription_payments;
          this.subscriptionLength = this.subscriptions.length;
        }
        if (data.voucher_history) {
          this.voucherHistory = data.voucher_history;
          this.voucherLength = this.voucherHistory.length;
        }
        if (this.subscriptions.length > 0) {
          this.display_content('subscription-details');
        } else if (this.paidVideos.length > 0) {
          this.display_content('ppv-details');
        } else if (this.voucherHistory.length > 0) {
          this.display_content('voucher_history');
        }
        this.amount = data.sub_total_amount;
      }
    });

  }


  display_content(id) {
    if (this.selectedTab !== id) {
      this.selectedTab = id;
      // $('ul.tabs li').removeClass('current');
      // $('.tab-content').removeClass('current');
      // $("#" + id).addClass('current');
    }
  }

}
