import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageSubcriptionsComponent } from './manage-subcriptions/manage-subcriptions.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { SubscriptionCancelComponent } from './subscription-cancel/subscription-cancel.component';
import { SubscriptionInvoiceComponent } from './subscription-invoice/subscription-invoice.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { UpdateMethodComponent } from './update-method/update-method.component';
import { VoucherComponent } from './voucher/voucher.component';
import { WebPaymentstatusComponent } from './web-paymentstatus/web-paymentstatus.component';

import { SubcribePaymentRoutingModule } from './subcribe-payment-routing.module';

@NgModule({
  declarations: [PaymentHistoryComponent, SubscriptionComponent, SubscriptionCancelComponent, SubscriptionInvoiceComponent,
    UpdateMethodComponent, VoucherComponent, WebPaymentstatusComponent, ManageSubcriptionsComponent],
  imports: [
    CommonModule,
    SubcribePaymentRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
        deps: [HttpClient]
      }
    })
  ]
})
export class SubcribePaymentModule { }
