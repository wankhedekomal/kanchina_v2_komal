import { Component, OnInit } from '@angular/core';
import { UikitService } from 'src/app/core/services/uikit.service';
import { TranslateService } from '@ngx-translate/core';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { SettingsService } from 'src/app/core/services';

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.scss']
})
export class VoucherComponent implements OnInit {

  voucherSuccessPage = false;
  message = '';
  successDesc = '';
  voucherCode: string;
  homeImage: any;
  constructor(private uikitService: UikitService, private translate: TranslateService, private subscriptionService: SubscriptionService,
              private loggerService: LoggerService, public settingsService: SettingsService) {
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
  }


  voucher_apply() {
    if (this.voucherCode === '' || this.voucherCode === 'undefined') {
      let voucherReq;
      this.translate.get('cng_pass_msg_pwd_def').subscribe(data => {
        voucherReq = data;
      });
      this.uikitService.staticErrorMessage(voucherReq);
      return;
    }
    // $("#before_loader").fadeIn();
    const Coupendata = { voucherCode: this.voucherCode, d_type: 'web' };
    this.subscriptionService.applyCoupon(Coupendata).subscribe((resp) => {
      if (resp) {
        this.voucherSuccessPage = true;
        this.message = resp.message;
        this.successDesc = resp.data.desc;
        this.loggerService.updatedMemoryStorage({});
      } else {
        this.uikitService.staticErrorMessage(resp.error.message);
      }
    });
  }

}
