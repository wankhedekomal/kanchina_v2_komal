import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGuard } from 'src/app/core/gaurds/user.guard';
import { TokenResolverService } from 'src/app/core/services/token-resolver.service';
import { ManageSubcriptionsComponent } from './manage-subcriptions/manage-subcriptions.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { SubscriptionCancelComponent } from './subscription-cancel/subscription-cancel.component';
import { SubscriptionInvoiceComponent } from './subscription-invoice/subscription-invoice.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { UpdateMethodComponent } from './update-method/update-method.component';
import { VoucherComponent } from './voucher/voucher.component';
import { WebPaymentstatusComponent } from './web-paymentstatus/web-paymentstatus.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'subscriptions',
        component: SubscriptionComponent,
        canActivate: [UserGuard],
        resolve: {
          data: TokenResolverService
        }
      },
      {
        path: 'subscription-cancel/:id/:orderId',
        component: SubscriptionCancelComponent,
        canActivate: [UserGuard],
        resolve: {
          data: TokenResolverService
        }
      },
      {
        path: 'invoice/:subscription_id/:orderId',
        component: SubscriptionInvoiceComponent,
        canActivate: [UserGuard],
        resolve: {
          data: TokenResolverService
        }
      },
      {
        path: 'manage-subscription/:type',
        component: ManageSubcriptionsComponent,
        resolve: {
          data: TokenResolverService
        }
      },
      {
        path: 'update/method/:status',
        component: UpdateMethodComponent,
        resolve: {
          data: TokenResolverService
        }
      },
      {
        path: 'redeem/voucher',
        component: VoucherComponent,
        canActivate: [UserGuard],
        resolve: {
          data: TokenResolverService
        }
      },
      {
        path: 'payment-status',
        component: WebPaymentstatusComponent,
        resolve: {
          data: TokenResolverService
        }
      },
      {
        path: 'payment-details',
        component: PaymentHistoryComponent,
        canActivate: [UserGuard],
        resolve: {
          data: TokenResolverService
        }
      }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubcribePaymentRoutingModule { }
