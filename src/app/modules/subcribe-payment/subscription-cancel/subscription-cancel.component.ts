import { Component, OnInit } from '@angular/core';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
import { ActivatedRoute } from '@angular/router';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-subscription-cancel',
  templateUrl: './subscription-cancel.component.html',
  styleUrls: ['./subscription-cancel.component.scss']
})
export class SubscriptionCancelComponent implements OnInit {
  showIosApp: boolean;
  showAndroidApp: boolean;
  cancelled;
  transactionDetails: any;
  orderId: any;
  id: any;
  homeImage: any;

  constructor(private subscriptionService: SubscriptionService, private route: ActivatedRoute,
              public settingsService: SettingsService, private baseService: BaseService, private storageService: StorageService) {
    this.homeImage = this.settingsService.homeImage;
    this.baseService.loaderSource.next(true);
  }

  ngOnInit(): void {
    this.baseService.checkDevice();
    this.showAndroidApp = this.baseService.showAndroidApp;
    this.showIosApp = this.baseService.showIosApp;
    this.route.params
      .subscribe(params => {
        if (Object.keys(params).length) {
          this.id = params.id;
          this.orderId = params.orderId;
          const data = {
            subscription_id: this.id,
            d_type: 'web',
            u_id: this.storageService.getLocalStore('u_id'),
            stripe_subscription_id: this.orderId
          };
          this.subscriptionService.cancelSubscriptionApiFun(data).subscribe((resp) => {
            if (resp) {
              this.baseService.loaderSource.next(false);
              if (resp.status_code === 200) {
                this.cancelled = true;
                this.transactionDetails = resp.data;
              } else {
                this.cancelled = false;
                this.transactionDetails = resp.error;
              }
            }
          });
        }
      });
  }

}
