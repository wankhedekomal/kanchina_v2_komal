import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { StripeService } from 'src/app/core/services/stripe.service';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-subscription-invoice',
  templateUrl: './subscription-invoice.component.html',
  styleUrls: ['./subscription-invoice.component.scss']
})
export class SubscriptionInvoiceComponent implements OnInit {
  subscriptionId: any;
  orderId: any;
  plan: any;
  billingDate: any;
  nextBillingDate: any;
  oneTime: any;
  couponSelected = false;
  couponApplied = false;
  coupon = '';
  coupondata: any;
  couponMessage: any;
  payableAmount: any;
  discountAmount: any;
  payNowSubscription = false;
  homeImage: any;
  coupenResp: any;

  constructor(private route: ActivatedRoute, private subscriptionService: SubscriptionService,
              private uikitService: UikitService, private stripeService: StripeService, private router: Router,
              public settingsService: SettingsService, private baseService: BaseService, private storageService: StorageService) {
      this.homeImage = this.settingsService.homeImage;
      this.baseService.loaderSource.next(true);
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {
        if (Object.keys(params).length) {
          this.subscriptionId = params.subscription_id;
          this.orderId = params.orderId;
          const Subdata = { stripe_plan_id: this.orderId, subscription_id: this.subscriptionId };
          this.subscriptionService.invoiceUserPlan(Subdata).subscribe((data) => {
            if (data) {
              this.baseService.loaderSource.next(false);
              this.plan = data.plan;
              this.billingDate = data.next_billing_date;
              this.nextBillingDate = data.next_billing_date;
              this.oneTime = data.one_time_subscription;
            }
          });
        }
      });
  }

  showCoupon(value) {
    if (value === 'apply') {
      if (this.couponApplied) {
        this.couponSelected = false;
      } else {
        this.couponSelected = true;
      }
    } else if (value === 'remove') {
      this.couponSelected = false;
      this.couponApplied = false;
      this.coupon = '';
      this.nextBillingDate = this.billingDate;
    }
  }

  applyCoupon(coupon) {
    const params = {
      subscription_id: this.subscriptionId, stripe_plan_id: this.orderId, coupon_code: coupon,
      d_type: 'web', u_id: this.storageService.getLocalStore('u_id')
    };
    this.subscriptionService.validateCoupon(params).subscribe((resp) => {
      this.coupenResp = resp;
      if (this.coupenResp.status_code === 200) {
        const respData = this.coupenResp.data;
        this.coupon = coupon;
        this.couponSelected = false;
        this.coupondata = respData.coupon;
        this.couponMessage = respData.message;
        this.couponApplied = true;
        this.payableAmount = respData.payable_amount;
        this.discountAmount = respData.discount_amount;
        this.nextBillingDate = respData.next_billing_date;
      } else {
        this.coupon = '';
        this.uikitService.notifyError(resp);
      }
    }, (error) => {
      this.coupon = '';
      this.uikitService.notifyError(error);
    });
  }
  checkout() {
    this.payNowSubscription = true;
    let params = {};
    if (this.coupon !== '') {
      params = {
        d_type: 'web', u_id: this.storageService.getLocalStore('u_id'),
        subscription_id: this.plan.subscription_id, stripe_plan_id: this.plan.stripe_plan_id, coupon_code: this.coupon
      };
    } else {
      params = {
        d_type: 'web',
        u_id: this.storageService.getLocalStore('u_id'),
        subscription_id: this.plan.subscription_id,
        stripe_plan_id: this.plan.stripe_plan_id
      };
    }
    this.subscriptionService.checkOut(params).subscribe(
      (res) => {
        this.payNowSubscription = false;
        if (res.status_code === 200) {
          this.stripeService.checkout(res.data.session.id);
          // $state.go('profile.checkout', { id: res.data.session.id, region: this.storageService.getLocalStore('region') });
        } else {
          this.uikitService.notifyError(res);
          if (res.error.code === 2192) {
            this.router.navigateByUrl('/subscriptions');
          }
        }
      },
      (err) => {
        this.payNowSubscription = false;
        if (err.error && err.error.code !== 1002) {
          this.uikitService.notifyError(err);
        }
      }
    );
  }

  // $scope.sendToPaypal = function (id, amt) {
  //   if (!$scope.type_of_payment) {
  //     UIkit.notify({ message: $filter('translate')('pay_summ_desc_mode_def'), timeout: 3000, pos: 'top-center', status: 'danger' });
  //     return;
  //   }
  //   $("#payNowSubscription").html("Request Sending...");
  //   $("#payNowSubscription").attr('disabled', true);
  //   var isMobile = {
  //     iOS: function () {
  //       return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  //     }
  //   }
  //   if (isMobile.iOS()) {
  //     $scope.deviceType = 'ios';
  //   } else {
  //     $scope.deviceType = 'web';
  //   }
  //   if ($scope.type_of_payment == 2) {
  //     $scope.paymentOptions = "visa";
  //   } else if ($scope.type_of_payment == 4) {
  //     $scope.paymentOptions = "ideal";
  //   }
  //   $http({
  //     url: api + "payments/start/transaction",
  //     method: 'post',
  //     async: false,
  //     params: { paymentOptions: $scope.paymentOptions, d_type: $scope.deviceType, u_id: this.storageService.getLocalStore('u_id'),
  // subscription_id: $stateParams.subscription_id },
  //   }).then(function (res) {
  //     if (res.status_code == 200) {
  //       window.location.href = res.data.redirect_url;
  //     } else {
  //       UIkit.notify({ message: res.error.message, timeout: 3000, pos: 'top-center', status: 'danger' });
  //     }
  //   }, function (res) {
  //     //error_codes 2001,2002,2003
  //     if (res.error && res.error.code !== 1002) {
  //       UIkit.notify({ message: res.error.message, timeout: 3000, pos: 'top-center', status: 'danger' });
  //     }
  //     return false;
  //   });
  // }
}
