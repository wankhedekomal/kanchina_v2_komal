import { Component, OnDestroy, OnInit } from '@angular/core';
import { BaseService, SettingsService } from 'src/app/core/services';
import { ChannelsService } from 'src/app/core/services/channels.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { Router } from '@angular/router';
import { merge } from 'rxjs';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit, OnDestroy {

  channelsData;
  chaneelsDataLength;
  channels;
  noChannelText;
  value;
  imagePath;
  windowWidth;
  slideToShowSeries;
  isLive;
  executed;
  params;
  homeImage;
  livechannels;

  constructor(public settingsService: SettingsService, private channelsService: ChannelsService,
              private router: Router, private baseService: BaseService) {
    this.imagePath = this.settingsService.imagePath;
    this.homeImage = this.settingsService.homeImage;
    this.baseService.loaderSource.next(true);
     }

  ngOnInit(): void {
    this.windowWidth = window.innerWidth;
    if (this.windowWidth > 1450) {
      this.slideToShowSeries = 5;
    } else if (this.windowWidth > 1023 && this.windowWidth < 1451) {
      this.slideToShowSeries = 4;
    } else if (this.windowWidth > 991 && this.windowWidth < 1024) {
      this.slideToShowSeries = 4;
    } else if (this.windowWidth > 767 && this.windowWidth < 992) {
      this.slideToShowSeries = 4;
    } else if (this.windowWidth > 479 && this.windowWidth < 768) {
      this.slideToShowSeries = 3;
    } else if (this.windowWidth < 480) {
      this.slideToShowSeries = 2;
    }
    this.isLive = true;
    // $(window).scroll(function () {
    //   if ($(window).scrollTop() == $(document).height() - $(window).height()) {
    //     // ajax call get data from server and append to the div
    //     $("#load_more_channels").click();
    //   }
    // });
    this.channels = [];
    this.noChannelText = '';
    this.executed = 0;
    this.channelsFn(0, 16);
  }

  loadMoreDetails() {
    const dataLength = this.channels.length;
    if (dataLength > 0 && this.executed === 1) {
      this.executed = 0;
      this.channelsFn(dataLength, 16);
    }
  }

  channelsFn(skip, take) {
    this.params = {
      d_type: 'web',
      skip,
      take
    };
    this.channelsService.getChannels(this.params).subscribe({
      next: data => {
        this.baseService.loaderSource.next(false);
        this.channelsData = data;
      }, complete: () => {
        if (this.channelsData.status_code === 200) {
          this.livechannels = this.channelsData.data.livechannels.length;
          if (this.livechannels > 0) {
            if (this.channels > 0) {
              this.channels = merge(this.channels, this.channelsData.data.livechannels);
              this.chaneelsDataLength = this.channels.length;
              for (let index in this.channels) {
                let slug = this.settingsService.slugify(this.channels[index].title);
                this.channels[index].slug = slug;
              }
            } else {
              this.channels = this.channelsData.data.livechannels;
              this.chaneelsDataLength = this.channels.length;
              for (let index in this.channels) {
                let slug = this.settingsService.slugify(this.channels[index].title);
                this.channels[index].slug = slug;
              }
            }
            if (this.channels.length === 0) {
              this.noChannelText = 'Present no live channels available.';
            }
            this.executed = 1;
          } else {
            this.executed = 0;
            if (this.channels.length === 0) {
              this.noChannelText = 'Present no live channels available.';
            }
          }
        } else {
          this.router.navigate(['/404']);
          // this.uikitService.staticErrorMessage(this.channelsData.error.message)
          // UIkit.notify({ message: res.error.message, timeout: 3000, pos: 'top-center', status: 'danger' });
        }
      }
    });
  }

  ngOnDestroy() {
    this.isLive =  false;
  }

}
