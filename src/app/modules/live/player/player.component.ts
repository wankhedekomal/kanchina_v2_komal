import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { SettingsService, BaseService } from 'src/app/core/services';
import { Location } from '@angular/common';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PlayerService } from 'src/app/core/services/player.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { ContentService } from 'src/app/core/services/content.service';
import { HttpClient, HttpParams } from '@angular/common/http';
declare function videojs(player: any): any;
declare function tv2zPlayer(options: object): any;

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, OnDestroy {

  video;
  back = true;
  close = true;
  playerId: any;
  windowWidth: any;
  width: any;
  holaWidth: any;
  height: any;
  videoId: any;
  contentType: string;
  response: any;
  playerData: any;
  isSvodEnabled: any;
  videoStartTime;
  playerActive: boolean;
  watchCount;
  endTime;
  durationInSeconds;
  guest;

  constructor(private settingService: SettingsService, private location: Location, private loggerService: LoggerService,
              private router: Router, private route: ActivatedRoute, private playerService: PlayerService,
              private baseService: BaseService, private uikitService: UikitService, private contentService: ContentService,
              private http: HttpClient) {
    this.settingService.showHeader = false;
    this.settingService.showFooter = false;
    this.isSvodEnabled = this.settingService.isSvodEnabled;
    this.resetScreen();
    this.baseService.loaderSource.next(true);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.contentType = params.content_type;
      this.videoId = params.content_id;
      this.response = {
        d_type: 'web',
        content_type: this.contentType,
        video_id: this.videoId
      };
      this.getPlayer();
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resetScreen();
  }

  getPlayer() {
    this.playerService.getPlayerVideo(this.response).subscribe({
      next: data => {
        this.baseService.loaderSource.next(false);
        this.playerData = data;
      }, complete: () => {
        if (this.playerData.status_code === 200) {
          this.video = this.playerData.data;
          this.playerId = 'tv2zPlayer' + Math.floor(Math.random() * 1000000000);
          this.baseService.guest.subscribe(data => this.guest = data);
          if (this.guest) {
            if (this.video.availability !== 3) {
              this.back = true;
              document.getElementById('signinButton').click();
            } else {
              this.playMainVideo();
            }
          } else {
            if (this.video.availability === 1) {
              this.router.navigate(['/subscriptions']);
            } else if (this.video.availability === 2) {
              if (this.video.user_type === 1 || this.isSvodEnabled === 0) {
                this.router.navigate(['/ppv-invoice', this.videoId, this.contentType]);
              } else {
                this.router.navigate(['/pay_per_view', this.videoId, this.contentType]);
              }
            } else {
              this.playMainVideo();
            }
          }
        } else {
          this.uikitService.staticErrorMessage(this.playerData.error.message);
        }
        if (this.playerData.error && this.playerData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.playerData.error.message);
        }
      }
    });
  }

  resetScreen() {
    switch (true) {
      case this.windowWidth > 1800:
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 1450 && this.windowWidth < 1801):
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 991 && this.windowWidth < 1451):
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.height = 500;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth < 480):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      default:
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
    }
  }

  playMainVideo = function() {
    if (this.baseService.guest && this.video.availability !== 3) {
      const iframe: HTMLIFrameElement = document.createElement('iframe');
      // iframe.frameBorder = '0';
      iframe.width = this.width;
      iframe.height = this.height;
      document.getElementById('channel-video-player').appendChild(iframe);
    } else {
      if (this.video.playback_type === 2) {
        const t = /v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(this.video.playback_url);
        const youtubeId = t ? t.slice(1).join('').replace('?', '') : '';
        const ytembed = 'https://www.youtube.com/embed/';
        let completeId = ytembed + youtubeId + '?enablejsapi=1&amp;rel=0&amp;showinfo=0&amp;autoplay=1';
        if (this.video.seek !== '') {
          completeId = completeId + '&amp;start=' + this.video.seek;
        }
        const iframe = document.createElement('iframe');
        // iframe.frameBorder = '0';
        iframe.width = '100%';
        iframe.height = '100%';
        iframe.allowFullscreen = true;
        iframe.id = 'randomid';
        iframe.setAttribute('src', completeId);
        document.getElementById('channel-video-player').appendChild(iframe);
        this.videoStartTime = new Date();
        this.video_width = this.width;
        this.playclicked = true;
        // if (window._paq) {
        //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //   _paq.push(['MediaAnalytics::enableTrackEvents']);
        //   _paq.push(['MediaAnalytics::enableTrackProgress']);
        //   _paq.push(['MediaAnalytics::scanForMedia']);
        // }
      } else if (this.video.playback_type === 4) {
        const url = 'https://vimeo.com/api/oembed.json?url=' + this.video.playback_url;
        const params = new HttpParams().set('url', this.video.playback_url);
        this.http.get(url, { params }).subscribe(
          (response) => {
            if (response.video_id) {
              let videoid = 'https://player.vimeo.com/video/' + response.video_id + '?color=ff9933&title=0&byline=0&portrait=0&autoplay=1';
              if (this.video.seek !== '') {
                const hours = Math.floor(this.video.seek / 60 / 60);
                const minutes = Math.floor(this.video.seek / 60) % 60;
                const seconds = Math.floor(this.video.seek - minutes * 60);
                let seekformat = hours > 0 ? hours + 'h' : '';
                seekformat = minutes > 0 ? seekformat + minutes + 'm' : '';
                seekformat = seconds > 0 ? seekformat + seconds + 's' : '';
                videoid = videoid + '#t=' + seekformat;
              }
              const iframe = document.createElement('iframe');
              // iframe.frameBorder = '0';
              iframe.width = '100%';
              iframe.height = '100%';
              iframe.id = 'randomid';
              iframe.setAttribute('allowfullscreen', 'true');
              iframe.setAttribute('webkitallowfullscreen', 'true');
              iframe.setAttribute('mozallowfullscreen', 'true');
              iframe.setAttribute('src', videoid);
              document.getElementById('channel-video-player').appendChild(iframe);
              this.videoStartTime = new Date();
              // if (window._paq) {
              //   _paq.push(['MediaAnalytics::enableMediaAnalytics']);
              //   _paq.push(['MediaAnalytics::enableTrackEvents']);
              //   _paq.push(['MediaAnalytics::enableTrackProgress']);
              //   _paq.push(['MediaAnalytics::scanForMedia']);
              // }
            } else {
              console.log('Given vimeo video id is not working');
            }
          }
        );
        this.playclicked = true;
        this.video_width = this.width;
      } else {
        this.holaPlayer = true;
        const videotag = document.createElement('video');
        videotag.controls = true;
        if (this.video.playback_url.match(/.mpd/g)) {
          this.type = 'application/dash+xml';
        } else if (this.video.playback_url.match(/.m3u8/g)) {
          this.type = 'application/x-mpegURL';
        } else {
          this.type = 'video/mp4';
        }
        videotag.setAttribute('title', this.video.title);
        videotag.setAttribute('id', this.playerId);
        videotag.setAttribute('height', this.height);
        videotag.setAttribute('width', this.holaWidth);
        videotag.setAttribute('class', 'video-js vjs-default-skin');
        document.getElementById('channel-video-player').appendChild(videotag);
        this.videoStartTime = new Date();

        // New Player integration related changes
        const res = this.video.playback_url.match(/DVR/ig);
        if (!res) {
          var options: any = { id: this.playerId, src: this.video.playback_url, type: this.type };
        } else {
          options = Object.assign(options, { dvr: true });
        }
        if (this.video.is_drm === 1) {
          if (this.playready) {
            options = Object.assign(options, { keySystems: { 'com.microsoft.playready': this.playready } });
          }
          if (this.widevine) {
            options = Object.assign(options, { keySystems: { 'com.widevine.alpha': this.widevine } });
          }
        }
        if (this.video.ad_tag) {
          options.adTagUrl = this.video.ad_tag;
        }
        this.playclicked = false;
        tv2zPlayer(options);
        const singlePlayer = videojs(this.playerId);
        if (this.video.seek !== '') {
          singlePlayer.currentTime(this.video.seek);
        }
        singlePlayer.on('play', function() {
          this.playclicked = true;
        });
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
          event: 'GAEvent',
          eventCategory: 'Video',
          eventAction: 'Start',
          eventLabel: this.video.content_title,
          loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
          userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
          content: this.video.content_title,
          VideoCategory: '',
          audio: '',
          resolution: '',
          subtitle: '',
          notificationStatus: 'False',
          country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
          plateform: 'Web'
        });
        console.log(window.dataLayer);
        // if (window._paq) {
        //   singlePlayer.ready(function () {
        //     _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //     _paq.push(['MediaAnalytics::enableTrackEvents']);
        //     _paq.push(['MediaAnalytics::enableTrackProgress']);
        //     _paq.push(['MediaAnalytics::scanForMedia']);
        //   });
        // }
        this.video_width = this.holaWidth;
        document.getElementsByClassName(this.playerId + '-dimensions')[0]['style']['width'] = '100%';
        document.getElementsByClassName(this.playerId + '-dimensions')[0]['style']['height'] = '100vh';
      }
    }
  };


  closeVideo() {
    this.playerActive = false;
    this.endTime = new Date();
    const durationInSeconds = 0;
    if (this.video.duration_sec !== undefined) {
      this.durationInSeconds = this.video.duration_sec;
    }
    if (this.videoStartTime) {
      let watchedTime = Math.ceil((this.endTime.getTime() - this.videoStartTime.getTime()) / 1000);
      if (60 <= watchedTime && this.contentType !== 'trailer') {
        this.contentService.watchCountApi(this.videoId, this.contentType);
      }
      if (!this.baseService.guest) {
        if (this.video.seek !== '') {
          watchedTime = watchedTime + this.video.seek;
        }
        if (durationInSeconds <= watchedTime) {
          this.contentService.userHistoryApi(this.videoId);
        } else {
          this.contentService.userContinueWatchingApi(this.videoId, watchedTime);
        }
      }
    }
    const seriesPlayer = document.getElementById(this.playerId);
    if (seriesPlayer) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(seriesPlayer).dispose();
    }
    this.location.back();
    // document.getElementById("channel-video-player").innerHTML = "";
    // $('.overlay').css('overflow-y', 'hidden');
    // $('.overlay').css('display', 'none');
    // $('body').css('overflow-y', 'auto');
    // $window.history.back();
  }

  signIn() {
    document.getElementById('signinclose').click();
    this.loggerService.updatedMemoryStorage({});
    this.router.navigateByUrl('/signin');
  }

  go_back() {
    this.location.back();
  }

  ngOnDestroy() {
    this.settingService.showHeader = true;
    this.settingService.showFooter = true;
    this.playerActive = false;
    const oldPlayer = document.getElementById(this.playerId);
    if (oldPlayer) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(oldPlayer).dispose();
    }
  }

}
