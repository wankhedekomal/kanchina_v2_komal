import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LiveRoutingModule } from './live-routing.module';

import { LiveVideoComponent } from './live-video/live-video.component';
import { PlayerComponent } from './player/player.component';
import { ChannelDataComponent } from './channel-data/channel-data.component';
import { ChannelsComponent } from './channels/channels.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { SlickCarouselModule } from 'ngx-slick-carousel';



@NgModule({
  declarations: [ChannelsComponent, ChannelDataComponent, PlayerComponent, LiveVideoComponent],
  imports: [
    CommonModule,
    InfiniteScrollModule,
    SlickCarouselModule,
    SharedModule,
    LiveRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
        deps: [HttpClient]
      }
    })
  ]
})
export class LiveModule { }
