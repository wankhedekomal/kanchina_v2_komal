import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChannelDataComponent } from './channel-data/channel-data.component';
import { ChannelsComponent } from './channels/channels.component';
import { LiveVideoComponent } from './live-video/live-video.component';
import { PlayerComponent } from './player/player.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'live-video/:vedio_name/:content_id',
        component: LiveVideoComponent
      },
      {
        path: 'player/:content_type/:content_id',
        component: PlayerComponent
      },
      {
        path: 'channel-program/:key/:channel_id',
        component: ChannelDataComponent
      },
      {
        path: 'channels',
        component: ChannelsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LiveRoutingModule { }
