import { Component, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { ChannelDataService } from 'src/app/core/services/channel-data.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import * as $ from 'jquery';
import { merge } from 'rxjs';
import { LoggerService } from 'src/app/core/authentication/logger.service';


@Component({
  selector: 'app-channel-data',
  templateUrl: './channel-data.component.html',
  styleUrls: ['./channel-data.component.scss']
})
export class ChannelDataComponent implements OnInit {

  data: any = [];
  value;
  section;
  windowWidth;
  seriesWidth;
  videosWidth;
  contentIndex;
  orientation;
  contentType;
  executed;
  dataLength;
  channelDataLength;
  params;
  channelData;
  channelId;
  imagePath;
  homeImage;
  type;


  constructor(public settingsService: SettingsService, private baseService: BaseService,
              private router: Router, private chaneelService: ChannelDataService, private uikitService: UikitService,
              private route: ActivatedRoute, private loggerService: LoggerService, private storageService: StorageService) {
    this.imagePath = this.settingsService.imagePath;
    this.homeImage = this.settingsService.homeImage;
    this.baseService.loaderSource.next(true);
  }

  ngOnInit(): void {
    this.windowWidth = window.innerWidth - 40;
    this.seriesWidth = (Math.floor(this.windowWidth / 255) * 255);
    this.videosWidth = (Math.floor(this.windowWidth / 334) * 334);
    this.route.params.subscribe(params => {
      this.channelId = params.channel_id;
      this.type = params.key;
    });
    this.detailsFn(0, 16, '');
  }

  loadMoreDetails() {
    if (this.data.length > 0) {
      this.dataLength = this.data[this.contentIndex].list.length;
    } else {
      this.dataLength = 0;
    }
    if (this.dataLength > 0 && this.executed === 1) {
      this.executed = 0;
      this.detailsFn(this.dataLength, 16, this.contentType);
    }
  }

  detailsFn(skip, take, contentType) {
    if (contentType !== '') {
      this.params = {
        region: this.storageService.getLocalStore('region'),
        d_type: 'web',
        type: this.contentType,
        skip,
        take,
        id: this.channelId
      };
    } else {
      this.params = {
        region: this.storageService.getLocalStore('region'),
        d_type: 'web',
        skip,
        take,
        id: this.channelId
      };
    }
    this.chaneelService.getChanneldata(this.params).subscribe({
      next: data => {
        this.baseService.loaderSource.next(false);
        this.channelData = data;
      }, complete: () => {
        if (this.channelData.status_code === 200) {
          if (this.data.length === 0) {
            this.data = this.channelData.data;
            this.channelDataLength = this.channelData.data.length;
            this.executed = 1;
            this.displayContent(this.type);
          } else {
            if (this.data[this.contentIndex].key === contentType && this.channelData.data[0].list.length > 0) {
              this.executed = 1;
              this.data[this.contentIndex].list = merge(this.data[this.contentIndex].list, this.channelData.data[0].list);
            } else {
              this.executed = 0;
            }
          }
        } else {
          this.uikitService.staticErrorMessage(this.channelData.error.message);
        }
        if (this.channelData.error && this.channelData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.channelData.error.message);
        }
      }
    });
  }


  displayContent(id) {
    for (let i = 0; i < this.data.length; i++) {
      $(this.data[i].key).hide();
      if (this.data[i].key === id) {
        this.contentIndex = i;
        this.orientation = this.data[i].orientation;
      }
    }
    $(id).show();
    this.contentType = id;
    this.executed = 1;
  }

  loadMore() {
    this.data = this.channelData;
  }

  redirect(video) {
    const vedioName = this.settingsService.slugify(video.title);
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
      event: 'GAEvent',
      eventCategory: 'Content Click',
      eventAction: 'Live Channels',
      eventLabel: video.title,
      loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
      userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
      content: video.title,
      VideoCategory: 'Live Channels',
      notificationStatus: 'False',
      country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
      plateform: 'Web'
    });
    console.log(window.dataLayer);
    if (video.is_series) {
      this.router.navigate(['/series', vedioName, video.content_id]);
    } else {
      this.router.navigate(['/player', 'video', video.content_id]);
    }
  }

}
