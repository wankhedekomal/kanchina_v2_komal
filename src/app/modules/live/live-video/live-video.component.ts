import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { LiveVideoService } from 'src/app/core/services/live-video.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { UikitService } from 'src/app/core/services/uikit.service';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { Location } from '@angular/common';
import { last } from 'rxjs/operators';
declare function tv2zPlayer(options: object): any;
declare function videojs(player: any): any;

@Component({
  selector: 'app-live-video',
  templateUrl: './live-video.component.html',
  styleUrls: [
    './live-video.component.scss'
  ]
})
export class LiveVideoComponent implements OnInit, OnDestroy {

  seriesConfig: { centerMode: boolean; infinite: boolean; slidesToShow: number; variableWidth: boolean; slidesToScroll: number; };
  imagePath;
  memoryStorage;
  channel: any;
  data;
  value;
  video;
  windowWidth: number;
  slideToShow: number;
  slideToScroll: number;
  slideToShowSeries: number;
  barwidth: number;
  slideToScrollSeries: number;
  increment = 6;
  response;
  videoId;
  liveData;
  related;
  similar;
  videoType;
  isSvodEnabled;
  channelDetails;
  livePlayerActive: boolean;
  calledLivePlayer: number;
  livePlayerId;
  back = false;
  channelId;
  height: any;
  width: any;
  holaWidth: any;
  isYoutubeVideo: any;
  startTime: any;
  type: any;
  endTime;
  similarLength: any;

  constructor(public settingsService: SettingsService, private livevideoService: LiveVideoService,
              private route: ActivatedRoute, private uikitService: UikitService, private baseService: BaseService,
              private router: Router, private loggerService: LoggerService, private location: Location,
              private storageService: StorageService) {
    this.imagePath = this.settingsService.imagePath;
    this.isSvodEnabled = this.settingsService.isSvodEnabled;
    this.getVideoCard();
    this.seriesConfig = {
      centerMode: false, infinite: false, slidesToShow: (this.slideToShowSeries - 1),
      variableWidth: true, slidesToScroll: (this.slideToScrollSeries - 1)
    };
    this.loggerService.memoryStorage.subscribe((data) => this.memoryStorage = data);
    this.resetScreen();
    window.dataLayer = window.dataLayer || [];
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.videoId = params.content_id;
      this.response = {
        u_id: this.storageService.getLocalStore('u_id'),
        d_type: 'web',
        video_id: this.videoId
      };
      this.liveVideos();
    });
  }

  liveVideos() {
    this.baseService.loaderSource.next(true);
    // this.settingsService.getSeo();
    this.livevideoService.getLiveVideo(this.response).subscribe({
      next: data => {
        this.baseService.loaderSource.next(false);
        this.liveData = data;
      }, complete: () => {
        if (this.liveData.status_code === 200) {
          this.channel = this.liveData.data.livechannel;
          this.related = this.liveData.data.related;
          this.similar = this.liveData.data.similar;
          this.similarLength = this.liveData.data.similar.length;
          this.channelId = this.channel.channel_id;
          // if ($scope.channel.chat_admin) {
          //     $scope.chat.admin = $scope.channel.chat_admin;
          // }
          //   angular.forEach($scope.similar, function(value, key) {
          //     var slug = slugify(value.title);
          //     $scope.similar[key]['slug'] = slug;
          // })
          for (var index in this.similar) {
            var slug = this.settingsService.slugify(this.similar[index].title);
            this.similar[index]['slug'] = slug;
          }
          // if ($rootScope.guest && $scope.channel.availability != 3) {
          //     // $scope.showSignIn('paid');
          // } else if ($scope.channel.availability == 1) {
          //     $state.go('profile.subscriptions', { region: this.storageService.getLocalStore('region') });
          // } else if ($scope.channel.availability == 2) {
          //     $state.go('profile.pay_per_view', { id: $scope.channel.channel_id, type: 'livechannel', 
          // region: this.storageService.getLocalStore('region') });
          // }
        } else {
          this.uikitService.staticErrorMessage(this.liveData.error.message);
        }
        if (this.liveData.error && this.liveData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.liveData.error.message);
        }
      }
    });
  }

  getVideoCard() {
    this.windowWidth = window.innerWidth;
    switch (true) {
      case this.windowWidth > 1800:
        this.slideToShow = 6;
        this.slideToScroll = 5;
        this.slideToShowSeries = 7;
        this.slideToScrollSeries = 7;
        break;
      case (this.windowWidth > 1365 && this.windowWidth < 1801):
        this.slideToShow = 5;
        this.slideToScroll = 4;
        this.slideToShowSeries = 6;
        this.slideToScrollSeries = 6;
        break;

      case (this.windowWidth > 1023 && this.windowWidth < 1366):
        this.slideToShow = 4;
        this.slideToScroll = 3;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 4;
        break;
      case (this.windowWidth > 992 && this.windowWidth < 1024):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 3;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 4;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.slideToShow = 2;
        this.slideToScroll = 1;
        this.slideToShowSeries = 3.5;
        this.slideToScrollSeries = 3;
        break;
      case (this.windowWidth < 480):
        this.slideToShow = 1.5;
        this.slideToScroll = 2;
        this.slideToShowSeries = 2.5;
        this.slideToScrollSeries = 3;
        break;
      case (this.windowWidth < 360):
        this.slideToShow = 1;
        this.slideToScroll = 1;
        this.slideToShowSeries = 1.8;
        this.slideToScrollSeries = 2;
        break;
      default:
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 3;
        break;
    }

  }

  showSignIn(value) {
    if (value === 'paid') {
      this.back = true;
    }
    document.getElementById('signinButton').click();
  }

  playVideo(contentId) {
    if (this.baseService.guest) {
      if (this.channel.availability !== 3) {
        document.getElementById('signinButton').click();
      } else {
        this.channelDetails = this.channel;
        this.livePlayerActive = true;
        this.calledLivePlayer = 1;
        // $('.overlay').css('display', 'block');
        // $('body').css('overflow-y', 'hidden');
        // $('body').css('height', '100%');
        // $rootScope.$emit("CallLivePlayer");
        this.CallLivePlayer();
      }
    } else {
      if (this.channel.availability === 1) {
        this.router.navigate(['/subscriptions']);
      } else if (this.channel.availability === 2) {
        this.videoType = 'live';
        if (this.channel.user_type === 1 || this.isSvodEnabled === 0) {
          this.router.navigate(['/ppv-invoice', this.channel.channel_id, this.videoType]);
        } else {
          this.router.navigate(['/pay_per_view', this.channel.channel_id, this.videoType]);
        }
      } else {
        this.channelDetails = this.channel;
        this.livePlayerActive = true;
        this.calledLivePlayer = 1;
        // $('.overlay').css('display', 'block');
        // $('body').css('overflow-y', 'hidden');
        // $('body').css('height', '100%');
        // $rootScope.$emit("CallLivePlayer");
        this.CallLivePlayer();
      }
    }
  }

  CallLivePlayer() {
    if (this.calledLivePlayer === 1) {
      this.calledLivePlayer = 0;
      this.livePlayerId = 'tv2zPlayer' + Math.floor(Math.random() * 1000000000);
      // document.getElementById("live-video-player").innerHTML = "";
      this.channel = this.channelDetails;
      this.playMainVideo(this.channel.playback_type);
    }
  }

  go_back() {
    this.location.back();
  }

  signIn() {
    document.getElementById('signinclose').click();
    this.loggerService.updatedMemoryStorage({});
    this.router.navigateByUrl('/signin');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.getVideoCard();
    this.resetScreen();
  }

  resetScreen() {
    switch (true) {
      case this.windowWidth > 1800:
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 1450 && this.windowWidth < 1801):
        this.height = 600;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 991 && this.windowWidth < 1451):
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.height = 500;
        this.width = this.windowWidth * 0.75;
        this.holaWidth = this.windowWidth * 0.75;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      case (this.windowWidth < 480):
        this.height = 350;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
      default:
        this.height = 450;
        this.width = '100%';
        this.holaWidth = this.windowWidth * 0.85;
        break;
    }
  }

  playMainVideo(playbackType) {
    if (this.baseService.guest && this.channel.availability !== 3) {
      const iframe: HTMLIFrameElement = document.createElement('iframe');
      iframe.width = this.width;
      iframe.height = this.height;
      document.getElementById('live-video-player').appendChild(iframe);
    } else {
      const youtuberegex = /^(http|\/\/).*(youtube\.com|youtu\.be)\/.+/;
      this.isYoutubeVideo = false;
      this.isYoutubeVideo = youtuberegex.test(this.channel.playback_url);

      if (this.channel.playback_type === 1) {
        const t = /v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(this.channel.playback_url);
        const youtubeId = t ? t.slice(1).join('').replace('?', '') : '';
        const ytembed = 'https://www.youtube.com/embed/';
        const completeId = ytembed + youtubeId + '?enablejsapi=1&rel=0&amp&showinfo=0';
        const iframe: HTMLIFrameElement = document.createElement('iframe');
        iframe.width = '100%';
        iframe.height = this.height;
        iframe.allowFullscreen = true;
        iframe.id = 'randomid';
        iframe.setAttribute('src', completeId);
        document.getElementById('live-video-player').appendChild(iframe);
        this.startTime = new Date();
        // if (window._paq) {
        //     _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //     _paq.push(['MediaAnalytics::enableTrackEvents']);
        //     _paq.push(['MediaAnalytics::enableTrackProgress']);
        //     _paq.push(['MediaAnalytics::scanForMedia']);
        // }
      } else {
        const videotag = document.createElement('video');
        videotag.controls = true;
        if (this.channel.playback_url.match(/.mpd/g)) {
          this.type = 'application/dash+xml';
        } else if (this.channel.playback_url.match(/.m3u8/g)) {
          this.type = 'application/x-mpegURL';
        } else {
          this.type = 'video/mp4';
        }
        videotag.setAttribute('title', this.channel.title);
        videotag.setAttribute('id', this.livePlayerId);
        videotag.setAttribute('class', 'video-js vjs-default-skin');
        videotag.setAttribute('height', this.height);
        videotag.setAttribute('width', this.width);
        document.getElementById('live-video-player').appendChild(videotag);
        this.startTime = new Date();
        // New Player integration changes
        const res = this.channel.playback_url.match(/DVR/ig);
        if (!res) {
          var options: any = { id: this.livePlayerId, src: this.channel.playback_url, type: this.type };
        } else {
          options = Object.assign(options, { dvr: true });
        }
        tv2zPlayer(options);
        window.dataLayer.push({
          event: 'GAEvent',
          eventCategory: 'Video',
          eventAction: 'Start',
          eventLabel: this.channel.title,
          loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
          userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
          content: this.channel.title,
          VideoCategory: '',
          audio: '',
          resolution: '',
          subtitle: '',
          notificationStatus: 'False',
          country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
          plateform: 'Web'
        });
        console.log(window.dataLayer);
        // if (window._paq) {
        //     videojs($rootScope.livePlayerId).ready(function() {
        //         _paq.push(['MediaAnalytics::enableMediaAnalytics']);
        //         _paq.push(['MediaAnalytics::enableTrackEvents']);
        //         _paq.push(['MediaAnalytics::enableTrackProgress']);
        //         _paq.push(['MediaAnalytics::scanForMedia']);
        //     });
        // }
        document.getElementsByClassName(this.livePlayerId + '-dimensions')[0]['style']['width'] = '100%';
        document.getElementsByClassName(this.livePlayerId + '-dimensions')[0]['style']['height'] = '100vh';

        this.settingsService.showHeader = false;
        this.settingsService.showFooter = false;
      }
    }
  }

  closeVideo() {
    this.livePlayerActive = false;
    this.endTime = new Date();
    if (this.startTime) {
      const watchedTime = Math.ceil((this.endTime.getTime() - this.startTime.getTime()) / 1000);
      if (60 <= watchedTime) {
        this.livevideoService.countwatchedTime(this.channel.channel_id, 'livechannel').subscribe({
          next: data => {
            this.liveData = data;
          }, complete: () => {
            if (this.liveData.status_code === 200) {
              //
            } else {
              this.uikitService.staticErrorMessage(this.liveData.error.message);
            }
            if (this.liveData.error && this.liveData.error.code !== 1002) {
              this.uikitService.staticErrorMessage(this.liveData.error.message);
            }
          }
        });
      }
    }
    const seriesPlayer = document.getElementById(this.livePlayerId);
    if (seriesPlayer) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(seriesPlayer).dispose();
    }
    this.settingsService.showHeader = true;
    this.settingsService.showFooter = true;
    // document.getElementById("live-video-player").innerHTML = "";
    // $('.overlay').css('overflow-y', 'hidden');
    // $('.overlay').css('display', 'none');
    // $('body').css('overflow-y', 'auto');
  }

  ngOnDestroy() {
    this.livePlayerActive = false;
    const oldPlayer = document.getElementById(this.livePlayerId);
    if (oldPlayer) {
      if (document['pictureInPictureElement']) {
        document['exitPictureInPicture']();
      }
      videojs(oldPlayer).dispose();
    }
  }

}
