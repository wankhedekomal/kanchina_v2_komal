import { SettingsService } from '../../core/services/settings.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  pageH1Tag;
  homeImage: any;

  constructor(public settingsService: SettingsService) {
    this.homeImage = this.settingsService.homeImage;
   }

  ngOnInit(): void {
  }

}
