import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayPerViewComponent } from './pay-per-view/pay-per-view.component';
import { PpvInvoiceComponent } from './ppv-invoice/ppv-invoice.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/modules/shared.module';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'pay-per-view/:content_id/:video_type',
        component: PayPerViewComponent
      },
      {
        path: 'ppv-invoice/:content_id/:video_type',
        component: PpvInvoiceComponent
      }
    ]
  }
];


@NgModule({
  declarations: [PayPerViewComponent, PpvInvoiceComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
        deps: [HttpClient]
      }
    })
  ]
})
export class PpvModule { }