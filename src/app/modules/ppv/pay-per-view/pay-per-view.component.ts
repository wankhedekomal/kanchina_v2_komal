import { Component, OnInit } from '@angular/core';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { PayPerViewService } from 'src/app/core/services/pay-per-view.service';
import { UikitService } from 'src/app/core/services/uikit.service';

@Component({
  selector: 'app-pay-per-view',
  templateUrl: './pay-per-view.component.html',
  styleUrls: ['./pay-per-view.component.scss']
})
export class PayPerViewComponent implements OnInit {

  ppv: any;
  pay: any;
  imagePath;
  contentId;
  response;
  videoType;
  ppvData;
  payOption;
  ppvCardsLength;
  homeImage;

  constructor(public settingService: SettingsService, private route: ActivatedRoute, private ppvService: PayPerViewService,
              private uikitService: UikitService, private router: Router, private baseService: BaseService,
              private storageService: StorageService) {
    this.imagePath = this.settingService.imagePath;
    this.homeImage = this.settingService.homeImage;
    this.baseService.loaderSource.next(true);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.videoType = params.video_type;
      this.contentId = params.content_id;
      this.response = {
        u_id: this.storageService.getLocalStore('u_id'),
        d_type: 'web',
        content_id: this.contentId,
        video_type: this.videoType
      };
      this.payPerView();
    });
  }

  payPerView() {
    this.ppvService.getpayperView(this.response).subscribe({
      next: data => {
        this.baseService.loaderSource.next(false);
        this.ppvData = data;
      }, complete: () => {
        if (this.ppvData.status_code === 200) {
          this.ppv = this.ppvData.data;
          this.ppvCardsLength = this.ppv.cards.length;
          // if ($scope.ppv.content_desc)
          //     $('#description').append($scope.ppv.content_desc);
        } else {
          this.uikitService.staticErrorMessage(this.ppvData.error.message);
         }
        if (this.ppvData.error && this.ppvData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.ppvData.error.message);
        }
      }
    });
  }

  ppv_pay(redirection) {
    this.payOption = redirection;
    if (this.ppvCardsLength > 1 && !this.payOption) {
      this.uikitService.staticErrorMessage('Please Select Payment option');
      return;
    }
    if (this.payOption === 'subscription') {
      this.router.navigate(['/subscriptions']);
     } else {
      this.router.navigate(['/ppv-invoice', this.ppv.content_id, this.ppv.content_type]);
     }
  }

  select_pay_type(value){
    this.payOption = value;
  }


}
