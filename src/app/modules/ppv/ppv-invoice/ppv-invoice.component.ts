import { Component, OnInit } from '@angular/core';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { PpvInvoiceService } from 'src/app/core/services/ppv-invoice.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LiveVideoService } from 'src/app/core/services/live-video.service';
import { TranslateService } from '@ngx-translate/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { StripeService } from 'src/app/core/services/stripe.service';

@Component({
  selector: 'app-ppv-invoice',
  templateUrl: './ppv-invoice.component.html',
  styleUrls: ['./ppv-invoice.component.scss']
})
export class PpvInvoiceComponent implements OnInit {

  imagePath;
  discountAmount;
  couponMessage;
  coupon;
  video;
  payableAmount;
  videoType;
  contentId;
  response;
  type;
  invoiceData;
  couponSelected;
  couponApplied;
  PromocodeForm: FormGroup;
  promoCode;
  payable;
  coupen;
  coupenRes;
  coupondata;
  userConfirm;
  liveData;
  related;
  similar;
  params;
  checkoutRes;
  proccedData;
  typeOfPayment;
  deviceType;
  paymentOptions;
  payPalData;
  homeImage: any;

  constructor(public settingService: SettingsService, private route: ActivatedRoute, private ppvInvoiceService: PpvInvoiceService,
              private uikitService: UikitService, private fb: FormBuilder, private livevideoService: LiveVideoService,
              private router: Router, private translate: TranslateService, private stripeService: StripeService,
              private baseService: BaseService, private storageService: StorageService) {
    this.imagePath = this.settingService.imagePath;
    this.homeImage = this.settingService.homeImage;
    this.baseService.loaderSource.next(true);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.videoType = params.video_type;
      this.contentId = params.content_id;
      this.response = {
        u_id: this.storageService.getLocalStore('u_id'),
        d_type: 'web',
        content_id: this.contentId,
        video_type: this.videoType
      };
      this.ppvInvoice();
    });

    this.PromocodeForm = this.fb.group({
      promoCode: ['', [Validators.required]]
    });
  }

  ppvInvoice() {
    if (this.videoType !== 'live') {
      this.type = this.videoType;
      this.ppvInvoiceService.getInvoice(this.response).subscribe({
        next: data => {
          this.baseService.loaderSource.next(false);
          this.invoiceData = data;
        }, complete: () => {
          if (this.invoiceData.status_code === 200) {
            this.video = this.invoiceData.data.content;
            // if (this.video.description)
            // $('#description').append(this.video.description);
          } else {
            this.uikitService.staticErrorMessage(this.invoiceData.error.message);
          }
        }
      });
    } else {
      this.type = 'livechannel';
      this.livevideoService.getLiveVideo(this.response).subscribe({
        next: data => {
          this.baseService.loaderSource.next(false);
          this.liveData = data;
        }, complete: () => {
          if (this.liveData.status_code === 200) {
            this.video = this.liveData.data.livechannel;
            if (this.video.user_type === 1) {
              this.video.amount = this.video.tvod_sub_amt;
            } else {
              this.video.amount = this.video.tvod_nor_amt;
            }
            this.related = this.liveData.data.related;
            this.similar = this.liveData.data.similar;
            // if (this.channel.chat_admin) {
            //   this.chat.admin = this.liveData.data.chat_as_admin;
            // }
            // if ($rootScope.guest && $scope.channel.availability != 3) {
            //     // $scope.showSignIn('paid');
            // } else if ($scope.channel.availability == 1) {
            //     $state.go('profile.subscriptions', { region: this.storageService.getLocalStore('region') });
            // } else if ($scope.channel.availability == 2) {
            //     $state.go('profile.pay_per_view', { id: $scope.channel.channel_id, type: 'livechannel',
            // region: this.storageService.getLocalStore('region') });
            // }
          } else {
            this.uikitService.staticErrorMessage(this.liveData.error.message);
          }
          if (this.liveData.error && this.liveData.error.code !== 1002) {
            this.uikitService.staticErrorMessage(this.liveData.error.message);
          }
        }
      });
    }
    this.userConfirm = 0;
    this.couponSelected = false;
    this.couponApplied = false;
  }

  showCoupon(value) {
    if (value === 'apply') {
      if (this.couponApplied) {
        this.couponSelected = false;
      } else {
        this.couponSelected = true;
      }
    } else if (value === 'remove') {
      this.couponSelected = false;
      this.couponApplied = false;
      this.coupon = '';
      this.payable = '1';
    }
  }

  applyCoupon(coupon) {
    this.response = {
      content_id: this.contentId,
      content_type: this.videoType,
      coupon_code: coupon,
      u_id: this.storageService.getLocalStore('u_id'),
      d_type: 'web'
    };
    this.ppvInvoiceService.applyCoupen(this.response).subscribe({
      next: data => {
        this.coupenRes = data;
      }, complete: () => {
        if (this.coupenRes.status_code === 200) {
          this.coupon = coupon;
          this.couponSelected = false;
          this.coupondata = this.coupenRes.data.coupon;
          this.couponMessage = this.coupenRes.data.message;
          this.couponApplied = true;
          this.discountAmount = this.coupenRes.data.discount_amount;
          this.payableAmount = this.coupenRes.data.payable_amount;
          this.payable = this.coupenRes.data.payable;
        } else {
          this.uikitService.staticErrorMessage(this.coupenRes.error.message);
        }
        if (this.coupenRes.error && this.coupenRes.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.coupenRes.error.message);
        }
      }
    });
  }

  checkout() {
    // $('#pay_now_subscription').attr('disabled', 'true');
    if (this.coupon !== '') {
      this.params = {
        content_type: this.type, content_id: this.contentId, d_type: 'web', u_id: this.storageService.getLocalStore('u_id'),
         coupon_code: this.coupon };
    } else {
      this.params = { content_type: this.type, content_id: this.contentId, d_type: 'web', u_id: this.storageService.getLocalStore('u_id') };
    }
    this.ppvInvoiceService.checkout(this.params).subscribe({
      next: data => {
        this.checkoutRes = data;
      }, complete: () => {
        // $('#pay_now_subscription').attr('disabled', 'false');
        if (this.checkoutRes.status_code === 200) {
          this.stripeService.checkout(this.checkoutRes.data.session.id);
        } else {
          this.uikitService.staticErrorMessage(this.checkoutRes.error.message);
          if (this.checkoutRes.error.code === 2192) {
            this.router.navigate(['/subscriptions']);
            // $state.reload('profile.subscriptions');
          }
        }
        // $('#pay_now_subscription').attr('disabled', 'false');
        if (this.checkoutRes.error && this.checkoutRes.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.checkoutRes.error.message);
        }
      }
    });
  }

  proceed() {
    // $('#pay_now_subscription').attr('disabled', 'true');
    if (this.coupon !== '') {
      this.params = {
        content_type: this.type, content_id: this.contentId, d_type: 'web', u_id: this.storageService.getLocalStore('u_id'),
        coupon_code: this.coupon };
    } else {
      this.params = { content_type: this.type, content_id: this.contentId, d_type: 'web', u_id: this.storageService.getLocalStore('u_id') };
    }
    this.ppvInvoiceService.ppvProceed(this.params).subscribe({
      next: data => {
        this.proccedData = data;
      }, complete: () => {
        // $('#pay_now_subscription').attr('disabled', 'false');
        const vedioName = this.settingService.slugify(this.video.title);
        if (this.proccedData.status_code === 200) {
          if (this.type === 'video') {
            this.router.navigate(['/video', vedioName, this.contentId]);
          } else {
            this.router.navigate(['/series', vedioName, this.contentId]);
          }
        } else {
          this.uikitService.staticErrorMessage(this.proccedData.error.message);
          if (this.proccedData.error.code === 2192) {
            this.router.navigate(['/subscriptions']);
            // $state.reload('profile.subscriptions');
          }
        }
        // $('#pay_now_subscription').attr('disabled', 'false');
        if (this.proccedData.error && this.proccedData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.proccedData.error.message);
        }
      }
    });
  }

  sendToPaypal() {
    let paymentMode;
    this.translate.get('pay_summ_desc_mode_def').subscribe(data => {
      paymentMode = data;
    });
    if (!this.typeOfPayment) {
      this.uikitService.staticErrorMessage(paymentMode);
      // UIkit.notify({ message: $filter('translate')('pay_summ_desc_mode_def'), timeout: 3000, pos: 'top-center', status: 'danger' });
      return;
    }
    if (!this.userConfirm) {
      this.uikitService.staticErrorMessage('Please Accept terms and conditions.');
      // UIkit.notify({ message: $filter('translate')('Please Accept terms and conditions.'),
      // timeout: 3000, pos: 'top-center', status: 'danger' });
      return;
    }
    const isMobile = {
      iOS() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      }
    };
    if (isMobile.iOS()) {
      this.deviceType = 'ios';
    } else {
      this.deviceType = 'web';
    }
    if (this.typeOfPayment === 2) {
      this.paymentOptions = 'visa';
    } else if (this.typeOfPayment === 4) {
      this.paymentOptions = 'ideal';
    }
    this.response = {
      paymentOptions: this.paymentOptions,
      d_type: this.deviceType,
      u_id: this.storageService.getLocalStore('u_id'),
      content_id: this.contentId,
      content_type: this.videoType
    };
    this.ppvInvoiceService.ppvPayPal(this.response).subscribe({
      next: data => {
        this.payPalData = data;
      }, complete: () => {
        if (this.payPalData.status_code === 200) {
          window.location.href = this.payPalData.data.redirect_url;
        } else {
          this.uikitService.staticErrorMessage(this.payPalData.error.message);
        }
        if (this.payPalData.error && this.payPalData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.payPalData.error.message);
        }
      }
    });

  }

}
