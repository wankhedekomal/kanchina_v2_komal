import { Component, OnInit } from '@angular/core';
import { IosRedirectionService } from 'src/app/core/services/ios-redirection.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-ios-redirection',
  templateUrl: './ios-redirection.component.html',
  styleUrls: ['./ios-redirection.component.scss']
})
export class IosRedirectionComponent implements OnInit {

  response;
  iosData;
  siteLogo;
  userDetails;
  uId;
  dId;
  gId;
  account;
  type;

  constructor(private iosService: IosRedirectionService, private uikitService: UikitService,
              private loggerService: LoggerService, private router: Router, private settingService: SettingsService,
              private route: ActivatedRoute, private baseService: BaseService, private storageService: StorageService) {
      this.baseService.loaderSource.next(true);
    }

  ngOnInit(): void {
    this.redirectToIOS();
  }

  redirectToIOS() {
    this.siteLogo = this.settingService.settings.web_logo;
    this.userDetails = this.route.queryParams.subscribe(params => {
        this.uId = params.u_id;
        this.dId = params.d_id;
        this.gId = params.g_id;
        this.account = params.account;
        this.type = params.type;
    });
    this.response = {
      u_id: this.storageService.getLocalStore('u_id'),
      d_id: this.storageService.getLocalStore('d_id'),
      g_id: this.storageService.getLocalStore('g_id')
    }
    this.iosService.iosRedirect(this.response).subscribe({
      next: data => {
      this.baseService.loaderSource.next(false);
      this.iosData = data;
      }, complete: () => {
        if (this.iosData.status_code === 200) {
          const memeoryUpdateData = {};
          localStorage.setItem('a_t', this.iosData.data.access_token);
          localStorage.setItem('r_t', this.iosData.data.refresh_token);
          localStorage.setItem('u_id', this.iosData.data.user_id);
          localStorage.setItem('logged_in', '1');
          localStorage.setItem('g_id', this.userDetails.g_id);
          localStorage.setItem('d_id', this.userDetails.d_id);
          if (this.iosData.data){
            memeoryUpdateData['login_by'] = this.iosData.data.user_login_by;
            memeoryUpdateData['user_picture'] = this.iosData.data.user_picture;
            memeoryUpdateData['user_name'] = this.iosData.data.user_name;
            memeoryUpdateData['user_type'] = this.iosData.data.user_type;
            memeoryUpdateData['one_time_subscription'] = this.iosData.data.one_time_subscription;
            memeoryUpdateData['u_id'] = this.iosData.data.user_id;
          }
          this.loggerService.updatedMemoryStorage(memeoryUpdateData);
          localStorage.setItem('sessionStorage', JSON.stringify(this.loggerService.memoryStorage));
          const accountPage = this.userDetails.account;
          if (accountPage === 1) {
            this.router.navigateByUrl('account-settings');
          } else if (accountPage === 2) {
            this.router.navigateByUrl('subscriptions');
          } else {
            if (this.userDetails.type) {
              // $state.go('profile.pay_per_view', { id: $scope.userDetails.content_id, type: $scope.userDetails.type,
              // region: this.storageService.getLocalStore('region') });
              this.router.navigateByUrl('pay_per_view');
            } else {
              this.router.navigateByUrl('index');
            }
          }
        } else {
          this.uikitService.staticErrorMessage(this.iosData.error.message);
          this.router.navigateByUrl('index');
        }
      }
    });
  }

}
