import { Component, OnInit } from '@angular/core';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MyAccountService } from 'src/app/core/services/my-account.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { TranslateService } from '@ngx-translate/core';
import { EditAccountService } from 'src/app/core/services/edit-account.service';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.scss']
})
export class EditAccountComponent implements OnInit {

  uId: string;
  editAccountForm: FormGroup;
  myAccountData;
  userName;
  userEmail;
  userMobile;
  userPicture;
  userLoginBy;
  imagePath;
  selectedFile;
  updatedData;
  dType = 'web';
  form = new FormData;
  homeImage: any;

  constructor(public settingsService: SettingsService, private accountService: MyAccountService,
              private formBuilder: FormBuilder, private uikitService: UikitService,
              private translate: TranslateService, private editService: EditAccountService,
              private loggerService: LoggerService, private router: Router, private baseService: BaseService,
              private storageService: StorageService) {
      this.homeImage = this.settingsService.homeImage;
      this.baseService.loaderSource.next(true);
    }

  ngOnInit(): void {
    this.imagePath = this.settingsService.imagePath;
    this.editAccountForm = this.formBuilder.group({
      user_name: [this.userName],
      user_email: [this.userEmail],
      user_mobile: [this.userMobile],
      user_picture: [this.userPicture]
    });
    this.getAccount();
    this.uId = this.storageService.getLocalStore('u_id');
  }

  getAccount() {
    this.accountService.getAccountDetails().subscribe({
      next: res => {
        this.baseService.loaderSource.next(false);
        this.myAccountData = res;
      }, complete: () => {
        if (this.myAccountData['status_code'] = 200) {
          this.userName = this.myAccountData.user_name;
          this.userEmail = this.myAccountData.user_email;
          this.userMobile = this.myAccountData.user_mobile;
          this.userPicture = this.settingsService.imagePath + this.myAccountData.user_picture;
          this.userLoginBy = this.myAccountData.user_login_by;
          if (this.userPicture.indexOf('http://') === 0 || this.userPicture.indexOf('https://') === 0) { } else {
            this.userPicture = this.imagePath + this.userPicture;
          }
        } else {
          this.uikitService.notifyError(this.myAccountData.error.message);
        }
      }
    });
  }


  openBrowse(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      this.selectedFile = event.target.files[0];
      this.form.append('picture', event.target.files[0], event.target.files[0].name);
      reader.onload = () => {
        this.userPicture = reader.result;
      };
    }
  }

  editAccount() {
    let nameRequired;
    let nameLength;
    let mobile;
    let mobileInvalid;
    this.translate.get('edit_acc_msg_name_req_def').subscribe(data => {
      nameRequired = data;
    });
    this.translate.get('edit_acc_msg_name_len_def').subscribe(data => {
      nameLength = data;
    });
    this.translate.get('edit_acc_msg_mbl_len_def').subscribe(data => {
      mobile = data;
    });
    this.translate.get('edit_acc_msg_invld_mbl_def').subscribe(data => {
      mobileInvalid = data;
    });
    if (this.userName === '' || this.userName === undefined) {
      this.uikitService.staticErrorMessage(nameRequired);
      return;
    }
    if (this.userName.length < 3) {
      this.uikitService.staticErrorMessage(nameLength);
      return;
    }
    if (this.userMobile === undefined && this.userMobile !== '' && this.userMobile.length < 6) {
      this.uikitService.staticErrorMessage(mobile);
      return;
    }
    if (this.userMobile && this.userMobile !== '') {
      const phoneno = /^[0-9]{6,11}$/;
      if (String(this.userMobile).match(phoneno)) { } else {
        this.uikitService.staticErrorMessage(mobileInvalid);
        return;
      }
    }

    this.form.append('name', this.editAccountForm.get('user_name').value);
    this.form.append('Email', this.editAccountForm.get('user_email').value);
    this.form.append('mobile', this.editAccountForm.get('user_mobile').value);
    this.form.append('login_by', this.userLoginBy);
    this.form.append('u_id', this.uId);
    this.form.append('device_type', this.dType);

    this.editService.editAccountFun(this.form).subscribe({
      next: res => {
        this.updatedData = res;
      }, complete: () => {
        if (this.updatedData.status_code === 200) {
          this.uikitService.staticSuccessMessage(this.updatedData.message);
          const updateMemoryData = {};
          updateMemoryData['login_by'] = this.updatedData.data.user_login_by;
          updateMemoryData['user_picture'] = this.updatedData.data.user_picture;
          updateMemoryData['user_name'] = this.updatedData.data.user_name;
          updateMemoryData['user_type'] = this.updatedData.data.user_type;
          updateMemoryData['one_time_subscription'] = this.updatedData.data.one_time_subscription;
          this.loggerService.updatedMemoryStorage(updateMemoryData);
          this.storageService.setLocalStore('sessionStorage', JSON.stringify(updateMemoryData));
          this.router.navigateByUrl('account-settings');
        } else {
          this.uikitService.staticErrorMessage(this.updatedData.error.message);
        }
        if (this.updatedData.error && this.updatedData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.updatedData.error.message);
        }
      }
    });

  }

}
