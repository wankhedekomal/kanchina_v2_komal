import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLanguageComponent } from './app-language/app-language.component';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DeleteAccountComponent } from './delete-account/delete-account.component';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { UserDevicesComponent } from './user-devices/user-devices.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'account-settings',
        component: MyAccountComponent,
      },
      {
        path: 'authenticate',
        component: AuthenticateComponent,
      },
      {
        path: 'app-language/:type',
        component: AppLanguageComponent,
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
      },
      {
        path: 'delete-account',
        component: DeleteAccountComponent,
      },
      {
        path: 'edit-account',
        component: EditAccountComponent,
      },
      {
        path: 'devices',
        component: UserDevicesComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
