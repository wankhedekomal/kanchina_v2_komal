import { Component, OnInit } from '@angular/core';
import { SettingsService, BaseService, StorageService } from 'src/app/core/services';
import { UserDevicesService } from 'src/app/core/services/user-devices.service';
import { UikitService } from 'src/app/core/services/uikit.service';
import { Router } from '@angular/router';
import { LoggerService } from 'src/app/core/authentication/logger.service';

@Component({
  selector: 'app-user-devices',
  templateUrl: './user-devices.component.html',
  styleUrls: ['./user-devices.component.scss']
})
export class UserDevicesComponent implements OnInit {

  devices;
  response;
  deviceData;
  removedData;
  length;
  url;
  homeImage: any;

  constructor(public settingsService: SettingsService, private userdevicesService: UserDevicesService,
              private uikitService: UikitService, private router: Router, private baseService: BaseService,
              private loggerService: LoggerService, private storageService: StorageService) {
      this.baseService.loaderSource.next(true);
      this.homeImage = this.settingsService.homeImage;
     }

  ngOnInit(): void {
    this.response = {
      d_type: 'web'
    };
    this.userdevicesService.getUserDevice(this.response).subscribe({
      next: data => {
        this.baseService.loaderSource.next(false);
        this.deviceData = data;
      }, complete: () => {
        if (this.deviceData.status_code === 200) {
          this.devices = this.deviceData.data.devices;
          this.length = this.devices.length;
        } else {
          this.uikitService.staticErrorMessage(this.deviceData.error.message);
        }
        if (this.deviceData.error && this.deviceData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.deviceData.error.message);
        }
        return false;
      }
    });
  }

  removeDevice(id, currentDevice) {
    if (id !== '') {
      this.url = 'users/logout/devices/' + id;
    } else {
      this.url = 'users/logout/devices';
    }
    this.userdevicesService.removeUserDevice(this.url).subscribe({
      next: data => {
        this.removedData = data;
      }, complete: () => {
        if (this.removedData.status_code === 200) {
          if (id === '' || currentDevice === 1) {
            this.loggerService.updatedMemoryStorage({});
            this.storageService.removeLocalStore('a_t');
            this.storageService.removeLocalStore('r_t');
            this.storageService.setLocalStore('logged_in', '0');
            this.storageService.removeLocalStore('u_id');
            this.storageService.removeLocalStore('sessionStorage');
            if (this.baseService.loginMode === 1) {
              this.router.navigateByUrl('index');
            } else {
              this.baseService.guestSource.next(true);
              this.router.navigateByUrl('home');
            }
          } else {
            window.location.reload();
          }
        } else {
          this.uikitService.staticErrorMessage(this.removedData.error.message);
        }
        if (this.removedData.error && this.removedData.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.removedData.error.message);
        }
      }
    });
  }

}
