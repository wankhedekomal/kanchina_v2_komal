import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppLanguageComponent } from './app-language/app-language.component';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DeleteAccountComponent } from './delete-account/delete-account.component';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { UserDevicesComponent } from './user-devices/user-devices.component';
import { AccountRoutingModule } from './account-routing.module';

@NgModule({
  declarations: [AppLanguageComponent, AuthenticateComponent, ChangePasswordComponent, DeleteAccountComponent, EditAccountComponent,
    MyAccountComponent, UserDevicesComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
        deps: [HttpClient]
      }
    })
  ]
})
export class AccountModule { }
