import { Component, OnInit } from '@angular/core';
import { SettingsService, StorageService, BaseService } from 'src/app/core/services';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { Router } from '@angular/router';
import { UikitService } from 'src/app/core/services/uikit.service';
import { ChangePasswordService } from 'src/app/core/services/change-password.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  // user input coming from html
  changePasswordForm: any;
  // Taking user input
  response: { old_password: any; password: any; password_confirmation: any; d_type: 'web', u_id: any };
  // response data after password change
  dataPass: any;
  // To show the password
  showPassword = false;
  // used to show confirmationPassword
  showConfirmPassword = false;
  // used to show oldPassword
  showOldPassword = false;
  changePassworData: any;
  guest;
  homeImage;


  constructor(public settingsService: SettingsService, private formBuilder: FormBuilder,
              private changePasswordService: ChangePasswordService, private baseService: BaseService,
              private uikitService: UikitService, private translate: TranslateService, private storageService: StorageService) {
      this.homeImage = this.settingsService.homeImage;
    }

  ngOnInit(): void {
    this.baseService.guest.subscribe(data => this.guest = data);
    this.changePasswordForm = this.formBuilder.group({
      old_password: ['', Validators.required],
      password: ['', Validators.compose([Validators.required])],
      password_confirmation: ['', Validators.compose([Validators.required])]
    });
  }

  changePassword() {
    let passErr;
    let passLength;
    let successMsg;
    this.translate.get('cng_pass_msg_pwd_def').subscribe(data => {
      passErr = data;
    });
    this.translate.get('cng_pass_msg_pwd_len_def').subscribe(data => {
      passLength = data;
    });
    this.translate.get('cng_pass_msg_cont_def').subscribe(data => {
      successMsg = data;
    });

    this.response =
    {
      old_password: this.changePasswordForm.get('old_password').value,
      password: this.changePasswordForm.get('password').value,
      password_confirmation: this.changePasswordForm.get('password_confirmation').value,
      d_type: 'web',
      u_id: this.storageService.getLocalStore('u_id')
    };

    if (this.response.password !== this.response.password_confirmation) {
      this.uikitService.staticErrorMessage(passErr);
      return;
    }
    if (this.response.password < 6) {
      this.uikitService.staticErrorMessage(passLength);
      return;
    }

    this.changePasswordService.changePasswordfun(this.response).subscribe({
      next: data => {
        this.changePassworData = data;
      }, complete: () => {
        if (this.changePassworData.status_code === 200) {
          this.uikitService.staticSuccessMessage(successMsg);
          this.storageService.removeLocalStore('a_t');
          this.storageService.removeLocalStore('r_t');
          this.storageService.removeLocalStore('u_id');
          this.storageService.setLocalStore('logged_in', '0');
          this.storageService.removeLocalStore('sessionStorage');
          const loginMode = this.storageService.getLocalStore('login_mode');
          if (loginMode === '1') {
            this.baseService.redirectTo('index');
          } else {
            this.baseService.guestSource.next(true);
            this.baseService.redirectTo('home');
          }
        } else {
          this.uikitService.staticErrorMessage(this.changePassworData.error.message);
        }
        if (this.changePassworData.error && this.changePassworData.error.code !== 1002) {
            this.uikitService.staticErrorMessage(this.changePassworData.error.message);
          }
        return false;
      }
    });
  }


  hidePassword(type) {
    if (type === 'password') {
      this.showPassword = !this.showPassword;
      const x = document.getElementById('password') as HTMLInputElement;
      if (x.type === 'password') {
        x.type = 'text';
      } else {
        x.type = 'password';
      }
    } else if (type === 'confirm_password') {
      this.showConfirmPassword = !this.showConfirmPassword;
      const x = document.getElementById('confirm_password') as HTMLInputElement;
      if (x.type === 'password') {
        x.type = 'text';
      } else {
        x.type = 'password';
      }
    } else {
      this.showOldPassword = !this.showOldPassword;
      const x = document.getElementById('old_password') as HTMLInputElement;
      if (x.type === 'password') {
        x.type = 'text';
      } else {
        x.type = 'password';
      }
    }
  }

}
