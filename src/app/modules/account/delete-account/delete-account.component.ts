import { Component, OnInit } from '@angular/core';
import { SettingsService, StorageService, BaseService } from 'src/app/core/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UikitService } from 'src/app/core/services/uikit.service';
import { DeleteAccountService } from 'src/app/core/services/delete-account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.scss']
})
export class DeleteAccountComponent implements OnInit {

  // user input if login_by = "manual"
  deleteAccountForm: FormGroup;
  // paramter to be passed into the function
  response: {};
  // data obtained from
  dataPass: any;
  //
  showPassword = false;
  // to take user password
  loginBy: any;
  sessionStorage: any;
  homeImage: any;

  constructor(public settingsService: SettingsService, private formBuilder: FormBuilder,
              private storageservice: StorageService, private uikitService: UikitService,
              private deleteService: DeleteAccountService, private router: Router,
              private baseService: BaseService, private storageService: StorageService) {
      this.homeImage = this.settingsService.homeImage;
    }

  ngOnInit(): void {
    this.deleteAccountForm = this.formBuilder.group({
      password: ['']
    });
    this.loginBy = this.storageservice.getLocalStore('sessionStorage').login_by;
  }

  deleteAccount() {
    if (this.loginBy === 'manual') {
      if (this.deleteAccountForm.get('password').value === '' || this.deleteAccountForm.get('password').value === undefined) {
        this.uikitService.staticErrorMessage('Please fill the password field');
        return false;
      }
      this.response = {
        u_id: this.storageService.getLocalStore('u_id'),
        d_type: 'web',
        password: this.deleteAccountForm.get('password').value
      };
    }else {
        this.response = { u_id: this.storageService.getLocalStore('u_id'), d_type: 'web' };
    }
    this.deleteService.deletefun(this.response).subscribe({
      next: data => {
        this.dataPass = data;
      }, complete: () => {
        if (this.dataPass.status_code = 200) {
          this.uikitService.notifySuccess(this.dataPass);
          this.storageService.removeLocalStore('a_t');
          this.storageService.removeLocalStore('r_t');
          this.storageService.removeLocalStore('u_id');
          this.storageService.setLocalStore('logged_in', JSON.stringify(0));
          this.storageService.removeLocalStore('sessionStorage');
          if (this.loginBy !== 'manual') {
            this.storageService.removeAll();
          }
          if (this.storageservice.getLocalStore('login_mode') === 1) {
            this.router.navigateByUrl('/index');
          } else {
            this.baseService.guestSource.next(true);
            this.router.navigateByUrl('/home');
          }
        } else{
          this.uikitService.staticErrorMessage(this.dataPass.error.message);
        }
        if (this.dataPass.error && this.dataPass.error.code !== 1002) {
          this.uikitService.staticErrorMessage(this.dataPass.error.message);
      }
        return false;
      }
    });
  }




  hidePassword(type) {
    this.showPassword = !this.showPassword;
    const x = document.getElementById('password') as HTMLInputElement;
    if (x.type === 'password') {
      x.type = 'text';
    } else {
      x.type = 'password';
    }
  }



}
