import { Component, OnInit } from '@angular/core';
import { BaseService, SettingsService } from 'src/app/core/services';
import { MyAccountService } from 'src/app/core/services/my-account.service';
import { Router } from '@angular/router';
import { LoggerService } from 'src/app/core/authentication/logger.service';
import { StripeService } from 'src/app/core/services/stripe.service';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
import { LanguageService } from 'src/app/core/services/language.service';


@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {

  // user data coming from response
  userData: any;
  // user name coming from response
  userName: any;
  // user email
  userEmail: any;
  // user picture
  userPicture: any;
  // variable to stire data from the response
  profile: any;
  // user_login_by coming from response
  userLoginBy: any;
  account: boolean;
  device: string;
  // Mt account  data coming from response
  myAccountData: any;
  updatePaymentData: any;
  userTelco: any;
  homeImage: any;
  locales: any;

  constructor(public settingsService: SettingsService, private accountService: MyAccountService, private router: Router,
              private loggerService: LoggerService, private stripeService: StripeService, private baseService: BaseService,
              private languageService: LanguageService) {
      this.homeImage = this.settingsService.homeImage;
      this.locales = this.languageService.appLanguage;
      this.baseService.loaderSource.next(true);
     }

  ngOnInit(): void {
    this.getAccount();
  }

  getAccount() {
    this.accountService.getAccountDetails().subscribe({
      next: res => {
        this.baseService.loaderSource.next(false);
        this.myAccountData = res;
      }, complete: () => {
        this.userName = this.myAccountData.user_name;
        this.userEmail = this.myAccountData.user_email;
        this.userPicture = this.settingsService.imagePath + this.myAccountData.user_picture;
        this.userLoginBy = this.myAccountData.user_login_by;
        this.userTelco = this.myAccountData.user_telco;
      }
    });
  }

  updateMethod() {
    this.accountService.updateData().subscribe((data) => {
      if (data) {
        this.stripeService.checkout(data.session.id);
      }
    });
  }

  checkSubscriptionDevice() {
    if (this.myAccountData.payment_method) {
      // this.myAccountData.payment_method.payment_mode = 'inapp-google';
      if (this.myAccountData.payment_method.payment_mode === 'inapp-google') {
        this.router.navigate(['/manage-subscription', 'android']);
      } else if (this.myAccountData.payment_method.payment_mode === 'inapp-apple') {
        this.router.navigate(['/manage-subscription', 'ios']);
      } else {
        this.router.navigate(['/subscriptions']);
      }
    } else {
      this.router.navigate(['/subscriptions']);
    }
  }

  logout = function() {
    this.loggerService.logout();
  };
}
