import { Component, OnDestroy, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseService, StorageService } from 'src/app/core/services';
import { LanguageService } from 'src/app/core/services/language.service';

@Component({
  selector: 'app-app-language',
  templateUrl: './app-language.component.html',
  styleUrls: ['./app-language.component.scss'],
})
export class AppLanguageComponent implements OnInit, OnDestroy {

  selectedLocale;
  locales;
  selectedLang;
  selectedLocaleKey;
  locale: any;
  user: any;
  homeImage: any;

  constructor(public settingsService: SettingsService, public languageService: LanguageService,
              private configService: ConfigurationService, private translate: TranslateService, private router: Router,
              private route: ActivatedRoute, public baseService: BaseService, private storageService: StorageService) {
    this.locales = this.languageService.appLanguage;
    this.selectedLocale = this.configService.locales[this.configService.localeIndex].lang_name || 'Select Locale';
    this.selectedLocaleKey = this.configService.locales[this.configService.localeIndex].lang_code;
    this.homeImage = this.settingsService.homeImage;
    this.route.params.subscribe(params => {
      this.user = params.type;
      if (this.user !== 'update') {
        this.settingsService.showHeader = false;
        this.settingsService.showFooter = false;
      }
    });
  }

  ngOnInit(): void {
  }

  setLocale(i, value) {
    this.selectedLocale = value.lang_name;
    this.selectedLocaleKey = value.lang_code;
    this.configService.localeIndex = i;
    this.translate.use(this.selectedLocaleKey);
    this.storageService.setLocalStore('locale', this.selectedLocaleKey);
    this.configService.locale = this.selectedLocaleKey;
  }

  changeLocale() {
    this.selectedLocaleKey = this.configService.locales[this.configService.localeIndex].lang_code;
    const localeAvailable = this.configService.availableLocale.indexOf(this.selectedLocaleKey);
    if (localeAvailable > -1) {
      this.translate.use(this.selectedLocaleKey);
    } else {
      this.translate.use(this.configService.defaultLanguage);
    }
    this.storageService.setLocalStore('locale', this.selectedLocaleKey);
    this.locale = this.selectedLocaleKey;
    if (this.user !== 'update') {
      if (this.baseService.loginMode === 1) {
        this.router.navigateByUrl('/index');
      }
      else {
        this.router.navigateByUrl('/home');
      }
    }
    else {
      this.router.navigateByUrl('/account-settings');
    }
  }

  ngOnDestroy() {
    this.settingsService.showHeader = true;
    this.settingsService.showFooter = true;
  }

}
