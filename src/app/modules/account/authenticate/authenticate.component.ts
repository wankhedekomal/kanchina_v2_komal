import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from 'src/app/core/services/authenticate.service';
import { Router } from '@angular/router';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.scss']
})
export class AuthenticateComponent implements OnInit {

  code: string;
  guest;
  back = true;
  close = false;
  homeImage: any;

  constructor(public settingsService: SettingsService, public authenticateService: AuthenticateService, private router: Router,
    private baseService: BaseService, private storageService: StorageService) {
    this.baseService.loaderSource.next(true);
    this.homeImage = this.settingsService.homeImage;
  }

  ngOnInit(): void {
    this.authenticateService.authenticate().subscribe(data => {
      this.baseService.loaderSource.next(false);
      this.code = data.data.mcode;
    });
  }

  signIn() {
    document.getElementById('signinclose').click();
    this.storageService.setLocalStore('authenticate', 'true');
    this.router.navigateByUrl('/signin');
  }

}
