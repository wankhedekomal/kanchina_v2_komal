import { Component, OnInit, HostListener } from '@angular/core';
import { HomeService } from 'src/app/core/services/home.service';
import { BaseService, SettingsService, StorageService } from 'src/app/core/services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  windowWidth: number;
  slideToShow: number;
  slideToScroll: number;
  slideToShowSeries: number;
  barwidth: number;
  slideToScrollSeries: number;
  increment = 6;
  datas;
  datasLength;
  mainData;
  recentVideo;
  slides;
  slidesLength;
  limit;
  subCatTitle;
  check = false;
  siteName: any;
  $keyindex = 0;
  imagePath;
  homeBgImage;
  bannerConfig;
  dataAvailable = true;
  type: any;
  constructor(private homeService: HomeService, private settingsService: SettingsService, private router: Router,
              private baseService: BaseService, private route: ActivatedRoute, private storageService: StorageService) {
    this.imagePath = this.settingsService.imagePath;
    this.homeBgImage = this.settingsService.homeBgImage;
    this.getAdjustCards();
    this.siteName = this.settingsService.siteName;
    this.bannerConfig = {
      infinite: true, slidesToShow: 1, fade: true, slidesToScroll: 1, dots: true, centerMode: ((this.slides?.length > 1) ? false : false)
      , autoplay: true, speed: 900, autoplaySpeed: 4000, prevArrow: false, nextArrow: false
    };
    this.baseService.loaderSource.next(true);
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.type = params.type;
        if (this.type === 'cancel') {
          window.dataLayer = window.dataLayer || [];
          window.dataLayer.push({
            event: 'GAEvent',
            eventCategory: 'Payment',
            eventAction: 'Cancel',
            eventLabel: '',
            loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
            userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
            notificationStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
            country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
            plateform: 'Web'
          });
          console.log(window.dataLayer);
        }
      });
    this.homeService.assets().subscribe((data) => {
      if (data) {
        this.baseService.loaderSource.next(false);
        if (!data.length) {
          this.dataAvailable = false;
        }
        this.mainData = [];
        let j = 0;
        for (let i = 0; i < data.length; i++) {
          if (data[i].key === 'banner') {
            this.recentVideo = data[i];
            this.slides = data[i].list;
            this.slidesLength = data[i].list.length;
            // angular.forEach($scope.slides, function (value, key) {
            //   var slug = slugify(value.title);
            //   $scope.slides[key]['slug'] = slug;
            // })
          } else {
            this.mainData.push(data[i]);
            if (this.mainData[j].key.includes('popular_')) {
              this.mainData[j].keyName = this.mainData[j].key.split('_')[1];
              this.mainData[j].key = this.mainData[j].key.replace(/ /g, '_');
              // this.mainData[j].keyNameSlug = slugify($scope.mainData[j].keyName);
              // homeCatReload[$scope.mainData[j].keyNameSlug] =this.mainData[j].keyName;
            } else {
              this.mainData[j].keyName = this.mainData[j].key;
              // $scope.mainData[j].keyNameSlug = slugify($scope.mainData[j].keyName);
              // homeCatReload[$scope.mainData[j].keyNameSlug] = $scope.mainData[j].keyName;
            }
            j++;
          }
        }

        // if (Object.keys(homeCatReload).length)
        //   this.storageService.setLocalStore("homeCatReload", JSON.stringify(homeCatReload))
        if (this.mainData.length > this.increment) {
          this.datas = this.mainData.slice(0, this.increment);
          this.datasLength = this.datas.length;
        } else {
          this.datas = this.mainData;
          this.limit = this.datas.length;
          this.datasLength = this.datas.length;
        }
      }
    }, (err) => {
      this.baseService.loaderSource.next(false);
    });

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.getAdjustCards();
  }
  loadMore = function() {
    this.datas = this.mainData;
  };
  setTitleValue = function(value) {
    this.subCatTitle = value;
  };

  redirect = (video, key) => {
    const vedioName = this.settingsService.slugify(video.title);
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
      event: 'GAEvent',
      eventCategory: 'Content Click',
      eventAction: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
      eventLabel: video.title,
      loginStatus: (this.storageService.getLocalStore('u_id') ? 'True' : 'False'),
      userId: (this.storageService.getLocalStore('u_id') ? this.storageService.getLocalStore('u_id') : ''),
      notificationStatus: 'False',
      content: video.title,
      VideoCategory: (this.storageService.getLocalStore('sub_cat_title') ? this.storageService.getLocalStore('sub_cat_title') : ''),
      country: (this.storageService.getLocalStore('region') ? this.storageService.getLocalStore('region') : ''),
      plateform: 'Web'
    });
    console.log(window.dataLayer);
    if (video.is_livechannel) {
      this.router.navigate(['/live-video', vedioName, video.content_id]);
    } else if (video.is_series) {
      if (key === 'continue') {
        this.router.navigate(['/video', vedioName, video.content_id]);
      } else {
        this.router.navigate(['/series', vedioName, video.content_id]);
      }
    } else {
      this.router.navigate(['/video', vedioName, video.content_id]);
    }
  }

  getAdjustCards() {
    this.windowWidth = window.innerWidth;
    switch (true) {
      case this.windowWidth > 1800:
        this.slideToShow = 6;
        this.slideToScroll = 5;
        this.slideToShowSeries = 7;
        this.slideToScrollSeries = 7;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 1365 && this.windowWidth < 1801):
        this.slideToShow = 5;
        this.slideToScroll = 4;
        this.slideToShowSeries = 6;
        this.slideToScrollSeries = 6;
        this.barwidth = 0.85;
        break;

      case (this.windowWidth > 1023 && this.windowWidth < 1366):
        this.slideToShow = 4;
        this.slideToScroll = 3;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 4;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 992 && this.windowWidth < 1024):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.85;
        break;
      case (this.windowWidth > 767 && this.windowWidth < 992):
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 4;
        this.slideToScrollSeries = 4;
        this.barwidth = 0.65;
        break;
      case (this.windowWidth > 479 && this.windowWidth < 768):
        this.slideToShow = 2;
        this.slideToScroll = 1;
        this.slideToShowSeries = 3.5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.44;
        break;
      case (this.windowWidth < 480):
        this.slideToShow = 1.5;
        this.slideToScroll = 2;
        this.slideToShowSeries = 2.5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.70;
        break;
      case (this.windowWidth < 360):
        this.slideToShow = 1;
        this.slideToScroll = 1;
        this.slideToShowSeries = 1.8;
        this.slideToScrollSeries = 2;
        this.barwidth = 0.70;
        break;
      default:
        this.slideToShow = 3;
        this.slideToScroll = 2;
        this.slideToShowSeries = 5;
        this.slideToScrollSeries = 3;
        this.barwidth = 0.85;
        break;
    }
  }

}
